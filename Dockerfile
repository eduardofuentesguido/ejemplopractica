#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 8081
EXPOSE 8080
EXPOSE 2181
EXPOSE 443

ENV ASPNETCORE_URLS http://+:8081
ENV ASPNETCORE_ENVIRONMENT DockerDevelopment

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["ApiGateway/ApiGateway.csproj", "ApiGateway/"]
COPY ["ApiGateway.Common.Aws.Utils/ApiGateway.Common.Aws.Utils.csproj", "ApiGateway.Common.Aws.Utils/"]
COPY ["ApiGateway.Common.Extensions/ApiGateway.Common.Extensions.csproj", "ApiGateway.Common.Extensions/"]
COPY ["ApiGateway.Common.HttpExtensions/ApiGateway.Common.HttpExtensions.csproj", "ApiGateway.Common.HttpExtensions/"]
COPY ["ApiGateway.Common.OnPremise.Utils/ApiGateway.Common.OnPremise.Utils.csproj", "ApiGateway.Common.OnPremise.Utils/"]
RUN dotnet restore "ApiGateway/ApiGateway.csproj"
COPY . .
WORKDIR "/src/ApiGateway"
RUN dotnet build "ApiGateway.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ApiGateway.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ApiGateway.dll"]
