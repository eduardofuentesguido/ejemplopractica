﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace ApiGateway.Common.Extensions
{
    public static class StringExtensions
    {
        public static string SplitCamelCase(this string value)
        {
            var regex = new Regex(@"(?<=[A-Z])(?=[A-Z][a-z]) | (?<=[^A-Z])(?=[A-Z]) | (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);
            return regex.Replace(value, " ");
        }

        public static byte[] StringToByteArray(this string value)
        {
            return Enumerable.Range(0, value.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(value.Substring(x, 2), 16))
                             .ToArray();
        }

        public static string UrlEncode(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            return value.TrimEnd('=').Replace('+', '-').Replace('/', '_');
        }

        public static string UrlDecode(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            value = value.Replace('-', '+').Replace('_', '/');

            int paddings = value.Length % 4;
            if (paddings > 0)
            {
                value += new string('=', 4 - paddings);
            }

            return value;
        }
    }
}
