﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGateway.Common.Extensions
{
    /// <summary>
    /// The enumerable extensions.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Obtiene el recuento de particiones.
        /// </summary>
        private static int GetPartitionCount => 5;

        /// <summary>
        /// Realiza una sentencia ForEach de forma asincrona del objeto de referencia.
        /// </summary>
        /// <param name="source">Objeto de referencia o clase.</param>
        /// <param name="body">Cuerpo del método o funciones.</param>
        /// <returns></returns>
        public static Task ForEachAsync<T>(this IEnumerable<T> source, Func<T, Task> body)
        {
            return Task.WhenAll(
                from partition in Partitioner.Create(source).GetPartitions(GetPartitionCount)
                select Task.Run(async delegate
                {
                    using (partition)
                        while (partition.MoveNext())
                            await body(partition.Current).ConfigureAwait(false);
                })
            );
        }
    }
}
