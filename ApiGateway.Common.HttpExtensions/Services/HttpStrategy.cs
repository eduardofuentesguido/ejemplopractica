﻿using ApiGateway.Common.HttpExtensions.Models;
using ApiGateway.Common.HttpExtensions.Models.HttpInterfaces;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using static ApiGateway.Common.HttpExtensions.Models.Enums.HttpEnum;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: INTERFAZ PARA DEFINIR LOS METODOS Y PROPIEDADES DE COMUNICACIÓN MEIDANTE HTTP.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.HttpExtensions.Services
{
    public class HttpStrategy : IHttpStrategy
    {
        private readonly IEnumerable<IHttpOperation> _httpOperation;

        public HttpStrategy(IEnumerable<IHttpOperation> httpOperation)
        {
            _httpOperation = httpOperation;
        }

        public async Task<HttpResponseMessage> CallServiceAsync(OperationType operation, HttpModelContent modelContent, Dictionary<string, string> headerDictionary)
        {
            return await (from op in _httpOperation where op.Operation == operation select op.CallServiceAsync(modelContent, headerDictionary)).FirstOrDefault().ConfigureAwait(false);
        }
    }
}
