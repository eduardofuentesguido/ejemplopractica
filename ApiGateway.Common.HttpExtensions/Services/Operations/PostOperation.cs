﻿using ApiGateway.Common.HttpExtensions.Models;
using ApiGateway.Common.HttpExtensions.Models.HttpInterfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using static ApiGateway.Common.HttpExtensions.Models.Enums.HttpEnum;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: EJECUTA UNA OPERACION POST MEDIANTE HTTP.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.HttpExtensions.Services.Operations
{
    public class PostOperation : IHttpOperation
    {
        public OperationType Operation => OperationType.Post;

        private readonly HttpClient _httpClient;
        private readonly ILogger<PostOperation> _logger;

        public PostOperation(
            HttpClient httpClient,
            ILogger<PostOperation> logger)
        {
            _logger = logger;
            _httpClient = httpClient;
        }

        public async Task<HttpResponseMessage> CallServiceAsync(
            HttpModelContent modelContent,
            Dictionary<string, string> headerDictionary)
        {
            try
            {
                var queryString = $"{modelContent.RequestUri}{modelContent.Query}";
                using var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, $"{modelContent.BaseUri}{queryString}")
                {
                    Content = modelContent.Content
                };

                if (!string.IsNullOrEmpty(modelContent.BearerToken))
                {
                    httpRequestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue(modelContent.AuthenticationSchema, modelContent.BearerToken);
                }

                if (!httpRequestMessage.Headers.Contains("Accept"))
                {
                    httpRequestMessage.Headers.Accept.Add(
                        new MediaTypeWithQualityHeaderValue(modelContent.AcceptHeaderValue));
                }

                if (headerDictionary != null && headerDictionary.Any())
                {
                    foreach (var item in headerDictionary)
                    {
                        httpRequestMessage.Headers.Add(item.Key, item.Value);
                    }
                }

                _logger.LogInformation(new JObject
                {
                    new JProperty("Request", $"{modelContent.BaseUri}{modelContent.RequestUri}"),
                    new JProperty("Headers", $"{httpRequestMessage.Headers}"),
                    new JProperty("HTTP Method Request", HttpMethod.Post.ToString()),
                    new JProperty("Content Request", await modelContent.Content.ReadAsStringAsync().ConfigureAwait(false))
                }.ToString());

                using var httpResponseMessage = _httpClient.SendAsync(httpRequestMessage, CancellationToken.None);
                return await httpResponseMessage.ConfigureAwait(false);
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error al procesar la solicitud.", ex);
                throw;
            }
        }
    }
}
