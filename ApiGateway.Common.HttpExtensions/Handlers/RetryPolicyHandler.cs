﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Polly;
using Polly.Retry;
using System;
using System.Net;
using System.Net.Http;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: GENERA REINTENTOS DE COMUNICACION HTTP.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   29 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    29-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.HttpExtensions.Handlers
{
    public static class RetryPolicyHandler
    {
        public static AsyncRetryPolicy<HttpResponseMessage> RetryPolicy(ILogger logger)
        {
            var retry = Policy
                .HandleResult<HttpResponseMessage>(message => (int)message.StatusCode >= 500)
                .OrResult(message => message.StatusCode == HttpStatusCode.RequestTimeout)
                .OrResult(message => message.StatusCode == HttpStatusCode.NotFound)
                .OrResult(message => message.StatusCode == HttpStatusCode.Unauthorized)
                .WaitAndRetryAsync(2,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(1, retryAttempt)),
                    (delegateResult, timeSpan, retryCount, context) =>
                    {
                        logger.LogInformation(new JObject
                        {
                            new JProperty("reasonPhrase", delegateResult.Result.ReasonPhrase),
                            new JProperty("statusCode", (int)delegateResult.Result.StatusCode),
                            new JProperty("content", $"La petición falló con código de estado HTTP {delegateResult.Result.StatusCode}. " +
                            $"Esperando {timeSpan} segundos para el siguiente reintento.\nNúmero de intento {retryCount}.\nURL: {delegateResult.Result.RequestMessage?.RequestUri}")
                        }.ToString());
                    });

            return retry;
        }
    }
}
