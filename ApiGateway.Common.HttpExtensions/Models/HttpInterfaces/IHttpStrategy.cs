﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using static ApiGateway.Common.HttpExtensions.Models.Enums.HttpEnum;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: INTERFAZ PARA DEFINIR LOS METODOS Y PROPIEDADES DE COMUNICACIÓN MEIDANTE HTTP.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.HttpExtensions.Models.HttpInterfaces
{
    public interface IHttpStrategy
    {
        Task<HttpResponseMessage> CallServiceAsync(OperationType operation, HttpModelContent modelContent, Dictionary<string, string> headerDictionary);
    }
}
