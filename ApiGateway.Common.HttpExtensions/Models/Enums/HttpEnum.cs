﻿//--------------------------------------------------------------------------------
//-- DESCRIPCION: ENUMERA EL TIPO DE METODOS A EJECUTAR MEDIANTE COMUNICACION HTTP.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.HttpExtensions.Models.Enums
{
    public static class HttpEnum
    {
        public enum OperationType
        {
            Get,
            Post,
            Delete,
            Put,
            Patch
        }
    }
}
