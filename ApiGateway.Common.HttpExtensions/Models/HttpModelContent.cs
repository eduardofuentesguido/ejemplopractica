﻿using System.Net.Http;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: MODELO DE DATOS PARA REALIZAR LAS SOLICITUDES HTTP.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.HttpExtensions.Models
{
    public class HttpModelContent
    {
        public StringContent Content { get; set; }
        public string BaseUri { get; set; }
        public string Query { get; set; } = string.Empty;
        public string RequestUri { get; set; }
        public string AuthenticationSchema { get; set; }
        public string AcceptHeaderValue { get; set; }
        public string BearerToken { get; set; } = string.Empty;
    }
}
