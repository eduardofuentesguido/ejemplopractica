﻿# SuperCore - Api Gateway
El Orquestador de **SuperCore** es un Microservicio que permite integrar servicios provenientes de diversas fuentes y proveer información de forma síncrona o asíncrona, a través del uso de Web Services.

El funcionamiento del Orquestador se lleva acabo gracias una librería llamada **Ocelot** que se instala en nuestra aplicación de .Net Core, esta librería permite configurar todo el enrutamiento desde un archivo **.json**.

## Ocelot
La funcionalidad principal de una puerta de enlace de API de Ocelot consiste en aceptar solicitudes HTTP entrantes y reenviarlas a un servicio de nivel inferior, actualmente como otra solicitud HTTP. Ocelot describe el enrutamiento de una solicitud a otra como un elemento ReRoute.

Ocelot manipula el objeto **HttpRequest** en un estado especificado por su configuración hasta que llega a un middleware de creación de solicitudes donde crea un objeto **HttpRequestMessage** que se utiliza para realizar una solicitud a un servicio descendente. El middleware que realiza la solicitud es lo último en la canalización de Ocelot. No llama al siguiente middleware. Hay una pieza de middleware que asigna **HttpResponseMessage** al objeto **HttpResponse** y que se devuelve al cliente. Eso es básicamente todo con un montón de otras características.

## Uso de las propiedades
| Propiedad | Función |
| ------ | ------ |
| DownstreampathTemplate | Define la ruta del punto final real de microservicio. |
| DownstreamScheme | Esquema de microservicio, HTTPS. |
| DownstreamHostsandPorts | Host y Puerto de microservicio se definirán aquí. |
| UpstreampathTemplate | La ruta en la que el cliente solicitará Ocelot API Gateway. |
| UpstreamHttpmethod | Los métodos HTTP admitidos para API Gateway. Según el método entrante, Ocelot también envía una solicitud de método HTTP similar a los microservicios. |
| GlobalConfiguration - BaseUrl | La rupta por defecto que tendrá configurado el API Gateway. |

**Para más información, puedes consultar la documentación oficial de Ocelot:** https://ocelot.readthedocs.io/en/latest/index.html

## Instalación
Ocelot y sus dependencias se instalan en el proyecto de .Net Core (Orquestador) con la herramienta de Nuget desde Visual Studio o simplemente, ejecute el siguiente comando desde un CLI.
```sh
Install-Package Ocelot
```

## Configuración
Cree un archivo llamado **configuration.json** en su proyecto del Orquestador para definir las rutas que son necesarias para los microservicios.
El siguiente código muestra la configuración básica para Ocelot, el sistema no hará nada, pero debería poner en marcha a Ocelot.
```sh
{
    "Routes": [],
    "GlobalConfiguration": {
        "BaseUrl": "https://api.mybusiness.com"
    }
}
```

Si quieres algún ejemplo que realmente haga algo, usa lo siguiente:
```sh
{
    "Routes": [
        {
        "DownstreamPathTemplate": "/todos/{id}",
        "DownstreamScheme": "https",
        "DownstreamHostAndPorts": [
            {
                "Host": "jsonplaceholder.typicode.com",
                "Port": 443
            }
        ],
        "UpstreamPathTemplate": "/todos/{id}",
        "UpstreamHttpMethod": [ "Get" ]
        }
    ],
    "GlobalConfiguration": {
        "BaseUrl": "https://localhost:5000"
    }
}
```

Agregue el archivo de configuración a la clase **Program.cs**
```sh
public static IHostBuilder CreateHostBuilder(string[] args) =>
    ...
    .ConfigureAppConfiguration(config =>
    {
        config.AddJsonFile("configuration.json", optional: false, reloadOnChange: false);
    })
});
```

Configure la clase **Startup.cs** para implementar la biblioteca de Ocelot.
```sh
using Ocelot.Middleware;  

public void ConfigureServices(IServiceCollection services)  
{
    ...
    services.AddOcelot();  
} 

public async void Configure(IApplicationBuilder app, IWebHostEnvironment env)  
{
    ...
    await app.UseOcelot(); 
}
```
