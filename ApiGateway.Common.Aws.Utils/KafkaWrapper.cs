﻿using ApiGateway.Common.Aws.Utils.Models.Interfaces.Wrappers;
using ApiGateway.Common.Aws.Utils.Models.Kafka;
using ApiGateway.Common.Aws.Utils.Services;
using System;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: WRAPPER PARA CONSUMIR EL SERVICIO DE ESCRITURA EN KAFKA.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.Aws.Utils
{
    public class KafkaWrapper : IKafkaWrapper
    {
        private readonly KafkaService _kafkaService;

        public KafkaWrapper()
        {
            _kafkaService = new KafkaService();
        }

        public async Task<Exception> SendMessageAsync(string message, KafkaConfiguration configuration)
        {
            return await _kafkaService.SendMessageAsync(message, configuration).ConfigureAwait(false);
        }
    }
}
