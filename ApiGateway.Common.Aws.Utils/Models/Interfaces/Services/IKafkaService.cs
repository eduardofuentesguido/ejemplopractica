﻿using ApiGateway.Common.Aws.Utils.Models.Kafka;
using System;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: INTERFAZ PARA ESTABLECER MÉTODOS DE RESPUESTA DEL SERVICIO DE KAFKA.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.Aws.Utils.Models.Interfaces.Services
{
    internal interface IKafkaService
    {
        /// <summary>
        /// Pubica un registro en Kafka.
        /// </summary>
        /// <param name="message">Mensaje a publicar.</param>
        /// <param name="configuration">Objeto de configuración de Kafka, ver:<see cref="KafkaConfiguration"/></param>
        /// <returns></returns>
        Task<Exception> SendMessageAsync(string message, KafkaConfiguration configuration);
    }
}
