﻿using ApiGateway.Common.Aws.Utils.Models.Kafka;
using System;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: INTERFAZ DEL WRAPPER DE KAFKA.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.Aws.Utils.Models.Interfaces.Wrappers
{
    public interface IKafkaWrapper
    {
        Task<Exception> SendMessageAsync(string message, KafkaConfiguration configuration);
    }
}
