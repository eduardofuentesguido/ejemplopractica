﻿//--------------------------------------------------------------------------------
//-- DESCRIPCION: MODELO DE DATOS PARA LA CONFIGURACION DE KAFKA.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.Aws.Utils.Models.Kafka
{
    public class KafkaConfiguration
    {
        public string Topic { get; set; }
        public int FlushTime { get; set; } = 5;
        public string BootstrapServers { get; set; }
    }
}
