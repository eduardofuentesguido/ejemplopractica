﻿using ApiGateway.Common.Aws.Utils.Models.Interfaces.Services;
using ApiGateway.Common.Aws.Utils.Models.Kafka;
using Confluent.Kafka;
using System;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: SERVICIO PARA REGISTRAR UN MENSAJE EN KAFKA.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.Aws.Utils.Services
{
    internal class KafkaService : IKafkaService
    {
        private IProducer<string, string> _producer;

        public async Task<Exception> SendMessageAsync(string message, KafkaConfiguration configuration)
        {
            Exception exception = null;
            var config = new ProducerConfig
            {
                Acks = Acks.All,
                BootstrapServers = configuration.BootstrapServers,
            };

            try
            {
                _producer = new ProducerBuilder<string, string>(config).Build();
                await _producer.ProduceAsync(configuration.Topic, new Message<string, string> { Key = Guid.NewGuid().ToString(), Value = message }).ConfigureAwait(false);
                _producer.Flush(TimeSpan.FromSeconds(configuration.FlushTime));
            }
            catch (Exception ex) when (ex != null)
            {
                exception = ex;
            }
            finally
            {
                if (_producer != null)
                {
                    _producer.Dispose();
                }
            }

            return exception;
        }
    }
}
