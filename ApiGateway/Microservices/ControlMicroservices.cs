﻿using ApiGateway.Models;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.RepositoryInterfaces.SuperCore;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using static ApiGateway.Models.Enums.ApiGatewayEnums;
using static ApiGateway.Models.Microservices.Settings.ControlSettings;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: DEFINE TODOS LOS MICROSERVICIOS DEL MODULO DE CONTROL.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   05 / 07 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    05-07-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Microservices
{
    public class ControlMicroservices : IControlMicroservices
    {
        private readonly CamSettings _camSettings;
        private readonly IServiceExecutionRepository _serviceExecutionRepository;

        public ControlMicroservices(
            IOptions<CamSettings> camSettings,
            IServiceExecutionRepository serviceExecutionRepository)
        {
            _camSettings = camSettings.Value;
            _serviceExecutionRepository = serviceExecutionRepository;
        }

        /// <inheritdoc cref="IControlMicroservices.ExecuteCamAsync(DatosSuperCore{MSCamDataEntry})"/>
        public async Task<HttpResponseMessage> ExecuteCamAsync(DatosSuperCore<MSCamDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_camSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }
    }
}
