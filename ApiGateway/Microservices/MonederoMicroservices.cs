﻿using ApiGateway.Models;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.RepositoryInterfaces.SuperCore;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using static ApiGateway.Models.Enums.ApiGatewayEnums;
using static ApiGateway.Models.Microservices.Settings.MonederoSettings;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: DEFINE TODOS LOS MICROSERVICIOS DEL MODULO DE MONEDERO.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   01 / 07 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    01-07-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Microservices
{
    /// <inheritdoc cref="IMonederoMicroservices"/>
    public class MonederoMicroservices : IMonederoMicroservices
    {
        private readonly BloqueoSettings _bloqueoSettings;
        private readonly AperturaSettings _aperturaSettings;
        private readonly DesbloqueoSettings _desbloqueoSettings;
        private readonly CancelacionSettings _cancelacionSettings;
        private readonly ConsultaSaldoSettings _consultaSaldoSettings;
        private readonly ModificaTelefonoSettings _modificaTelefonoSettings;
        private readonly ConsultaTelefonoSettings _consultaTelefonoSettings;
        private readonly ValidaAcumuladosSettings _validaAcumuladosSettings;
        private readonly ConsultaMonederoSettings _consultaMonederoSettings;
        private readonly ConsultaMonederoTelefonoSettings _consultaMonederoTelefonoSettings;
        private readonly ConsultaBloqueosSettings _consultaBloqueosSettings;
        private readonly ConsultaSaldoSicuSettings _consultaSaldoSicuSettings;
        private readonly ConsultaMovimientosSettings _consultaMovimientosSettings;
        private readonly IServiceExecutionRepository _serviceExecutionRepository;

        /// <summary>
        /// Constructor de la clase.
        /// </summary>
        /// <remarks>
        /// Carga todas las clases Setting necesarias para realizar una Inyección de Dependencias del modulo de Monedero.
        /// </remarks>
        /// <param name="bloqueoSettings"></param>
        /// <param name="aperturaSettings"></param>
        /// <param name="desbloqueSettings"></param>
        /// <param name="cancelacionSettings"></param>
        /// <param name="consultaSaldoSettings"></param>
        /// <param name="modificaTelefonoSettings"></param>
        /// <param name="consultaTelefonoSettings"></param>
        /// <param name="validaAcumuladosSettings"></param>
        /// <param name="consultaMonederoSettings"></param>
        /// <param name="consultaMonederoTelefonoSettings"></param>
        /// <param name="consultaBloqueosSettings"></param>
        /// <param name="consultaSaldoSicuSettings"></param>
        /// <param name="consultaMovimientosSettings"></param>
        /// <param name="serviceExecutionRepository"></param>
        public MonederoMicroservices(
            IOptions<BloqueoSettings> bloqueoSettings,
            IOptions<AperturaSettings> aperturaSettings,
            IOptions<DesbloqueoSettings> desbloqueSettings,
            IOptions<CancelacionSettings> cancelacionSettings,
            IOptions<ConsultaSaldoSettings> consultaSaldoSettings,
            IOptions<ModificaTelefonoSettings> modificaTelefonoSettings,
            IOptions<ConsultaTelefonoSettings> consultaTelefonoSettings,
            IOptions<ValidaAcumuladosSettings> validaAcumuladosSettings,
            IOptions<ConsultaMonederoSettings> consultaMonederoSettings,
            IOptions<ConsultaMonederoTelefonoSettings> consultaMonederoTelefonoSettings,
            IOptions<ConsultaBloqueosSettings> consultaBloqueosSettings,
            IOptions<ConsultaSaldoSicuSettings> consultaSaldoSicuSettings,
            IOptions<ConsultaMovimientosSettings> consultaMovimientosSettings,
            IServiceExecutionRepository serviceExecutionRepository)
        {
            _bloqueoSettings = bloqueoSettings.Value;
            _aperturaSettings = aperturaSettings.Value;
            _desbloqueoSettings = desbloqueSettings.Value;
            _cancelacionSettings = cancelacionSettings.Value;
            _consultaSaldoSettings = consultaSaldoSettings.Value;
            _modificaTelefonoSettings = modificaTelefonoSettings.Value;
            _consultaTelefonoSettings = consultaTelefonoSettings.Value;
            _validaAcumuladosSettings = validaAcumuladosSettings.Value;
            _consultaMonederoSettings = consultaMonederoSettings.Value;
            _consultaMonederoTelefonoSettings = consultaMonederoTelefonoSettings.Value;
            _consultaBloqueosSettings = consultaBloqueosSettings.Value;
            _consultaSaldoSicuSettings = consultaSaldoSicuSettings.Value;
            _consultaMovimientosSettings = consultaMovimientosSettings.Value;
            _serviceExecutionRepository = serviceExecutionRepository;
        }

        /// <inheritdoc cref="IMonederoMicroservices.AperturaAsync(DatosSuperCore{MSAperturaDataEntry}, Dictionary{string, string})"/>
        public async Task<HttpResponseMessage> AperturaAsync(DatosSuperCore<MSAperturaDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_aperturaSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.BloqueoAsync(DatosSuperCore{MSBloqueoDataEntry}), Dictionary{string, string})"/>
        public async Task<HttpResponseMessage> BloqueoAsync(DatosSuperCore<MSBloqueoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_bloqueoSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.CancelacionAsync(DatosSuperCore{MSCancelacionDataEntry}), Dictionary{string, string})" />
        public async Task<HttpResponseMessage> CancelacionAsync(DatosSuperCore<MSCancelacionDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_cancelacionSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.ConsultaBloqueoAsync(DatosSuperCore{MSConsultaBloqueoDataEntry}), Dictionary{string, string})" />
        public async Task<HttpResponseMessage> ConsultaBloqueoAsync(DatosSuperCore<MSConsultaBloqueoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_consultaBloqueosSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.ConsultaMonederoAsync(DatosSuperCore{MSConsultaMonederoDataEntry}), Dictionary{string, string})" />
        public async Task<HttpResponseMessage> ConsultaMonederoAsync(DatosSuperCore<MSConsultaMonederoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_consultaMonederoSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.ConsultaMonederoAsync(DatosSuperCore{MSConsultaMonederoTelefonoDataEntry}), Dictionary{string, string})" />
        public async Task<HttpResponseMessage> ConsultaMonederoAsync(DatosSuperCore<MSConsultaMonederoTelefonoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_consultaMonederoTelefonoSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        public async Task<HttpResponseMessage> ConsultaMonederoSicuAsync(DatosSuperCore<MSConsultaDetalleCuentaDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_consultaMonederoSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.ConsultaMovimientosAsync(DatosSuperCore{MSConsultaMovimientosDataEntry}), Dictionary{string, string})" />
        public async Task<HttpResponseMessage> ConsultaMovimientosAsync(DatosSuperCore<MSConsultaMovimientosDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_consultaMovimientosSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.ConsultaSaldoAsync(DatosSuperCore{MSConsultaSaldoMonederoDataEntry}), Dictionary{string, string})" />
        public async Task<HttpResponseMessage> ConsultaSaldoAsync(DatosSuperCore<MSConsultaSaldoMonederoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_consultaSaldoSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.ConsultaSaldoSicuAsync(DatosSuperCore{MSConsultaSaldoSicuDataEntry}), Dictionary{string, string})" />
        public async Task<HttpResponseMessage> ConsultaSaldoSicuAsync(DatosSuperCore<MSConsultaSaldoSicuDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_consultaSaldoSicuSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.ConsultaTelefonoAsync(DatosSuperCore{MSConsultaTelefonoDataEntry}), Dictionary{string, string})" />
        public async Task<HttpResponseMessage> ConsultaTelefonoAsync(DatosSuperCore<MSConsultaTelefonoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_consultaTelefonoSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.DesbloqueoAsync(DatosSuperCore{MSDesbloqueDataEntry}), Dictionary{string, string})" />
        public async Task<HttpResponseMessage> DesbloqueoAsync(DatosSuperCore<MSDesbloqueDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_desbloqueoSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.ModificaTelefonoAsync(DatosSuperCore{MSModificaTelefonoDataEntry}), Dictionary{string, string})" />
        public async Task<HttpResponseMessage> ModificaTelefonoAsync(DatosSuperCore<MSModificaTelefonoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_modificaTelefonoSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }

        /// <inheritdoc cref="IMonederoMicroservices.ValidaAcumuladoAsync(DatosSuperCore{MSValidaAcumuladoDataEntry}), Dictionary{string, string})" />
        public async Task<HttpResponseMessage> ValidaAcumuladoAsync(DatosSuperCore<MSValidaAcumuladoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_validaAcumuladosSettings, datosSuperCore, headerDictionary, EnterpriseSystem.Microservices);
        }
    }
}
