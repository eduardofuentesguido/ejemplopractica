﻿namespace ApiGateway.Models
{
    public class BaseDataOutput
    {
        public string Folio { get; set; }
        public string Mensaje { get; set; }
        public string Codigo { get; set; }
    }
}
