﻿using System.Collections.Generic;

namespace ApiGateway.Models
{
    /// <summary>
    /// Constantes de ApiGateway.
    /// </summary>
    public static class ApiGatewayConstants
    {
        /// <summary>
        /// Archivo fuente de configuración de la aplicación.
        /// </summary>
        public static class AppSettingsSource
        {
            /// <summary>
            /// Obtiene el nombre del archivo.
            /// </summary>
            public static string FileName => "appSettings";

            /// <summary>
            /// Obtiene la extensión del archivo.
            /// </summary>
            public static string FileExtension => "json";
        }

        /// <summary>
        /// Las variables de entorno.
        /// </summary>
        public static class EnvironmentVariables
        {
            /// <summary>
            /// Obtiene el ambiente.
            /// </summary>
            public static string Environment => "ASPNETCORE_ENVIRONMENT";
        }

        /// <summary>
        /// Los ambientes.
        /// </summary>
        public static class Environments
        {
            /// <summary>
            /// Obtiene el ambiente de desarrollo.
            /// </summary>
            public static string Development => "Development";
        }

        /// <summary>
        /// Configuracion del ApiGateway.
        /// </summary>
        public static class ApiGatewayConfiguration
        {
            /// <summary>
            /// Obtiene la clave de sección.
            /// </summary>
            public static string SectionKey => "Configuration";
        }

        /// <summary>
        /// The headers.
        /// </summary>
        public static class Headers
        {
            public static string IdAcceso => "x-idAcceso";
            public static string IdInteraccion => "x-id-interaccion";
            public static string XSicu => "x-sicu";
            public static string Sicu => "sicu";
            public static string Empresa => "x-empresa";
            public static string NombreDispositivo => "x-nombre-dispositivo";
            public static string IdDispositivo => "x-id-dispositivo";
            public static string SistemaDispositivo => "x-sistema-dispositivo";
            public static string VersionAplicacion => "x-version-aplicacion";
            public static string ModeloDispositivo => "x-modelo-dispositivo";
            public static string FabricanteDispositivo => "x-fabricante-dispositivo";
            public static string SerieProcesador => "x-serie-procesador";
            public static string OperadorTelefonia => "x-operador-telefonia";
            public static string Latitud => "x-latitud";
            public static string Longuitud => "x-longitud";
            public static string TokenUsuario => "x-token-usuario";
            public static string IdLealtad => "x-id-lealtad";
            public static string IdClaveRastreo => "x-idClaveRastreo";
            public static string IdOperacionConciliacion => "x-idOperacionConciliacion";
        }

        /// <summary>
        /// Configuracion de Swagger.
        /// </summary>
        public static class Swagger
        {
            /// <summary>
            /// Obtiene el nombre del grupo.
            /// </summary>
            public static string GroupName => "v1";
        }

        /// <summary>
        /// Configuracion de SuperCore.
        /// </summary>
        public static class SuperCore
        {
            /// <summary>
            /// Configuración de los datos de Control de los Microservicios.
            /// </summary>
            public static class DatosDeControl
            {
                /// <summary>
                /// Establece el Canal por defecto.
                /// </summary>
                public static string Canal => "54";
                /// <summary>
                /// Establece el Centro por defecto.
                /// </summary>
                public static string Centro => "1";
                /// <summary>
                /// Establece la Empresa por defecto.
                /// </summary>
                public static string Empresa => "5499";
                /// <summary>
                /// Establece el Usuario por defecto.
                /// </summary>
                public static string Usuario => "USRSUPERAPP";
            }

            /// <summary>
            /// Configuracin de los Microservicios.
            /// </summary>
            public static class Microservicios
            {
                /// <summary>
                /// Configuracion de los Microservicios de Monedero
                /// </summary>
                public static class Monedero
                {
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de Abono.
                    /// </summary>
                    public static string AbonoSectionKey => "SuperCore:Microservices:Monedero:Abono";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de Apertura.
                    /// </summary>
                    public static string AperturaSectionKey => "SuperCore:Microservices:Monedero:Apertura";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de Bloqueo.
                    /// </summary>
                    public static string BloqueoSectionKey => "SuperCore:Microservices:Monedero:Bloqueo";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de Cancelacion.
                    /// </summary>
                    public static string CancelacionSectionKey => "SuperCore:Microservices:Monedero:Cancelacion";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de CancelaRetencion.
                    /// </summary>
                    public static string CancelaRetencionSectionKey => "SuperCore:Microservices:Monedero:CancelaRetencion";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de Cargo.
                    /// </summary>
                    public static string CargoSectionKey => "SuperCore:Microservices:Monedero:Cargo";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de ConsultaBloqueos.
                    /// </summary>
                    public static string ConsultaBloqueosSectionKey => "SuperCore:Microservices:Monedero:ConsultaBloqueos";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de ConsultaMonedero.
                    /// </summary>
                    public static string ConsultaMonederoSectionKey => "SuperCore:Microservices:Monedero:ConsultaMonedero";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de ConsultaMonederoTelefono.
                    /// </summary>
                    public static string ConsultaMonederoTelefonoSectionKey => "SuperCore:Microservices:Monedero:ConsultaMonederoTelefono";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de ConsultaMovimientos.
                    /// </summary>
                    public static string ConsultaMovimientosSectionKey => "SuperCore:Microservices:Monedero:ConsultaMovimientos";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de ConsultaSaldo.
                    /// </summary>
                    public static string ConsultaSaldoSectionKey => "SuperCore:Microservices:Monedero:ConsultaSaldo";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de ConsultaSaldoSicu.
                    /// </summary>
                    public static string ConsultaSaldoSicuSectionKey => "SuperCore:Microservices:Monedero:ConsultaSaldoSicu";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de ConsultaTelefono.
                    /// </summary>
                    public static string ConsultaTelefonoSectionKey => "SuperCore:Microservices:Monedero:ConsultaTelefono";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de Desbloqueo.
                    /// </summary>
                    public static string DesbloqueoSectionKey => "SuperCore:Microservices:Monedero:Desbloqueo";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de LiberaRetencion.
                    /// </summary>
                    public static string LiberaRetencionSectionKey => "SuperCore:Microservices:Monedero:LiberaRetencion";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de ModificaTelefono.
                    /// </summary>
                    public static string ModificaTelefonoSectionKey => "SuperCore:Microservices:Monedero:ModificaTelefono";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de Retencion.
                    /// </summary>
                    public static string RetencionSectionKey => "SuperCore:Microservices:Monedero:Retencion";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de ReversaAbono.
                    /// </summary>
                    public static string ReversaAbonoSectionKey => "SuperCore:Microservices:Monedero:ReversaAbono";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de ReversaCargo.
                    /// </summary>
                    public static string ReversaCargoSectionKey => "SuperCore:Microservices:Monedero:ReversaCargo";
                    /// <summary>
                    /// Obtiene la clave de sección del Microservicio de ValidaAcumulados.
                    /// </summary>
                    public static string ValidaAcumuladosSectionKey => "SuperCore:Microservices:Monedero:ValidaAcumulados";
                }
                /// <summary>
                /// Configuracion de los Microservicios de Control.
                /// </summary>
                public static class Control
                {
                    /// <summary>
                    /// Obtiene la clave de la sección de CAM.
                    /// </summary>
                    public static string CamSectionKey => "SuperCore:Microservices:Control:CAM";
                }
            }
        }

        /// <summary>
        /// Configuracion de OnPremise.
        /// </summary>
        public static class OnPremise
        {
            /// <summary>
            /// Autenticación de OnPremise.
            /// </summary>
            public static class Autentication
            {
                /// <summary>
                /// Obtiene la clave de la sección de autenticación de OnPremise.
                /// </summary>
                public static string OnPremAutenticationSectionKey => "OnPremise:Autentication";
            }

            /// <summary>
            /// Apis de OnPremise
            /// </summary>
            public static class Apis
            {
                /// <summary>
                /// Obtiene la clave de la sección de la Apertura de Monedero de OnPremise.
                /// </summary>
                public static string AperturaSectionKey => "OnPremise:Services:Apertura";
                /// <summary>
                /// Obtiene la clave de la sección de Consulta Movimientos de OnPremise.
                /// </summary>
                public static string ConsultaMovimientosSectionKey => "OnPremise:Services:ConsultaMovimientos";
                /// <summary>
                /// Obtiene la clave de la sección de Consulta Saldo de Monedero de OnPremise.
                /// </summary>
                public static string ConsultaSaldoSectionKey => "OnPremise:Services:ConsultaSaldo";
                /// <summary>
                /// Obtiene la clave de la sección de Traspaso entre Cuentas de OnPremise.
                /// </summary>
                public static string TraspasoCuentasSectionKey => "OnPremise:Services:TraspasoCuentas";
            }
        }

        /// <summary>
        /// Configuración de AWS.
        /// </summary>
        public static class Aws
        {
            /// <summary>
            /// Configuracion de Kafka.
            /// </summary>
            public static class Kafka
            {
                /// <summary>
                /// Obtiene la clave de sección de Kafka.
                /// </summary>
                public static string SectionKey => "AWS:Kafka";
            }
        }

        public static class Avisos
        {
            public static Dictionary<string, string> CodigosDeAviso => GetCodigosAviso();

            private static Dictionary<string, string> GetCodigosAviso()
            {
                return new Dictionary<string, string>()
                {
                    { "ACTL001", "Actualización de la configuración del servicio {0} completada correctamente." }                    
                };
            }
        }

        public static class Errores
        {
            public static Dictionary<string, string> CodigosDeError => GetCodigosError();

            private static Dictionary<string, string> GetCodigosError()
            {
                return new Dictionary<string, string>()
                {
                    { "ECTL001", "La configuración del servicio {0} no es valida." },
                    { "ECTL002", "Actualización de la configuración del servicio {0} completada correctamente." }
                };
            }
        }

        public static class ApiResponse
        {
            public static string Mensaje200 => "Operación exitosa.";
            public static string Mensaje201 => "Operación existente";
            public static string Mensaje202 => "La solicitud se ha recibido, sin embargo, no se ha completado. Ver los detalles.";
            public static string Mensaje204 => "La petición se ha completado con éxito pero su respuesta no tiene ningún contenido. Ver los detalles.";
            public static string Mensaje400 => "El servidor no pudo interpretar la solicitud dada una sintaxis inválida.";
            public static string Mensaje401 => "Es necesario autenticar para obtener la respuesta solicitada.";
            public static string Mensaje404 => "Recurso no encontrado.";
            public static string Mensaje500 => "Ocurrio algo inesperado.";
        }
    }
}
