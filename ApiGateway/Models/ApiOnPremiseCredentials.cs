﻿using System;
using System.Globalization;
using System.Reflection;

namespace ApiGateway.Models
{
    public class ApiOnPremiseCredentials
    {
        private static readonly object InstanceLock = new object();
        private static ApiOnPremiseCredentials _instance;

        public string OnPremiseToken { get; set; }
        public decimal OnPremiseTokenExpiration { get; set; }
        public DateTime OnPremiseDateRegistrationToken { get; set; }

        protected ApiOnPremiseCredentials()
        {
        }

        public static ApiOnPremiseCredentials Instance
        {
            get
            {
                if (_instance != null) return _instance;
                lock (InstanceLock)
                {
                    _instance ??= (typeof(ApiOnPremiseCredentials).InvokeMember(nameof(ApiOnPremiseCredentials),
                        BindingFlags.CreateInstance | BindingFlags.Instance, null, null, null,
                        //BindingFlags.NonPublic, null, null, null,
                        CultureInfo.CurrentCulture) as ApiOnPremiseCredentials);
                }

                return _instance;
            }
        }

        public static void ClearInstance()
        {
            _instance = null;
        }
    }
}
