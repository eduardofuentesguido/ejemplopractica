﻿using Microsoft.Extensions.Configuration;

namespace ApiGateway.Models.ServiceInterfaces.Configuration
{
    public interface IConfigurationService
    {
        /// <summary>
        /// Obtiene la configuración del archivo appSettings.json
        /// </summary>
        /// <returns>El método devuelve un objeto <see cref="IConfiguration"/></returns>
        IConfiguration GetConfiguration();
    }
}
