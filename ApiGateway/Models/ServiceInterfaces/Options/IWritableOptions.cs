﻿using Microsoft.Extensions.Options;
using System;

namespace ApiGateway.Models.ServiceInterfaces.Options
{

    /// <summary>
    /// The writable options.
    /// </summary>
    public interface IWritableOptions<out T> : IOptions<T> where T : class, new()
    {

        /// <summary>
        /// Update appSettings.
        /// </summary>
        /// <param name="applyChanges">The apply changes.</param>
        void Update(Action<T> applyChanges);
    }
}
