﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces
{
   public interface IHealthChecksService
    {
     Task<String> ExecuteServiceAsync();
    
    }
}
