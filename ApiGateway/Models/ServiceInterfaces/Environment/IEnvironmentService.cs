﻿namespace ApiGateway.Models.ServiceInterfaces.Environment
{
    /// <summary>
    /// Define las variables de entorno y extrae su valor del sistema.
    /// </summary>
    public interface IEnvironmentService
    {
        /// <summary>
        /// Variable de entorno para obtener el ambiente en que el ApiGateway se está ejecutando.
        /// </summary>
        /// <value>Development | Production | QA </value>
        string EnvironmentName { get; }
    }
}
