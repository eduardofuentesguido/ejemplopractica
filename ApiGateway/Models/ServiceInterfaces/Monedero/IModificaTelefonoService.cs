﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IModificaTelefonoService
    {
        /// <summary>
        /// Ejecuta servicios de Actualizacion de Teléfono.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para realizar una actualización de teléfono.
        /// </remarks>
        /// <param name="modificaTelfonoDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        Task<DatosSalida<ModificaTelfonoDataOutput>> ExecuteServiceAsync(
            ModificaTelfonoDataEntry modificaTelfonoDataEntry, IHeaderDictionary headerDictionary);
    }
}
