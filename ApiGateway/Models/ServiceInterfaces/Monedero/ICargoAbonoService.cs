﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface ICargoAbonoService
    {
        /// <summary>
        /// Ejecuta servicios de Cargo/Abono de Monedero.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para realizar un Cargo/Abono al Monedero.
        /// </remarks>
        /// <param name="cargoAbonoDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/>.</returns>
        Task<DatosSalida<CargoAbonoDataOutput>> ExecuteServiceAsync(
            CargoAbonoDataEntry cargoAbonoDataEntry, IHeaderDictionary headerDictionary);
    }
}
