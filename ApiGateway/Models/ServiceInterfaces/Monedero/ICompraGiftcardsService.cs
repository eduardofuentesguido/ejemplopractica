﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface ICompraGiftcardsService
    {
        /// <summary>
        /// Ejecuta servicios de Compra de Tarjetas de Regalo.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para realizar compras de tarjetas.
        /// </remarks>
        /// <param name="idOperador">Id del Operador.</param>
        /// <param name="compraGiftcardsDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/>.</returns>
        Task<DatosSalida<CompraGiftcardsDataOutput>> ExecuteServiceAsync(
            string idOperador, CompraGiftcardsDataEntry compraGiftcardsDataEntry, IHeaderDictionary headerDictionary);
    }
}
