﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IInversionesService
    {
        /// <summary>
        /// Ejecuta servicios de Inversiones.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para realizar una inversió.
        /// </remarks>
        /// <param name="inversionesDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/></returns>
        Task<DatosSalida<InversionesDataOutput>> ExecuteServiceAsync(
            InversionesDataEntry inversionesDataEntry, IHeaderDictionary headerDictionary);
    }
}
