﻿using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IReversaAbonoService
    {
        Task<DatosSalida<ReversaAbonoDataOutput>> ExecuteServiceAsync(IHeaderDictionary headerDictionary);
    }
}
