﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IPagosEstatusBusquedasService
    {
        /// <summary>
        /// Ejecuta servicios de Apertura de Monedero.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para una apertura completa del Monedero.
        /// </remarks>
        /// <param name="pagoServiciosReferenciadosDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/></returns>
        Task<DatosSalida<PagosEstatusBusquedasDataOutput>> ExecuteServiceAsync(
            PagoServiciosReferenciadosDataEntry pagoServiciosReferenciadosDataEntry, IHeaderDictionary headerDictionary);
    }
}
