﻿using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IValidaAcumuladosService
    {
        Task<object> ExecuteServiceAsync(object input);
    }
}
