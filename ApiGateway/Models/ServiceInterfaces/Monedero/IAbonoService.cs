﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IAbonoService
    {
        /// <summary>
        /// Ejecuta servicios de Abono a Monedero.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para un Abono al Monedero.
        /// </remarks>
        /// <param name="abonoDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/>.</returns>
        Task<DatosSalida<AbonoDataOutput>> ExecuteServiceAsync(
            AbonoDataEntry abonoDataEntry, IHeaderDictionary headerDictionary);
    }
}
