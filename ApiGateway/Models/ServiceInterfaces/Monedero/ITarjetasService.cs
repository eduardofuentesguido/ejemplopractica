﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface ITarjetasService
    {
        /// <summary>
        /// Ejecuta servicios de Tarjetas Digitales.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para realizar una Tarjeta Digital.
        /// </remarks>
        /// <param name="tarjetaDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/></returns>
        Task<DatosSalida<TarjetaDataOutput>> ExecuteServiceAsync(
            TarjetaDataEntry tarjetaDataEntry, IHeaderDictionary headerDictionary);
    }
}
