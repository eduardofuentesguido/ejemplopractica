﻿using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IConsultaMonederoService
    {
        Task<object> ExecuteServiceAsync(/*ConsultaMonederoDataEntry consultaMonederoDataEntry*/);
    }
}
