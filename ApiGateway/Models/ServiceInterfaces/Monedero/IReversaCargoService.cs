﻿using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IReversaCargoService
    {
        Task<object> ExecuteServiceAsync(object input);
    }
}
