﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IPagosCanalesTercerosService
    {
        /// <summary>
        /// Ejecuta servicios de Pago de Transacciones.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para realizar Pago de Transacciones.
        /// </remarks>
        /// <param name="pagosCanalesTercerosDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/>.</returns>
        Task<DatosSalida<PagosCanalesTercerosDataOutput>> ExecuteServiceAsync(
            PagosCanalesTercerosDataEntry pagosCanalesTercerosDataEntry, IHeaderDictionary headerDictionary);
    }
}
