﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface ICargoService
    {
        /// <summary>
        /// Ejecuta servicios de Cargo a Monedero.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para un Cargo al Monedero.
        /// </remarks>
        /// <param name="cargoDataEntry"></param>
        /// <param name="headerDictionary"></param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/>.</returns>
        Task<DatosSalida<PagoTarjetasDataOutput>> ExecuteServiceAsync(
            CargoDataEntry cargoDataEntry, IHeaderDictionary headerDictionary);
    }
}
