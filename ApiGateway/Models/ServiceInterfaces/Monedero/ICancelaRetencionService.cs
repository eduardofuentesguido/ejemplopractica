﻿using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface ICancelaRetencionService
    {
        Task<object> ExecuteServiceAsync(object input);
    }
}
