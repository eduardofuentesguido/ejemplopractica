﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface ITarjetaBusquedaService
    {
        /// <summary>
        /// Ejecuta servicios de Busqueda de Tarjetas.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para realziar una busqueda de tarjeta.
        /// </remarks>
        /// <param name="tarjetaBusquedaDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/></returns>
        Task<DatosSalida<TarjetaBusquedaDataOutput>> ExecuteServiceAsync(
            TarjetaBusquedaDataEntry tarjetaBusquedaDataEntry, IHeaderDictionary headerDictionary);
    }
}
