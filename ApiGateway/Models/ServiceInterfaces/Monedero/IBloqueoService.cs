﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IBloqueoService
    {
        /// <summary>
        /// Ejecuta servicios de Bloqueo de Monedero.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para bloqueo de Monedero.
        /// </remarks>
        /// <param name="bloqueoMonederoDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/>.</returns>
        Task<DatosSalida<BloqueoMonederoDataOutput>> ExecuteServiceAsync(
            BloqueoMonederoDataEntry bloqueoMonederoDataEntry, IHeaderDictionary headerDictionary);
    }
}
