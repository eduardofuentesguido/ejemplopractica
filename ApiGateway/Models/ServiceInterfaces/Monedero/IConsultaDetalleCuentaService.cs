﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IConsultaDetalleCuentaService
    {
        /// <summary>
        /// Ejecuta servicios de Consulta detalle del Monedero.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para consulta del Monedero.
        /// </remarks>
        /// <param name="consultaDetalleCuentaDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/>.</returns>
        Task<DatosSalida<ConsultaDetalleCuentaDataOutput>> ExecuteServiceAsync(
            ConsultaDetalleCuentaDataEntry consultaDetalleCuentaDataEntry, IHeaderDictionary headerDictionary);
    }
}
