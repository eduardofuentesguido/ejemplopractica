﻿using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IRetencionService
    {
        Task<object> ExecuteServiceAsync(object input);
    }
}
