﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IRecargaTiempoAireService
    {
        /// <summary>
        /// Ejecuta servicios de Recarga Tiempo Aire.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para realizar una Recarga.
        /// </remarks>
        /// <param name="recargaTiempoAireDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/></returns>
        Task<DatosSalida<RecargaTiempoAireDataOutput>> ExecuteServiceAsync(
            RecargaTiempoAireDataEntry recargaTiempoAireDataEntry, IHeaderDictionary headerDictionary);
    }
}
