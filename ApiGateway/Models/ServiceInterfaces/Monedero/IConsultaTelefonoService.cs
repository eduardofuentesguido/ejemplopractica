﻿using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IConsultaTelefonoService
    {
        /// <summary>
        /// Ejecuta servicios de Consulta de Telefono.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para realizar una consulta de teléfono.
        /// </remarks>       
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/></returns>
        Task<DatosSalida<ConsultaTelfonoDataOutput>> ExecuteServiceAsync(IHeaderDictionary headerDictionary);
    }
}
