﻿using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface ILiberaRetencionService
    {
        Task<object> ExecuteServiceAsync(object input);
    }
}
