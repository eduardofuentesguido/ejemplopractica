﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface IConsultaBloqueosService
    {
        /// <summary>
        /// Ejecuta servicios de Bloqueo de Monedero.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para realizar una consulta de Bloqueo de Monedero.
        /// </remarks>
        /// <param name="consultaBloqueosDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/></returns>
        Task<DatosSalida<ConsultaBloqueosDataOutput>> ExecuteServiceAsync(
            ConsultaBloqueosDataEntry consultaBloqueosDataEntry, IHeaderDictionary headerDictionary);
    }
}
