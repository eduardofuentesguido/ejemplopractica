﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public interface ITraspasosBotonPagoService
    {
        /// <summary>
        /// Ejecuta servicios de Traspasos mediante Botón de pago.
        /// </summary>
        /// <remarks>
        /// En este método se define toda la lógica que tendrá nuestro servicio para realizar un Traspaso mediante Boton Pago.
        /// </remarks>
        /// <param name="traspasoBotonPagoDataEntry">Objeto que contiene toda la cadena de entrada.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <returns>Devuelve un objeto <see cref="DatosSalida{TDataOutput}"/></returns>
        Task<DatosSalida<TraspasoBotonPagoDataOutput>> ExecuteServiceAsync(
            TraspasoBotonPagoDataEntry traspasoBotonPagoDataEntry, IHeaderDictionary headerDictionary);
    }
}
