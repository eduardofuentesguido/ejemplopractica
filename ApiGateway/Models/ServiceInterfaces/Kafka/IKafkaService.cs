﻿using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Kafka
{
    public interface IKafkaService
    {
        /// <summary>
        /// Envia mensaje para su registro en Kafka.
        /// </summary>
        /// <param name="message">Mensaje a registrar en Kafka.</param>
        /// <returns>Devuelve un objeto <see cref="Task"/></returns>
        Task SendMessageAsync(string message);
    }
}
