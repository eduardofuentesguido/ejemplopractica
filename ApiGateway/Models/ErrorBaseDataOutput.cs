﻿using System.Collections.Generic;

namespace ApiGateway.Models
{
    public class ErrorBaseDataOutput
    {
        public string Codigo { get; set; }
        public string Mensaje { get; set; }
        public string Folio { get; set; }
        public string Info { get; set; }
        public List<string> Detalles { get; set; }
    }
}
