﻿using ApiGateway.Models.Microservices.DataEntry;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.Microservices.Interfaces
{
    /// <summary>
    /// Registro de métodos para llamar a todos los Microservicios del móduo de Monedero.
    /// <remarks>
    /// El registro de cada método representa una solicitud Http para consumir el Microservicio al que pertenece.
    /// </remarks>
    /// </summary>
    public interface IMonederoMicroservices
    {
        #region Operaciones de Monedero
        /// <summary>
        /// Ejecuta el servicio para crear una Apertura de Monedero.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Apertura de Monedero.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> AperturaAsync(DatosSuperCore<MSAperturaDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para hacer un Bloqueo de Monedero.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Bloqueo de Monedero.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> BloqueoAsync(DatosSuperCore<MSBloqueoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para hacer un Desbloqueo de Monedero.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Desbloqueo de Monedero.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> DesbloqueoAsync(DatosSuperCore<MSDesbloqueDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para hacer una Cancelación de Monedero.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Cancelación de Monedero.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> CancelacionAsync(DatosSuperCore<MSCancelacionDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para modificar el Teléfono asociado al Monedero.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Modifica Teléfono.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ModificaTelefonoAsync(DatosSuperCore<MSModificaTelefonoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);
        #endregion

        #region Consultas Genericas

        /// <summary>
        /// Ejecuta el servicio para hacer una Consulta de Monedero.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Consulta de Monedero.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ConsultaMonederoAsync(DatosSuperCore<MSConsultaMonederoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para hacer una Consulta de Monedero por medio del Teléfono.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Consulta de Monedero.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ConsultaMonederoAsync(DatosSuperCore<MSConsultaMonederoTelefonoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para hacer una Consulta de Teléfono.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Consulta de Teléfono.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ConsultaTelefonoAsync(DatosSuperCore<MSConsultaTelefonoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para hacer una Consulta del Bloqueo del Monedero.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Consulta de Bloqueo.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ConsultaBloqueoAsync(DatosSuperCore<MSConsultaBloqueoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);
        #endregion

        #region Consultas de Saldo
        /// <summary>
        /// Ejecuta el servicio para hacer una Consulta de Saldo.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Consulta de Saldo.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ConsultaSaldoAsync(DatosSuperCore<MSConsultaSaldoMonederoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para hacer una Consulta de Saldo por medio del SICU.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Consulta de Saldo.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ConsultaSaldoSicuAsync(DatosSuperCore<MSConsultaSaldoSicuDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para hacer una Consulta de Saldo por medio del SICU.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Consulta de Saldo.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ConsultaMonederoSicuAsync(DatosSuperCore<MSConsultaDetalleCuentaDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);
        #endregion

        #region Consultas de Movimientos
        /// <summary>
        /// Ejecuta el servicio para hacer una Consulta de Movimientos del Monedero.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Consulta de Movimientos.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ConsultaMovimientosAsync(DatosSuperCore<MSConsultaMovimientosDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);
        #endregion

        #region Validacions
        /// <summary>
        /// Ejecuta el servicio para hacer una Validación de Acumulados.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de Consulta de Valida Acumulado.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ValidaAcumuladoAsync(DatosSuperCore<MSValidaAcumuladoDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);
        #endregion
    }
}
