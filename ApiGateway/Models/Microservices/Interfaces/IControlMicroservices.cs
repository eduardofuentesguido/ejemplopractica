﻿using ApiGateway.Models.Microservices.DataEntry;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.Microservices.Interfaces
{
    /// <summary>
    /// Registro de métodos para llamar a todos los Microservicios del móduo de Monedero.
    /// <remarks>
    /// El registro de cada método representa una solicitud Http para consumir el Microservicio al que pertenece.
    /// </remarks>
    /// </summary>
    public interface IControlMicroservices
    {
        /// <summary>
        /// Ejecuta el servicio de CAM.
        /// </summary>
        /// <param name="datosSuperCore">Objeto de entrada del Microservicio.</param>
        /// <remarks>
        /// Ejecuta el Microservicio de CAM.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ExecuteCamAsync(DatosSuperCore<MSCamDataEntry> datosSuperCore, Dictionary<string, string> headerDictionary);
    }
}
