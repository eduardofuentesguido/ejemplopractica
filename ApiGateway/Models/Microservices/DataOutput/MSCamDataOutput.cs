﻿using System.Collections.Generic;

namespace ApiGateway.Models.Microservices.DataOutput
{
    public class MSCamDataOutput
    {
        public MSCamDataOutputDatosSalida DatosSalida { get; set; }
    }

    public class MSCamDataOutputDatosSalida
    {
        public List<MSCamDataOutputMovimientos> Movimientos { get; set; }
    }

    public class MSCamDataOutputMovimientos
    {
        public string NumeroMovimiento { get; set; }
        public string Operacion { get; set; }
        public string ConceptoMovimiento { get; set; }
        public string CuentaRelacionada { get; set; }
        public string SaldoActual { get; set; }
    }
}
