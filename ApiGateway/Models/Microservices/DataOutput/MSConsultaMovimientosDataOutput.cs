﻿using System.Collections.Generic;

namespace ApiGateway.Models.Microservices.DataOutput
{
    public class MSConsultaMovimientosDataOutput
    {
        public MSConsultaMovimientosDataOutputDatosSalida DatosSalida { get; set; }
    }

    public class MSConsultaMovimientosDataOutputDatosSalida
    {        
        public string Monedero { get; set; }
        public string Producto { get; set; }
        public string TitularMonedero { get; set; }
        public string FechaConsulta { get; set; }
        public string HoraConsulta { get; set; }
        public List<DetalleDeMovimientos> Movimientos { get; set; } = new List<DetalleDeMovimientos>();
    }

    public class DetalleDeMovimientos
    {
        public string MontoOperacion { get; set; }
        public string SaldoAnterior { get; set; }
        public string ConceptoMovimiento { get; set; }
        public string FechaRegistro { get; set; }
        public string NumeroMovimiento { get; set; }
        public string CodigoDivisa { get; set; }
    }
}
