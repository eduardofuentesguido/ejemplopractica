﻿namespace ApiGateway.Models.Microservices.DataOutput
{
    public class MSConsultaSaldoMonederoDataOutput
    {
        public int SaldoDisponible { get; set; }
        public string DesDivisa { get; set; }
        public string TitularMonedero { get; set; }
        public int IdMonedero { get; set; }
        public string DesTipoMonedero { get; set; }
        public int NumeroOperacion { get; set; }
        public string FechahoraOperacion { get; set; }
        public double TiempoOBDT { get; set; }
    }
}
