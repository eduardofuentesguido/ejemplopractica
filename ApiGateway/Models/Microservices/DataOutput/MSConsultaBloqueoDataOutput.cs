﻿using System.Collections.Generic;

namespace ApiGateway.Models.Microservices.DataOutput
{
    public class MSConsultaBloqueoDataOutput
    {
        public MSConsultaBloqueoDataOutputDatosSalida DatosSalida { get; set; }
    }

    public class MSConsultaBloqueoDataOutputDatosSalida
    {
        public List<MSConsultaBloqueoDataOutputBloqueo> Bloqueos { get; set; }
    }

    public class MSConsultaBloqueoDataOutputBloqueo
    {
        public string TipoOperacion { get; set; }
        public string DesTipoOperacion { get; set; }
        public string ConceptoBloqueo { get; set; }
    }
}
