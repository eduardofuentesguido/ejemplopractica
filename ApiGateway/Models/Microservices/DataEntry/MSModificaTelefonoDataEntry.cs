﻿namespace ApiGateway.Models.Microservices.DataEntry
{
    public class MSModificaTelefonoDataEntry
    {
        public string NumTelefono { get; set; }
        public string SICU { get; set; }
    }
}
