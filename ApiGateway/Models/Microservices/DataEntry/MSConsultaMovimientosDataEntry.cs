﻿namespace ApiGateway.Models.Microservices.DataEntry
{
    public class MSConsultaMovimientosDataEntry
    {
        public string Monedero { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
    }
}
