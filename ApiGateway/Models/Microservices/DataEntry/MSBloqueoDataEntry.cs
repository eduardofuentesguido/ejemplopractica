﻿namespace ApiGateway.Models.Microservices.DataEntry
{
    public class MSBloqueoDataEntry
    {
        public int IdMonedero { get; set; }
        public string CodigoBloqueo { get; set; }
        public string Concepto { get; set; }
    }
}
