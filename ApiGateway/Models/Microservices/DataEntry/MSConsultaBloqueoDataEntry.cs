﻿namespace ApiGateway.Models.Microservices.DataEntry
{
    public class MSConsultaBloqueoDataEntry
    {
        public string Empresa { get; set; }
        public int IdMonedero { get; set; }
    }
}
