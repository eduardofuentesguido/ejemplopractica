﻿using System.Collections.Generic;

namespace ApiGateway.Models.Microservices.DataEntry
{
    public class MSCamDataEntry
    {
        public string UUIDR { get; set; }
        public string TipoOperacion { get; set; }
        public IList<string> Referencias { get; set; }
        public IList<decimal> Montos { get; set; }
        public IList<int> Monederos { get; set; }
    }
}
