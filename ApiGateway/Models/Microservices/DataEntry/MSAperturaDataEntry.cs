﻿namespace ApiGateway.Models.Microservices.DataEntry
{
    public class MSAperturaDataEntry
    {
        public string IdSICU { get; set; }
        public int IdTipoMonedero { get; set; }
        public int IdSubTipoMonedero { get; set; }
        public string CuentaInstrumental { get; set; }
        public string CuentaClabe { get; set; }
        public string Nombre { get; set; }
        public string ApePaterno { get; set; }
        public string ApeMaterno { get; set; }
        public string IdGenero { get; set; }
        public string FechaNacimiento { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
    }
}