﻿namespace ApiGateway.Models.Microservices.DataEntry
{
    public class MSCancelacionDataEntry
    {
        public int IdMonedero { get; set; }
        public string Centro { get; set; }
    }
}
