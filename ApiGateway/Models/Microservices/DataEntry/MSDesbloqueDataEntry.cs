﻿namespace ApiGateway.Models.Microservices.DataEntry
{
    public class MSDesbloqueDataEntry
    {
        public int IdMonedero { get; set; }
        public string TipoOperacion { get; set; }
    }
}
