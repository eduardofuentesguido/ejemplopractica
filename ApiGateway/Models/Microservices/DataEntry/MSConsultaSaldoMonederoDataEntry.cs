﻿namespace ApiGateway.Models.Microservices.DataEntry
{
    public class MSConsultaSaldoMonederoDataEntry
    {
        public int Empresa { get; set; }
        public int IdMonedero { get; set; }
    }
}
