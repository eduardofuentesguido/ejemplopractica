﻿using ApiGateway.Models.Settings;

namespace ApiGateway.Models.Microservices.Settings
{
    public class MonederoSettings
    {
        public class AbonoSettings : MicroserviceBaseSettings { }
        public class AperturaSettings : MicroserviceBaseSettings { }
        public class BloqueoSettings : MicroserviceBaseSettings { }
        public class CancelacionSettings : MicroserviceBaseSettings { }
        public class CancelaRetencionSettings : MicroserviceBaseSettings { }
        public class CargoSettings : MicroserviceBaseSettings { }
        public class ConsultaBloqueosSettings : MicroserviceBaseSettings { }
        public class ConsultaMonederoSettings : MicroserviceBaseSettings { }
        public class ConsultaMonederoTelefonoSettings : MicroserviceBaseSettings { }
        public class ConsultaMovimientosSettings : MicroserviceBaseSettings { }
        public class ConsultaSaldoSettings : MicroserviceBaseSettings { }
        public class ConsultaSaldoSicuSettings : MicroserviceBaseSettings { }
        public class ConsultaTelefonoSettings : MicroserviceBaseSettings { }
        public class DesbloqueoSettings : MicroserviceBaseSettings { }
        public class LiberaRetencionSettings : MicroserviceBaseSettings { }
        public class ModificaTelefonoSettings : MicroserviceBaseSettings { }
        public class RetencionSettings : MicroserviceBaseSettings { }
        public class ReversaAbonoSettings : MicroserviceBaseSettings { }
        public class ReversaCargoSettings : MicroserviceBaseSettings { }
        public class ValidaAcumuladosSettings : MicroserviceBaseSettings { }
    }
}
