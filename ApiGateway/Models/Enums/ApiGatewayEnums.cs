﻿namespace ApiGateway.Models.Enums
{
    /// <summary>
    /// The api gateway enums.
    /// </summary>
    public static class ApiGatewayEnums
    {
        /// <summary>
        /// Ambiente a Ejecutar.
        /// </summary>
        public enum EnterpriseSystem
        {
            OnPremise,
            Microservices
        }

        /// <summary>
        /// Switch de ejecición de los servicios.
        /// </summary>
        public enum SwitchExecution
        {
            //Solo ejecuta los servicios de Alnova
            SoloAlnova = 0,
            //Ejecuta los servicios de Alnova y SuperCore de forma Asincrona.
            AlnovaSuperCoreAsync,
            //Ejecuta los servicios de Alnova y SuperCore de forma Sincrona.
            AlnovaSuperCoreSync,
            //Ejecuta los servicios de SuperCore y después los de Alnova.
            SuperCoreAlnova
        }
    }
}
