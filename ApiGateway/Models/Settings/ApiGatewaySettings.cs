﻿namespace ApiGateway.Models.Settings
{
    /// <summary>
    /// Configuracion del ApiGateway
    /// </summary>
    public class ApiGatewaySettings
    {
        public CodigosRespuesta CodigosRespuesta { get; set; }
    }

    public class Errores
    {
        public string ECTL001 { get; set; }
        public string ECTL002 { get; set; }
    }

    public class Avisos
    {
        public string ACTL001 { get; set; }
    }

    public class CodigosRespuesta
    {
        public Errores Errores { get; set; }
        public Avisos Avisos { get; set; }
    }
}
