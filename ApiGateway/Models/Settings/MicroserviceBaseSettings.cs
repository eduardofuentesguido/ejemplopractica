﻿namespace ApiGateway.Models.Settings
{
    public abstract class MicroserviceBaseSettings
    {
        public string Query { get; set; }
        public string BaseUri { get; set; }
        public string RequestUri { get; set; }
        public string AuthenticationSchema { get; set; }
        public string AcceptHeaderValue { get; set; }
        public string ContentTypeHeaderValue { get; set; }
    }
}
