﻿namespace ApiGateway.Models.Settings
{
    public class KafkaSettings
    {
        public string Topic { get; set; }
        public int FlushTime { get; set; }
        public string BootstrapServers { get; set; }
    }
}
