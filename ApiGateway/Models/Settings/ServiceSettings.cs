﻿using static ApiGateway.Models.Enums.ApiGatewayEnums;

namespace ApiGateway.Models.Settings
{
    /// <summary>
    /// Servicios el Orquestador.
    /// </summary>
    public class ServiceSettings
    {
        /// <summary>
        /// Obtiene o establece la configuración para el servicio de Apertura de Monedero.
        /// </summary>
        public SwitchExecution AperturaMonedero { get; set; }
        /// <summary>
        /// Obtiene o establece la configuración para el servicio de Consulta de Movimienots del Monedero.
        /// </summary>
        public SwitchExecution ConsultaMovimientos { get; set; }
        /// <summary>
        /// Obtiene o establece la configuración para el servicio de Consulta de Saldo del Monedero.
        /// </summary>
        public SwitchExecution ConsultaSaldo { get; set; }
        /// <summary>
        /// Obtiene o establece la configuración para el servicio de Traspaso entre Cuentas de Monedero.
        /// </summary>
        public SwitchExecution TraspasoEntreCuentas { get; set; }
    }
}
