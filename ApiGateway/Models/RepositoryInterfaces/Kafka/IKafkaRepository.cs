﻿using System;
using System.Threading.Tasks;

namespace ApiGateway.Models.RepositoryInterfaces.Kafka
{
    /// <summary>
    /// Repositorio de Kafka.
    /// </summary>
    public interface IKafkaRepository
    {
        /// <summary>
        /// Envia un mensaje al servicio de Kafka.
        /// </summary>
        /// <param name="message"></param>
        /// <returns>El método devuelve una Excepcion.</returns>
        Task<Exception> SendMessageAsync(string message);
    }
}
