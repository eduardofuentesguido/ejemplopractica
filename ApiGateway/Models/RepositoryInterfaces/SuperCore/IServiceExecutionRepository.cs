﻿using ApiGateway.Models.Settings;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using static ApiGateway.Models.Enums.ApiGatewayEnums;

namespace ApiGateway.Models.RepositoryInterfaces.SuperCore
{
    /// <summary>
    /// Interfaz para llamar a los servicios mediante un método Http.
    /// </summary>
    public interface IServiceExecutionRepository
    {
        /// <summary>
        /// Método para llamar a un servicio mediante el método POST.
        /// </summary>
        /// <typeparam name="TSettings"><see cref="MicroserviceBaseSettings"/></typeparam>
        /// <param name="settings">Objeto de configuraciones basadas en <see cref="MicroserviceBaseSettings"/>.</param>
        /// <param name="input">Objeto Body del servicio a llamar.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <param name="enterpriseSystem">Objeto que define tipo de sistema a ejecutar.</param>
        /// <returns>El método devuelve un objeto <see cref="HttpResponseMessage"/>.</returns>
        Task<HttpResponseMessage> ExecutePostAsync<TSettings>(TSettings settings, object input, Dictionary<string, string> headerDictionary, EnterpriseSystem enterpriseSystem) where TSettings : MicroserviceBaseSettings;

        /// <summary>
        /// Método para llamar a un servicio mediante el método GET.
        /// </summary>
        /// <typeparam name="TSettings"><see cref="MicroserviceBaseSettings"/></typeparam>
        /// <param name="settings">Objeto de configuraciones basadas en <see cref="MicroserviceBaseSettings"/>.</param>
        /// <param name="input">Objeto Body del servicio a llamar.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <param name="enterpriseSystem">Objeto que define tipo de sistema a ejecutar.</param>
        /// <returns>El método devuelve un objeto <see cref="HttpResponseMessage"/>.</returns>
        Task<HttpResponseMessage> ExecuteGetAsync<TSettings>(TSettings settings, object input, Dictionary<string, string> headerDictionary, EnterpriseSystem enterpriseSystem) where TSettings : MicroserviceBaseSettings;

        /// <summary>
        /// Método para llamar a un servicio mediante el método PUT.
        /// </summary>
        /// <typeparam name="TSettings"><see cref="MicroserviceBaseSettings"/></typeparam>
        /// <param name="settings">Objeto de configuraciones basadas en <see cref="MicroserviceBaseSettings"/>.</param>
        /// <param name="input">Objeto Body del servicio a llamar.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <param name="enterpriseSystem">Objeto que define tipo de sistema a ejecutar.</param>
        /// <returns>El método devuelve un objeto <see cref="HttpResponseMessage"/>.</returns>
        Task<HttpResponseMessage> ExecutePutAsync<TSettings>(TSettings settings, object input, Dictionary<string, string> headerDictionary, EnterpriseSystem enterpriseSystem) where TSettings : MicroserviceBaseSettings;

        /// <summary>
        /// Método para llamar a un servicio mediante el método PATCH.
        /// </summary>
        /// <typeparam name="TSettings"><see cref="MicroserviceBaseSettings"/></typeparam>
        /// <param name="settings">Objeto de configuraciones basadas en <see cref="MicroserviceBaseSettings"/>.</param>
        /// <param name="input">Objeto Body del servicio a llamar.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <param name="enterpriseSystem">Objeto que define tipo de sistema a ejecutar.</param>
        /// <returns>El método devuelve un objeto <see cref="HttpResponseMessage"/>.</returns>
        Task<HttpResponseMessage> ExecutePatchAsync<TSettings>(TSettings settings, object input, Dictionary<string, string> headerDictionary, EnterpriseSystem enterpriseSystem) where TSettings : MicroserviceBaseSettings;

        /// <summary>
        /// Método para llamar a un servicio mediante el método DELETE.
        /// </summary>
        /// <typeparam name="TSettings"><see cref="MicroserviceBaseSettings"/></typeparam>
        /// <param name="settings">Objeto de configuraciones basadas en <see cref="MicroserviceBaseSettings"/>.</param>
        /// <param name="input">Objeto Body del servicio a llamar.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <param name="enterpriseSystem">Objeto que define tipo de sistema a ejecutar.</param>
        /// <returns>El método devuelve un objeto <see cref="HttpResponseMessage"/>.</returns>
        Task<HttpResponseMessage> ExecuteDeleteAsync<TSettings>(TSettings settings, object input, Dictionary<string, string> headerDictionary, EnterpriseSystem enterpriseSystem) where TSettings : MicroserviceBaseSettings;
    }
}
