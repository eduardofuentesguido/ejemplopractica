﻿using System;

namespace ApiGateway.Models
{
    public class OperationException : Exception
    {
        public string Code { get; }

        public OperationException() { }

        public OperationException(string message)
        : base(message) { }

        public OperationException(string message, Exception inner)
            : base(message, inner) { }

        public OperationException(string message, string code)
            : this(message)
        {
            Code = code;
        }
    }
}
