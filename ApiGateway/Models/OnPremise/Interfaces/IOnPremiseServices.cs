﻿using ApiGateway.Models.ControllerDataEntry;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiGateway.Models.OnPremise.Interfaces
{
    public interface IOnPremiseServices
    {
        /// <summary>
        /// Ejecuta el servicio para crear una Apertura de Monedero en OnPremise.
        /// </summary>
        /// <param name="aperturaDataEntry">Objeto de entrada del Microservicio.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <remarks>
        /// Ejecuta el Servicio de Apertura de Monedero en OnPremise.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> AperturaAsync(AperturaDataEntry aperturaDataEntry, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para consultar los Movimientos del Monedero en OnPremise.
        /// </summary>
        /// <param name="consultaMovimientosDataEntry">Objeto de entrada del Microservicio.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <remarks>
        /// Ejecuta el Servicio de Consulta de Movimientos del Monedero en OnPremise.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ConsultaMovimientosAsync(ConsultaMovimientosDataEntry consultaMovimientosDataEntry, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para Consulta de Saldo por Monedero en OnPremise.
        /// </summary>
        /// <param name="consultaSaldoSicuDataEntry">Objeto de entrada del Microservicio.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <remarks>
        /// Ejecuta el Servicio de Consulta de Saldo por Monedero en OnPremise.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> ConsultaSaldoAsync(ConsultaSaldoSicuDataEntry consultaSaldoSicuDataEntry, Dictionary<string, string> headerDictionary);

        /// <summary>
        /// Ejecuta el servicio para realizar un Traspaso entre Monederos en OnPremise.
        /// </summary>
        /// <param name="traspasoCuentasDataEntry">Objeto de entrada del Microservicio.</param>
        /// <param name="headerDictionary">Diccionario de Headers.</param>
        /// <remarks>
        /// Ejecuta el Servicio de Traspaso entre Monederos en OnPremise.
        /// </remarks>
        /// <returns>El método devuelve un HttpResponseMessage.</returns>
        Task<HttpResponseMessage> TraspasoCuentasAsync(TraspasoCuentasDataEntry traspasoCuentasDataEntry, Dictionary<string, string> headerDictionary);
    }
}
