﻿using ApiGateway.Models.Settings;

namespace ApiGateway.Models.OnPremise.Settings
{
    public class OnPremiseSettings
    {
        public class OnPremAperturaSettings : MicroserviceBaseSettings { }

        public class OnPremConsultaSaldoSettings : MicroserviceBaseSettings { }

        public class OnPremConsultaMovimientosSettings : MicroserviceBaseSettings { }

        public class OnPremTraspasoCuentasSettings : MicroserviceBaseSettings { }
    }
}
