﻿namespace ApiGateway.Models.OnPremise.Settings
{
    public class AuthOnPremise
    {
        public string BaseUri { get; set; }
        public string RequestUri { get; set; }
        public string GrantType { get; set; }
        public string ContentType { get; set; }
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
    }
}
