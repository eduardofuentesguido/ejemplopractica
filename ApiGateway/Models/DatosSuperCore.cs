﻿namespace ApiGateway.Models
{
    public class DatosSuperCore<TDatosEntrada> where TDatosEntrada : class
    {
        public DatosControl DatosControl { get; set; }
        public TDatosEntrada DatosEntrada { get; set; }
        public DatosExtendidos DatosExtendidos { get; set; }
    }

    public class DatosControl
    {
        public string Id { get; set; }
        public string Canal { get; set; }
        public string Centro { get; set; }
        public string Empresa { get; set; }
        public string Usuario { get; set; }
    }

    public class DatosExtendidos
    {
        public string TokenSrvCom { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string UUID { get; set; }
        public decimal TiempoOrquestador { get; set; }
    }
}
