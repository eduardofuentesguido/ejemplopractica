﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class ConsultaDetalleCuentaDataEntry
    {
        public string PrimerTokenVerificacion { get; set; }
        [Required]
        public string Sicu { get; set; }

        public string MontoCargo { get; set; }
        public string MontoAbono { get; set; }

    }
}
