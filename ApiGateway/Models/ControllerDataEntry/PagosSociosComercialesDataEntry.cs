﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class PagosSociosComercialesDataEntry
    {
        [Required]
        public PagosSociosComercialesDataEntryTransaccion Transaccion { get; set; }
        public string CodigoBarras { get; set; }

        [Required]
        public string Nip { get; set; }

        [Required]
        public string Monto { get; set; }
    }

    public class PagosSociosComercialesDataEntryTransaccion
    {
        [Required]
        public PagosSociosComercialesDataEntryOperador Operador { get; set; }

        [Required]
        public string NumeroTienda { get; set; }

        [Required]
        public string Terminal { get; set; }

        [Required]
        public string NumeroTransaccion { get; set; }
    }

    public class PagosSociosComercialesDataEntryOperador
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Nombre { get; set; }
    }
}
