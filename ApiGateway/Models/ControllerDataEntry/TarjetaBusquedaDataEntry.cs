﻿namespace ApiGateway.Models.ControllerDataEntry
{
    public class TarjetaBusquedaDataEntry
    {
        public string NumeroCuenta { get; set; }
        public string Sicu { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }
}
