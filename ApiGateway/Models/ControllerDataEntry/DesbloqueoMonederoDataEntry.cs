﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class DesbloqueoMonederoDataEntry
    {
        [Required]
        public string NumeroMonedero { get; set; }

        [Required]
        public string TipoOperacion { get; set; }
    }
}
