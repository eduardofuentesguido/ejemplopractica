﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class PagosCanalesTercerosDataEntry
    {
        public string CodigoBarras { get; set; }
        public string CuentaCargo { get; set; }
        public string Pais { get; set; }

        [Required]
        public string Monto { get; set; }

        [Required]
        public PagosCanalesTercerosDataEntryTransaccion Transaccion { get; set; }
    }

    public class PagosCanalesTercerosDataEntryTransaccion
    {
        [Required]
        public PagosCanalesTercerosDataEntryOperador Operador { get; set; }

        [Required]
        public string NumeroTienda { get; set; }

        [Required]
        public string Terminal { get; set; }

        [Required]
        public string NumeroTransaccion { get; set; }
    }

    public class PagosCanalesTercerosDataEntryOperador
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Nombre { get; set; }
    }
}
