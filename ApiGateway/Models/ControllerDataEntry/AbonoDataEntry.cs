﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class AbonoDataEntry
    {
        [Required]
        public string PrimerTokenVerificacion { get; set; }

        [Required]
        public string SegundoTokenVerificacion { get; set; }
        public Geolocalizacion Geolocalizacion { get; set; }
        public AbonoDataEntryTransaccion Transaccion { get; set; }
    }

    public class AbonoDataEntryTransaccion
    {
        [Required]
        public string NumeroCuenta { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public string FechaOperacion { get; set; }

        [Required]
        public string CodigoDivisa { get; set; }

        public Abono Abono { get; set; }
    }

    public class Abono
    {
        [Required]
        public string Codigo { get; set; }

        [Required]
        public decimal Importe { get; set; }

        [Required]
        public string Folio { get; set; }

        public string Referencia { get; set; }

        [Required]
        public string Observaciones { get; set; }
    }
}