﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class InversionesDataEntry
    {
        [Required]
        public string CodigoDivisa { get; set; }
    }
}
