﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class CargoAbonoDataEntry
    {
        public string PrimerTokenVerificacion { get; set; }

        [Required]
        public string SegundoTokenVerificacion { get; set; }

        [Required]
        public Geolocalizacion Geolocalizacion { get; set; }
        public CargoAbonoDataEntryTransaccion Transaccion { get; set; }
    }

    public class CargoAbonoDataEntryTransaccion
    {
        public string NumeroCuenta { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public string FechaOperacion { get; set; }

        public string CodigoDivisa { get; set; }
        public CargoAbonoDataEntryAbono Abono { get; set; }
        public CargoAbonoDataEntryCargo Cargo { get; set; }
    }

    public class CargoAbonoDataEntryAbono
    {
        public string CodigoAbono { get; set; }
        public string Importe { get; set; }
        public string Folio { get; set; }
    }

    public class CargoAbonoDataEntryCargo
    {
        public string CodigoCargo { get; set; }
        public string Importe { get; set; }
        public string Folio { get; set; }
    }

}
