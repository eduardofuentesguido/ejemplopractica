﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class TransferenciaDataEntry
    {
        public string PrimerTokenVerificacion { get; set; }

        [Required]
        public string SegundoTokenVerificacion { get; set; }
        public string CuentaOrigen { get; set; }
        public bool AltaCuentaFrecuente { get; set; }
        public bool OperacionCuentaFrecuente { get; set; }

        [Required]
        public Geolocalizacion Geolocalizacion { get; set; }
        public TransferenciaDataEntryBeneficiario Beneficiario { get; set; }
        public Transferencia Transferencia { get; set; }
    }

    public class Transferencia
    {
        [Required]
        public string Monto { get; set; }

        [Required]
        public string Comisiones { get; set; }

        [Required]
        public string Concepto { get; set; }
        public string Referencia { get; set; }
    }

    public class TransferenciaDataEntryBeneficiario
    {
        [Required]
        public string IdCliente { get; set; }

        [Required]
        public string DatoBancario { get; set; }

        [Required]
        public string Nombre { get; set; }
        public string Alias { get; set; }

        [Required]
        public string NombreBanco { get; set; }

        [Required]
        public string CodigoBanco { get; set; }
    }
}
