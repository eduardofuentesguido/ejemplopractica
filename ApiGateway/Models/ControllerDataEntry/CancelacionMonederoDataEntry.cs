﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class CancelacionMonederoDataEntry
    {
        [Required]
        public string NumeroMonedero { get; set; }

        [Required]
        public string Centro { get; set; }
    }
}
