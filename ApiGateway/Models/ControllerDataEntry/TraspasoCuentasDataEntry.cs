﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class TraspasoCuentasDataEntry
    {
        public string PrimerTokenVerificacion { get; set; }

        [Required]
        public string SegundoTokenVerificacion { get; set; }
        public bool AltaCuentaFrecuente { get; set; }
        public bool OperacionCuentaFrecuente { get; set; }

        [Required]
        public Geolocalizacion Geolocalizacion { get; set; }
        public TraspasoCuentasDataEntryBeneficiario Beneficiario { get; set; }
        public TraspasoCuentasDataEntryTraspaso Traspaso { get; set; }
    }

    public class TraspasoCuentasDataEntryBeneficiario
    {
        [Required]
        public string IdCliente { get; set; }

        [Required]
        public string DatoBancario { get; set; }
        public string Nombre { get; set; }
        public string Alias { get; set; }
    }

    public class TraspasoCuentasDataEntryTraspaso
    {
        public decimal Monto { get; set; }
        public int CuentaOrigen { get; set; }
        public string CodigoDivisa { get; set; }
        public string Concepto { get; set; }
    }
}
