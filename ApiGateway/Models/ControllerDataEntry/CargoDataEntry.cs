﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class CargoDataEntry
    {
        [Required]
        public string PrimerTokenVerificacion { get; set; }

        [Required]
        public string SegundoTokenVerificacion { get; set; }

        [Required]
        public Geolocalizacion Geolocalizacion { get; set; }
        public CargoDataEntryTransaccion Transaccion { get; set; }
    }

    public class CargoDataEntryTransaccion
    {
        [Required]
        public string NumeroCuenta { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public string FechaOperacion { get; set; }

        [Required]
        public string CodigoDivisa { get; set; }
        public CargoDataEntryCargo Cargo { get; set; }
    }

    public class CargoDataEntryCargo
    {
        [Required]
        public string Codigo { get; set; }

        [Required]
        public string Importe { get; set; }

        [Required]
        public string Folio { get; set; }
        public string Referencia { get; set; }

        [Required]
        public string Observaciones { get; set; }
    }
}
