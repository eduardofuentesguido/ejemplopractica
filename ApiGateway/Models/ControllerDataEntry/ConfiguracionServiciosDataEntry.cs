﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    /// <summary>
    /// Datos de entrada para Actualiza la Configuracion del Orquestador.
    /// </summary>
    public class ConfiguracionServiciosDataEntry
    {
        /// <summary>
        /// Obtiene o establece el nombre de la sección.
        /// </summary>
        [Required]
        public string NombreServicio { get; set; }
        /// <summary>
        /// Obtiene o establece el valor de la sección.
        /// </summary>
        [Required]
        [Range(0, 3)]
        public string ModoEjecucion { get; set; }
    }
}
