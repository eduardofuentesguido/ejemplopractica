﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class ModificaTelfonoDataEntry
    {
        [Required]
        public string NumeroTelefono { get; set; }
    }
}
