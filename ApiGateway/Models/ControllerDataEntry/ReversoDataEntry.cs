﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class ReversoDataEntry
    {
        [Required]
        public string UUIDR { get; set; }

        [Required]
        public string TipoOperacion { get; set; }

        public Geolocalizacion Geolocalizacion { get; set; }
    }
}
