﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class ConsultaSaldoSicuDataEntry
    {
        [Required]
        public string Folio { get; set; }
        public string NumeroCuenta { get; set; }
    }
}
