﻿using ApiGateway.Attributes;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class Geolocalizacion
    {
        [Encrypt]
        [Decrypt]
        public string Latitud { get; set; }

        [Encrypt]
        [Decrypt]
        public string Longitud { get; set; }
    }
}
