﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class TraspasoTelefonoDataEntry
    {
        public string PrimerTokenVerificacion { get; set; }

        [Required]
        public string SegundoTokenVerificacion { get; set; }

        [Required]
        public string NumeroCuentaRetiro { get; set; }
        public bool AltaCuentaFrecuente { get; set; }
        public bool OperacionCuentaFrecuente { get; set; }

        [Required]
        public Geolocalizacion Geolocalizacion { get; set; }
        public TraspasoTelefonoDataEntryBeneficiario Beneficiario { get; set; }
        public TraspasoTelefonoDataEntryTraspaso Traspaso { get; set; }
    }

    public class TraspasoTelefonoDataEntryBeneficiario
    {
        [Required]
        public string IdCliente { get; set; }

        [Required]
        public string DatoBancario { get; set; }
        public string Nombre { get; set; }
        public string Alias { get; set; }
    }

    public class TraspasoTelefonoDataEntryTraspaso
    {
        public string NumeroTelefonoOrigen { get; set; }

        [Required]
        public string Monto { get; set; }
        public string Concepto { get; set; }
    }
}
