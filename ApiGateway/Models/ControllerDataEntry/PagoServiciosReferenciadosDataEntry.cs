﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{

    public class PagoServiciosReferenciadosDataEntry
    {
        public DatosGeneralesTransaccion Transaccion { get; set; }
    }

    public class DatosGeneralesTransaccion
    {
        [Required]
        public int IdPais { get; set; }

        [Required]
        public int IdComercio { get; set; }

        [Required]
        public int IdNegocio { get; set; }

        [Required]
        public int IdSubsidiaria { get; set; }

        [Required]
        public int IdSucursal { get; set; }

        [Required]
        public string Terminal { get; set; }

        [Required]
        public string IdUsuario { get; set; }

        public DatosTransaccion Transaccion { get; set; }
    }

    public class DatosTransaccion
    {
        [Required]
        public string IdUnico { get; set; }

        [Required]
        public string Referencia { get; set; }

        [Required]
        public int IdEmisor { get; set; }

        [Required]
        public string Monto { get; set; }

        [Required]
        public Atributos Atributos { get; set; }
    }

    public class Atributos
    {
        public List<Parametros> Parametros { get; set; }
        public List<Adicionales> Adicionales { get; set; }
    }

    public class Parametros
    {
        public string Nombre { get; set; }
        public string IdTipo { get; set; }
        public string Valor { get; set; }
    }

    public class Adicionales
    {
        public string Nombre { get; set; }
        public string IdTipo { get; set; }
        public string Valor { get; set; }
    }
}
