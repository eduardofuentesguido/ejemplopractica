﻿using ApiGateway.Attributes;
using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class AperturaDataEntry
    {
        public string PrimerTokenVerificacion { get; set; }

        [Encrypt]
        [Decrypt]
        [Required]
        public string Sicu { get; set; }

        [Required]
        public string IdProducto { get; set; }

        [Required]
        public string IdSubProducto { get; set; }
        public string IdSistemaMovil { get; set; }
        public bool ActivarBanca { get; set; }

        [Required]
        public bool AceptaPublicidad { get; set; }

        [Required]
        public bool AceptaCompartirDatos { get; set; }

        [Required]
        public bool AceptaAvisoPrivacidad { get; set; }

        [Required]
        public Geolocalizacion Geolocalizacion { get; set; }
        public Cliente Cliente { get; set; }
    }

    public class Cliente
    {
        [Required]
        public string IdOrigen { get; set; }

        [Encrypt]
        [Decrypt]
        public string IdClienteBanco { get; set; }

        [Encrypt]
        [Decrypt]
        public string ClienteUnico { get; set; }

        [Required]
        [Encrypt]
        [Decrypt]
        public string Nombre { get; set; }

        [Required]
        [Encrypt]
        [Decrypt]
        public string ApellidoPaterno { get; set; }

        [Required]
        [Encrypt]
        [Decrypt]
        public string ApellidoMaterno { get; set; }

        [Required]
        [Encrypt]
        [Decrypt]
        public string FechaNacimiento { get; set; }

        [Required]
        [Encrypt]
        [Decrypt]
        public string IdNacionalidad { get; set; }

        [Required]
        [Encrypt]
        [Decrypt]
        public string IdGenero { get; set; }

        [Required]
        public string IdEntidadFederativaNacimiento { get; set; }

        [Required]
        public Contacto Contacto { get; set; }
        public Domicilio Domicilio { get; set; }
    }

    public class Contacto
    {
        [Required]
        [Encrypt]
        [Decrypt]
        public string CorreoElectronico { get; set; }

        [Required]
        [Encrypt]
        [Decrypt]
        public string NumeroCelular { get; set; }
    }

    public class Domicilio
    {
        [Required]
        [Encrypt]
        [Decrypt]
        public string IdEntidadFederativa { get; set; }

        [Required]
        public string IdMunicipio { get; set; }

        [Required]
        public string IdColonia { get; set; }

        [Required]
        public string Municipio { get; set; }

        [Required]
        [Encrypt]
        [Decrypt]
        public string Colonia { get; set; }

        [Required]
        [Encrypt]
        [Decrypt]
        public string Calle { get; set; }

        [Required]
        public string NumeroInterior { get; set; }

        [Required]
        [Encrypt]
        [Decrypt]
        public string NumeroExterior { get; set; }

        [Required]
        [Encrypt]
        [Decrypt]
        public string CodigoPostal { get; set; }
    }
}
