﻿namespace ApiGateway.Models.ControllerDataEntry
{
    public class ConsultaMovimientosDataEntry
    {
        public string NumeroCuenta { get; set; }
        public string FechaInicial { get; set; }
        public string FechaFinal { get; set; }
    }
}
