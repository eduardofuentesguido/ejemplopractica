﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class TraspasoBotonPagoDataEntry
    {
        public string PrimerTokenVerificacion { get; set; }

        [Required]
        public string SegundoTokenVerificacion { get; set; }
        public string NumeroCuentaRetiro { get; set; }
        public bool AltaCuentaFrecuente { get; set; }
        public bool OperacionCuentaFrecuente { get; set; }

        [Required]
        public Geolocalizacion Geolocalizacion { get; set; }
        public TraspasoBotonPagoDataEntryBeneficiario Beneficiario { get; set; }
        public TraspasoBotonPagoDataEntryTraspaso Traspaso { get; set; }
        public TraspasoBotonPagoDataEntryDetallePago DetallePago { get; set; }
    }

    public class TraspasoBotonPagoDataEntryBeneficiario
    {
        [Required]
        public string IdCliente { get; set; }

        [Required]
        public string IdEmpresaCifrado { get; set; }

        [Required]
        public string IdEmpresa { get; set; }
        public string Nombre { get; set; }
        public string Alias { get; set; }
    }

    public class TraspasoBotonPagoDataEntryTraspaso
    {
        public decimal Monto { get; set; }
        public int CuentaOrigen { get; set; }
        public string CodigoDivisa { get; set; }
        public string Concepto { get; set; }
    }

    public class TraspasoBotonPagoDataEntryDetallePago
    {
        public float Comision { get; set; }
        public float Iva { get; set; }
        public float MontoEnvio { get; set; }
        public int CantidadProductos { get; set; }
        public string IdReferenciaPago { get; set; }
    }
}
