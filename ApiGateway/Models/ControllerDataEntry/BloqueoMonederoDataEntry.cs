﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class BloqueoMonederoDataEntry
    {
        [Required]
        public string NumeroMonedero { get; set; }

        public string CodigoBloqueo { get; set; }

        [Required]
        public string Concepto { get; set; }
    }
}
