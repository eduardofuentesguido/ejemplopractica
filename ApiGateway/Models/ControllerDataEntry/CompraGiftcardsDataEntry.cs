﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class CompraGiftcardsDataEntry
    {
        [Required]
        public string Token { get; set; }

        public CompraGiftcardsDataEntryTransaccion Transaccion { get; set; }

        public Notificacion Notificacion { get; set; }

        [Required]
        public Geolocalizacion Geolocalizacion { get; set; }
    }

    public class CompraGiftcardsDataEntryTransaccion
    {
        [Required]
        public string Monto { get; set; }

        [Required]
        public string Cuenta { get; set; }
    }

    public class Notificacion
    {
        [Required]
        public string Celular { get; set; }

        [Required]
        public string Correo { get; set; }

        public string CorreoAdicional { get; set; }
    }
}
