﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class TarjetaDataEntry
    {
        public string PrimerTokenVerificacion { get; set; }

        [Required]
        public string NumeroCuenta { get; set; }

        [Required]
        public string Sicu { get; set; }

        [Required]
        public string Token { get; set; }

        [Required]
        public string Latitud { get; set; }

        [Required]
        public string Longitud { get; set; }
    }
}