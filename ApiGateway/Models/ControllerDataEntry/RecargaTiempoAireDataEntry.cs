﻿using System.ComponentModel.DataAnnotations;

namespace ApiGateway.Models.ControllerDataEntry
{
    public class RecargaTiempoAireDataEntry
    {
        public RecargaTiempoAireDataEntryTransaccion Transaccion { get; set; }
    }

    public class RecargaTiempoAireDataEntryTransaccion
    {
        [Required]
        public string IdOperador { get; set; }

        [Required]
        public string Sku { get; set; }

        [Required]
        public string Celular { get; set; }

        [Required]
        public string MontoRecarga { get; set; }

        [Required]
        public string IdCuenta { get; set; }

        [Required]
        public string NumeroTarjeta { get; set; }
        [Required]
        public string TokenCobro { get; set; }
    }
}
