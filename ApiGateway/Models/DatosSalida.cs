﻿using System;

namespace ApiGateway.Models
{
    public class DatosSalida<TDataOutput>
    {
        public Exception Exception { get; set; }
        public decimal ElapsedTime { get; set; }
        public TDataOutput DataOutput { get; set; }
    }
}
