﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class InversionesDataOutput : BaseDataOutput
    {
        public InversionesDataOutputResultado Resultado { get; set; }

    }
    public class InversionesDataOutputResultado
    {
        public string MontoUdi { get; set; }

        public string FechaTransaccion { get; set; }

        public string TipoDivisa { get; set; }
    }

}
