﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class CargoAbonoDataOutput : BaseDataOutput
    {
        public CargoAbonoDataOutputResultado Resultado { get; set; }
    }

    public class CargoAbonoDataOutputResultado
    {
        public string Nombre { get; set; }
        public float Importe { get; set; }
        public string NumeroMovimiento { get; set; }
        public string FechaOperacion { get; set; }
    }
}