﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class RecargaTiempoAireDataOutput : BaseDataOutput
    {
        public RecargaTiempoAireDataOutputResultado Resultado { get; set; }
    }

    public class RecargaTiempoAireDataOutputResultado
    {
        public string NumeroAutorizacion { get; set; }
        public string Celular { get; set; }
        public string MontoRecarga { get; set; }
        public string NumeroAutorizacionPago { get; set; }
        public string FechaHoraTransaccion { get; set; }
        public string IdFolioTransaccion { get; set; }
    }
}