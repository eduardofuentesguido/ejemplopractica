﻿namespace ApiGateway.Models.ControllerDataOutput
{

    public class PagosDataOutput
    {
        public string IdUnico { get; set; }
        public string IdMovimiento { get; set; }
    }
}
