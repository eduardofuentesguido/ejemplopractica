﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class TraspasoTelefonoDataOutput : BaseDataOutput
    {
        public TraspasoTelefonoDataOutputResultado Resultado { get; set; }
    }

    public class TraspasoTelefonoDataOutputResultado
    {
        public string NumeroMovimiento { get; set; }
        public string FechaOperacion { get; set; }
        public string HoraOperacion { get; set; }
    }
}
