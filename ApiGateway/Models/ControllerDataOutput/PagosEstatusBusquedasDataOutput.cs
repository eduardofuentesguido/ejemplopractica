﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class PagosEstatusBusquedasDataOutput : BaseDataOutput
    {
        public PagosEstatusBusquedasDataOutputResultado Resultado { get; set; }
    }

    public class PagosEstatusBusquedasDataOutputResultado
    {
        public PagosEstatusBusquedasDataOutputOperacion Operacion { get; set; }
    }

    public class PagosEstatusBusquedasDataOutputOperacion
    {
        public string IdUnico { get; set; }
        public string IdMovimiento { get; set; }
    }
}
