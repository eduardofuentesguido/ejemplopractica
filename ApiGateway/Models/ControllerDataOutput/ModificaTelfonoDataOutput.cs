﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class ModificaTelfonoDataOutput : BaseDataOutput
    {
        public ModificaTelfonoDataOutputResultado Resultado { get; set; }
    }

    public class ModificaTelfonoDataOutputResultado
    {
        public string NumeroTelefono { get; set; }
    }
}
