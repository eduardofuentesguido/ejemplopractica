﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class TransferenciaDataOutput : BaseDataOutput
    {
        public TransferenciaDataOutputResultado Resultado { get; set; }
    }

    public class TransferenciaDataOutputResultado
    {
        public string FechaOperacion { get; set; }
        public string HoraOperacion { get; set; }
        public TransferenciaDataOutputCargo Cargo { get; set; }

    }

    public class TransferenciaDataOutputCargo
    {
        public string Concepto { get; set; }
        public string Monto { get; set; }
        public string CodigoDivisa { get; set; }
        public string ConceptoOperacion { get; set; }
        public string HoraRegistro { get; set; }
        public string FechaRegistro { get; set; }
        public string ClaveRastreo { get; set; }
    }
}
