﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class CompraGiftcardsDataOutput : BaseDataOutput
    {
        public CompraGiftcardsDataOutputResultado Resultado { get; set; }
    }

    public class CompraGiftcardsDataOutputResultado
    {
        public string Monto { get; set; }
        public string Comision { get; set; }
        public string Operador { get; set; }
        public string CodigoTarjeta { get; set; }
        public string NumeroReferencia { get; set; }
        public string FechaHoraOperacion { get; set; }
    }
}
