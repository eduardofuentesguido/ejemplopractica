﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class ConsultaTelfonoDataOutput : BaseDataOutput
    {
        public ConsultaTelfonoDataOutputResultado Resultado { get; set; }
    }

    public class ConsultaTelfonoDataOutputResultado
    {
        public string NumeroTelefono { get; set; }
    }
}
