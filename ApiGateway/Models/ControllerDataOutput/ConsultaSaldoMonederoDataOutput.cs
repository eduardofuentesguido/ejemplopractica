﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class ConsultaSaldoMonederoDataOutput : BaseDataOutput
    {
        public ConsultaSaldoMonederoDataOutputResultado Resultado { get; set; }
    }

    public class ConsultaSaldoMonederoDataOutputResultado
    {
        public string SaldoDisponible { get; set; }
        public string NumeroOperacion { get; set; }
        public string Divisa { get; set; }
        public string TitularCuenta { get; set; }
        public string NumeroCuenta { get; set; }
        public string DescripcionCuenta { get; set; }
        public string FechaHoraOperacion { get; set; }
    }

}
