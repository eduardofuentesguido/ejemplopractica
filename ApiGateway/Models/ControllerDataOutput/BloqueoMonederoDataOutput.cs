﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class BloqueoMonederoDataOutput : BaseDataOutput
    {
        public BloqueoMonederoDataOutputResultado Resultado { get; set; }
    }

    public class BloqueoMonederoDataOutputResultado
    {
        public string SaldoImpactado { get; set; }
        public string NumeroOperacion { get; set; }
    }
}
