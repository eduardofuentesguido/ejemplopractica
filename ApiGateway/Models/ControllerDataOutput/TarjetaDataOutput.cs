﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class TarjetaDataOutput : BaseDataOutput
    {
        public TarjetaDataOutputResultado Resultado { get; set; }
    }

    public class TarjetaDataOutputResultado
    {
        public string NumeroCuenta { get; set; }
        public string NumeroTarjeta { get; set; }
        public string Cvv { get; set; }
        public string FechaExpiracion { get; set; }
    }
}
