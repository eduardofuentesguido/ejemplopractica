﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class CancelacionMonederoDataOutput : BaseDataOutput
    {
        public CancelacionMonederoDataOutputResultado Resultado { get; set; }
    }

    public class CancelacionMonederoDataOutputResultado
    {
        public string Codigo { get; set; }
        public string Mensaje { get; set; }
    }
}
