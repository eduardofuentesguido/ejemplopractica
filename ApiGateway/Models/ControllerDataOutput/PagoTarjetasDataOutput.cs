﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class PagoTarjetasDataOutput : BaseDataOutput
    {
        public PagoTarjetasDataOutputResultado Resultado { get; set; }
    }

    public class PagoTarjetasDataOutputResultado
    {
        public PagoTarjetasDataOutputOperacion Operacion { get; set; }
        public PagoTarjetasDataOutputCliente Cliente { get; set; }
        public PagoTarjetasDataOutputSucursal Sucursal { get; set; }
    }

    public class PagoTarjetasDataOutputOperacion
    {
        public string FechaOperacion { get; set; }
        public string NumeroMovimiento { get; set; }
        public string NumeroCuenta { get; set; }
        public string Importe { get; set; }
        public string CodigoDivisa { get; set; }
        public string Divisa { get; set; }
        public string Descripcion { get; set; }
    }

    public class PagoTarjetasDataOutputCliente
    {
        public string Nombre { get; set; }
        public string Representante { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
    }

    public class PagoTarjetasDataOutputSucursal
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
    }
}
