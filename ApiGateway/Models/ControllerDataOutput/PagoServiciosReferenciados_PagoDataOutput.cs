﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class PagoServiciosReferenciadosPagoDataOutput : BaseDataOutput
    {
        public PagoServiciosReferenciadosPagoDataOutputResultado Resultado { get; set; }
    }

    public class PagoServiciosReferenciadosPagoDataOutputResultado
    {
        public PagoServiciosReferenciadosPagoDataOutputOperacion Operacion { get; set; }
    }

    public class PagoServiciosReferenciadosPagoDataOutputOperacion
    {
        public string IdUnico { get; set; }
        public string IdMovimiento { get; set; }
        public Atributos Atributos { get; set; }
    }
}
