﻿namespace ApiGateway.Models.ControllerDataOutput
{
    /// <summary>
    /// Datos de salida de la configuración de los Servicios.
    /// </summary>
    public class ConfiguracionServiciosDataOutput : BaseDataOutput
    {
    }
}
