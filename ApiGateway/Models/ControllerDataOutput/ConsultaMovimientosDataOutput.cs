﻿using System.Collections.Generic;

namespace ApiGateway.Models.ControllerDataOutput
{
    public class ConsultaMovimientosDataOutput : BaseDataOutput
    {
        public ConsultaMovimientosDataOutputResultado Resultado { get; set; }
    }

    public class ConsultaMovimientosDataOutputResultado
    {
        public string NumeroCuenta { get; set; }
        public string Producto { get; set; }
        public string TitularCuenta { get; set; }
        public string FechaConsulta { get; set; }
        public string HoraConsulta { get; set; }
        public List<Movimiento> Movimientos { get; set; }
    }

    public class Movimiento
    {
        public string Importe { get; set; }
        public string Saldo { get; set; }
        public string Descripcion { get; set; }
        public string FechaOperacion { get; set; }
        public string NumeroMovimiento { get; set; }
        public string CodigoDivisa { get; set; }
    }
}
