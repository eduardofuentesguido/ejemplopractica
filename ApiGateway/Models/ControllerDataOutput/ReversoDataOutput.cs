﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class ReversoDataOutput : BaseDataOutput
    {
        public ReversoDataOutputResultado Resultado { get; set; }
    }

    public class ReversoDataOutputResultado
    {
        public string NumeroMovimiento { get; set; }
        public string Operacion { get; set; }
        public string ConceptoMovimiento { get; set; }
        public string CuentaRelacionada { get; set; }
        public string SaldoActual { get; set; }
    }
}
