﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class AperturaDataOutput : BaseDataOutput
    {
        public AperturaDataOutputResultado Resultado { get; set; }
    }

    public class AperturaDataOutputResultado
    {
        public string NumeroCliente { get; set; }
        public string NumeroCuenta { get; set; }
        public string NumeroCuentaClabe { get; set; }
        public string EstatusDigitalizacion { get; set; }
    }
}
