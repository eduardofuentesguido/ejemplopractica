﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class ConsultaBloqueosDataOutput : BaseDataOutput
    {
        public ConsultaBloqueoDataOutputResultado Resultado { get; set; }
    }

    public class ConsultaBloqueoDataOutputResultado
    {
        public string TipoOperacion { get; set; }
        public string DescripcionTipoOperacion { get; set; }
        public string ConceptoBloqueo { get; set; }
    }
}
