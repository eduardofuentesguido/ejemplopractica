﻿using System.Collections.Generic;

namespace ApiGateway.Models.ControllerDataOutput
{
    public class ConsultaSaldoSicuDataOutput : BaseDataOutput
    {
        public Resul Resultado { get; set; }
    }

    public class Resul
    {
        public Client Cliente { get; set; }
    }

    public class Client
    {
        public string Sicu { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string NumeroCelular { get; set; }
        public List<Cuentas> Cuentas { get; set; }
    }

    public class Cuentas
    {
        public string IdClienteBanco { get; set; }
        public string IdProducto { get; set; }
        public string IdSubProducto { get; set; }
        public string Alias { get; set; }
        public string Numero { get; set; }
        public string Clabe { get; set; }
        public string NumeroTarjeta { get; set; }
        public string CodigoEstatus { get; set; }
        public string SaldoTotal { get; set; }
        public string SaldoRetenido { get; set; }
        public string SaldoDisponible { get; set; }

    }
}
