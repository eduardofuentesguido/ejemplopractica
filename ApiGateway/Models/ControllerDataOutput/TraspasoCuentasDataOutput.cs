﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class TraspasoCuentasDataOutput : BaseDataOutput
    {
        public TraspasoCuentasDataOutputResultado Resultado { get; set; }
    }

    public class TraspasoCuentasDataOutputResultado
    {
        public string FechaOperacion { get; set; }
        public string HoraOperacion { get; set; }
        public string NumeroMovimiento { get; set; }
    }
}
