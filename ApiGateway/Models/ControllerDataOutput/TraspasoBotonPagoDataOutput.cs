﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class TraspasoBotonPagoDataOutput : BaseDataOutput
    {
        public TraspasoBotonPagoDataOutputResultado Resultado { get; set; }
    }

    public class TraspasoBotonPagoDataOutputResultado
    {
        public string FechaOperacion { get; set; }
        public string HoraOperacion { get; set; }
        public string NumeroMovimiento { get; set; }
        public string DescripcionOrigen { get; set; }
        public string DescripcionDestino { get; set; }
    }
}
