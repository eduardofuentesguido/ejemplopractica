﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class PagosCanalesTercerosDataOutput : BaseDataOutput
    {
        public PagosCanalesTercerosDataOutputResultado Resultado { get; set; }
    }

    public class PagosCanalesTercerosDataOutputResultado
    {
        public string FechaHora { get; set; }
        public string NumeroCuenta { get; set; }
        public string NumeroMovimiento { get; set; }
        public PagosCanalesTercerosDataOutputCliente Cliente { get; set; }
        public string IdProgramaSocial { get; set; }
    }

    public class PagosCanalesTercerosDataOutputCliente
    {
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
    }
}
