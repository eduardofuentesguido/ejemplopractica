﻿using ApiGateway.Models.ControllerDataEntry;

namespace ApiGateway.Models.ControllerDataOutput
{
    public class TarjetaBusquedaDataOutput
    {
        public ResultadoTD Resultadotd { get; set; }
        public Geolocalizacion Geolocalizacion { get; set; }

    }
    public class ResultadoTD
    {
        public string NumeroContrato { get; set; }
        public string CodigoMoneda { get; set; }
        public string CodigoComisionPlan { get; set; }
        public string DescripcionComision { get; set; }
        public string IdentificadorClave { get; set; }
        public string ValorEstatus { get; set; }
        public string ValorDisposicion { get; set; }
        public string EstatusDisposicion { get; set; }
        public string Disposicion { get; set; }
        public Participante Participante { get; set; }
        public Tarjeta Tarjeta { get; set; }
        public Monto Monto { get; set; }
        public Limite Limite { get; set; }
    }

    public class Participante
    {
        public string Nombre { get; set; }
        public string Clave { get; set; }
        public string Secuencia { get; set; }
    }

    public class Tarjeta
    {
        public string Numero { get; set; }
        public string Tipo { get; set; }
        public string ValorTipo { get; set; }
        public string Estatus { get; set; }
        public string FechaExpiracion { get; set; }
    }

    public class Monto
    {
        public string Credito { get; set; }
        public string LimiteCredito { get; set; }
        public string AutorizadoCredito { get; set; }
        public string Disponible { get; set; }
        public string Autorizado { get; set; }
        public string Total { get; set; }
    }

    public class Limite
    {
        public string CajeroDia { get; set; }
        public string ZonaDia { get; set; }
        public string Credito { get; set; }
        public string DispositivoDia { get; set; }
        public string CajeroMes { get; set; }

    }
}
