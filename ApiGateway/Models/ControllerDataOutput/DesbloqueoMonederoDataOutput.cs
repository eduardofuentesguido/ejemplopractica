﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class DesbloqueoMonederoDataOutput : BaseDataOutput
    {
        public DesbloqueoMonederoDataOutputResultado Resultado { get; set; }
    }

    public class DesbloqueoMonederoDataOutputResultado
    {
        public string SaldoImpactado { get; set; }
        public string NumeroOperacion { get; set; }
    }
}
