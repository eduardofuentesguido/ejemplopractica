﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class PagosSociosComercialesDataOutput : BaseDataOutput
    {
        public PagosSociosComercialesDataOutputResultado Resultado { get; set; }
    }

    public class PagosSociosComercialesDataOutputResultado
    {
        public string NumeroMovimiento { get; set; }
        public string FechaRegistro { get; set; }
        public string HoraRegistro { get; set; }
        public PagosSociosComercialesDataOutputCliente Cliente { get; set; }
    }

    public class PagosSociosComercialesDataOutputCliente
    {
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string NumeroCuenta { get; set; }
    }
}
