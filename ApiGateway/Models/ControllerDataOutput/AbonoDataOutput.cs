﻿namespace ApiGateway.Models.ControllerDataOutput
{
    public class AbonoDataOutput : BaseDataOutput
    {
        public AbonoDataOutputResultado Resultado { get; set; }
    }

    public class AbonoDataOutputResultado
    {
        public AbonoDataOutputCliente Cliente { get; set; }
        public AbonoDataOutputOperacion Operacion { get; set; }
        public AbonoDataOutputSucursal Sucursal { get; set; }
    }

    public class AbonoDataOutputCliente
    {
        public string Nombre { get; set; }
        public string Representante { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
    }

    public class AbonoDataOutputOperacion
    {
        public string FechaOperacion { get; set; }
        public string NumeroMovimiento { get; set; }
        public string NumeroCuenta { get; set; }
        public string Importe { get; set; }
        public string CodigoDivisa { get; set; }
        public string Divisa { get; set; }
        public string Descripcion { get; set; }
    }

    public class AbonoDataOutputSucursal
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
    }
}
