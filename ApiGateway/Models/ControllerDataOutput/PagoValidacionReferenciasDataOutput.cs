﻿using System.Collections.Generic;

namespace ApiGateway.Models.ControllerDataOutput
{
    public class PagoValidacionReferenciasDataOutput : BaseDataOutput
    {
        public PagoValidacionReferenciasDataOutputResultado Resultado { get; set; }
    }
    public class PagoValidacionReferenciasDataOutputResultado
    {
        public PagoValidacionReferenciasDataOutputOperacion Operacion { get; set; }
    }

    public class PagoValidacionReferenciasDataOutputOperacion
    {
        public string NumeroCuenta { get; set; }
        public string MontoComision { get; set; }
        public string MontoComisionCliente { get; set; }
        public string Iva { get; set; }
        public Atributos Atribiutos { get; set; }
    }

    public class Atributos
    {
        public List<Parametros> Parametros { get; set; }
        public List<Adicionales> Adicionales { get; set; }

    }
    public class Parametros
    {
        public string IdTipo { get; set; }
        public string Valor { get; set; }
        public string Nombre { get; set; }

    }

    public class Adicionales
    {
        public string IdTipo { get; set; }
        public string Valor { get; set; }
        public string Nombre { get; set; }
    }
}

