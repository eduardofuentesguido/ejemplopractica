﻿
namespace ApiGateway.Models.ControllerDataOutput
{
    public class ConsultaDetalleCuentaDataOutput : BaseDataOutput
    {
        public ConsultaDetalleCuentaDataOutputResultado Resultado { get; set; }
    }

    public class ConsultaDetalleCuentaDataOutputResultado
    {
        public Cuenta Cuenta { get; set; }
    }

    public class Cuenta
    {
        public string Numero { get; set; }
        public string Nivel { get; set; }
        public float MontoDeposito { get; set; }
    }

}
