﻿using System.Collections.Generic;

namespace ApiGateway.Models
{
    public class MSNotificationResponse
    {
        public List<Notificaciones> Notificaciones { get; set; }
    }

    public class Notificaciones
    {
        public string Mensaje { get; set; }
        public string Notificacion { get; set; }
    }
}
