﻿using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ApiGateway.Middleware
{
    /// <summary>
    /// Intenta medir el tiempo de procesamiento de la solicitud.
    /// </summary>
    public class ResponseTimeMiddleware
    {
        // Nombre del encabezado de respuesta, los encabezados personalizados comienzan con "X-"
        private const string ResponseHeaderResponseTime = "X-Response-Time-Ms";

        // Manejar al siguiente Middleware en la canalización
        private readonly RequestDelegate _next;

        ///<inheritdoc/>
        public ResponseTimeMiddleware(RequestDelegate next) => _next = next;

        /// <inheritdoc/>
        public Task InvokeAsync(HttpContext context)
        {
            // Iniciar el temporizador con el cronómetro
            var watch = new Stopwatch();
            watch.Start();

            context.Response.OnStarting(() =>
            {
                // Detenga la información del temporizador y calcule el tiempo.
                watch.Stop();
                var responseTimeForCompleteRequest = watch.ElapsedMilliseconds;

                // Agregue la información del tiempo de respuesta en los encabezados de respuesta.   
                context.Response.Headers.Add("X-Frame-Options", "DENY");
                context.Response.Headers.Add("X-Xss-Protection", "1; mode=block");
                context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                context.Response.Headers.Add("X-Permitted-Cross-Domain-Policies", "none");
                context.Response.Headers[ResponseHeaderResponseTime] = $"{responseTimeForCompleteRequest} ms";                

                return Task.CompletedTask;
            });

            // Llame al próximo delegado / middleware en la canalización.  
            return _next(context);
        }
    }
}
