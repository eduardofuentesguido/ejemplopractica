﻿using ApiGateway.Attributes;
using ApiGateway.Common.Extensions;
using ApiGateway.Services.Crypto;
using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ApiGateway.Helpers
{
    /// <summary>
    /// The encrypt attribute helper.
    /// </summary>
    public static class EncryptAttributeHelper
    {
        /// <summary>
        /// Obtiene el objeto encriptado de forma asincrónica.
        /// </summary>
        /// <param name="objectClass">Objeto de referencia.</param>
        /// <returns>Devuelve el objeto encriptado.</returns>
        public static async Task<T> GetEncryptedObjectAsync<T>(T objectClass) where T : class
        {
            PropertyInfo[] propertyInfos = objectClass.GetType().GetProperties();

            await propertyInfos.ForEachAsync(async propertyInfo =>
            {
                var atributes = propertyInfo.GetCustomAttribute<DecryptAttribute>();
                if (atributes != null)
                {
                    var value = propertyInfo.GetValue(objectClass);
                    propertyInfo.SetValue(objectClass, await CryptoService.EncryptAsync(value.ToString()).ConfigureAwait(false));
                }
                if (propertyInfo.PropertyType.IsClass && !propertyInfo.PropertyType.IsValueType
                    && !propertyInfo.PropertyType.IsPrimitive && !propertyInfo.PropertyType.FullName.StartsWith("System."))
                {
                    var value = propertyInfo.GetValue(objectClass);
                    if (value != null)
                    {
                        await GetEncryptedObjectAsync(value).ConfigureAwait(false);
                    }
                }
                if (propertyInfo.PropertyType.IsClass && propertyInfo.GetValue(objectClass, null) is ICollection collection)
                {
                    await collection.OfType<object>().Where(x => x != null).ForEachAsync(async item =>
                    {
                        await GetEncryptedObjectAsync(item).ConfigureAwait(false);
                    }).ConfigureAwait(false);
                }
            }).ConfigureAwait(false);

            return objectClass;
        }
    }
}
