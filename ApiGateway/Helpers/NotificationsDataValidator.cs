﻿using ApiGateway.Models;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Text.Json;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: VALIDA SI LA LISTA DE NOTIFICACIONES CONTIENE ERRORES.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   12 / 07 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    12-07-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Helpers
{
    /// <summary>
    /// Clase que valida los datos de notificaciones.
    /// </summary>
    public static class NotificationsDataValidator
    {
        /// <summary>
        /// Valida el objeto de notificaciones.
        /// </summary>
        /// <param name="dataObject">Objeto de referencia.</param>
        /// <returns>Devuelve un objeto de la clase <see cref="MSNotificationResponse"/>.</returns>
        public static MSNotificationResponse ValidateNotifications(JObject dataObject)
        {
            var notificationResponse = JsonSerializer.Deserialize<MSNotificationResponse>(
                    dataObject.ToString(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

            if (notificationResponse != null && notificationResponse.Notificaciones.Any())
            {
                var notificacion = notificationResponse.Notificaciones.FirstOrDefault(x => x.Notificacion.StartsWith("E"));
                if (notificacion != null)
                {
                    throw new OperationException(notificacion.Mensaje, notificacion.Notificacion);
                }
            }

            return notificationResponse;
        }
    }
}
