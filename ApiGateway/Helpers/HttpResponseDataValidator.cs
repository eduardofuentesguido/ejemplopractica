﻿using ApiGateway.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiGateway.Helpers
{
    /// <summary>
    /// Validador de datos de Respuesta Http.
    /// </summary>
    public static class HttpResponseDataValidator
    {
        /// <summary>
        /// Obtiene el mensaje de respuesta de la solicitud Http.
        /// </summary>
        /// <param name="httpResponseMessage">The http response message.</param>
        /// <param name="logger">The logger.</param>
        /// <returns>Devuelve un objeto de tipo <see cref="JObject"/> con la información de respuesta de la solicitud Http.</returns>
        public static async Task<JObject> GetResponseMessageAsync(HttpResponseMessage httpResponseMessage, ILogger logger)
        {
            JObject responseObjectData;
            var contentResponse = await httpResponseMessage.Content.ReadAsStringAsync();

            switch (httpResponseMessage.StatusCode)
            {
                case HttpStatusCode.OK:
                case HttpStatusCode.Created:
                    responseObjectData = JObject.Parse(contentResponse);
                    if (responseObjectData.ContainsKey("code"))
                    {
                        throw new OperationException(responseObjectData.Value<string>("message"), responseObjectData.Value<string>("code"));
                    }

                    break;
                default:
                    var responseErrorObjectData = string.IsNullOrEmpty(contentResponse) ? null : JObject.Parse(contentResponse);
                    logger.LogError(new JObject
                    {
                        new JProperty("reasonPhrase", httpResponseMessage.ReasonPhrase),
                        new JProperty("statusCode", (int)httpResponseMessage.StatusCode),
                        new JProperty("Response", responseErrorObjectData?.ToString())
                    }.ToString());

                    throw new ArgumentException(nameof(httpResponseMessage), GetErrorMessage(httpResponseMessage, contentResponse, responseErrorObjectData));
            }
            return responseObjectData;
        }

        /// <summary>
        /// Devuelve el mensaje de error encontrado.
        /// </summary>
        /// <param name="httpResponseMessage">The http response message.</param>
        /// <param name="contentResponse">The content response.</param>
        /// <param name="responseErrorObjectData">The response error object data.</param>
        /// <returns>Devuelve el mensaje en una cadena de texto.</returns>
        private static string GetErrorMessage(HttpResponseMessage httpResponseMessage, string contentResponse, JObject responseErrorObjectData)
        {
            if (string.IsNullOrEmpty(contentResponse))
            {
                return httpResponseMessage.ReasonPhrase;
            }

            if (responseErrorObjectData.ContainsKey("codigo"))
            {
                return responseErrorObjectData.Value<string>("codigo");
            }
            else if (responseErrorObjectData.ContainsKey("message"))
            {
                return responseErrorObjectData.Value<string>("message");
            }
            else
            {
                return responseErrorObjectData.ToString();
            }
        }
    }
}
