﻿using ApiGateway.Common.Aws.Utils;
using ApiGateway.Common.Aws.Utils.Models.Interfaces.Wrappers;
using ApiGateway.Common.HttpExtensions.Models.HttpInterfaces;
using ApiGateway.Common.HttpExtensions.Services;
using ApiGateway.Common.HttpExtensions.Services.Operations;
using ApiGateway.Common.OnPremise.Utils;
using ApiGateway.Common.OnPremise.Utils.Models.Interfaces;
using ApiGateway.Microservices;
using ApiGateway.Services;
using ApiGateway.Models;
using ApiGateway.Models.ServiceInterfaces;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.OnPremise.Interfaces;
using ApiGateway.Models.OnPremise.Settings;
using ApiGateway.Models.RepositoryInterfaces.Kafka;
using ApiGateway.Models.RepositoryInterfaces.SuperCore;
using ApiGateway.Models.ServiceInterfaces.Environment;
using ApiGateway.Models.ServiceInterfaces.Kafka;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces.Options;
using ApiGateway.Models.Settings;
using ApiGateway.Repositories.Kafka;
using ApiGateway.Repositories.SuperCore;
using ApiGateway.Services.Configuration;
using ApiGateway.Services.Environment;
using ApiGateway.Services.Kafka;
using ApiGateway.Services.Monedero;
using ApiGateway.Services.Monedero.CompraGiftcards;
using ApiGateway.Services.Monedero.Cuentas;
using ApiGateway.Services.Monedero.Inversiones;
using ApiGateway.Services.Monedero.Traspasos;
using ApiGateway.Services.OnPremise;
using ApiGateway.Services.Options;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using static ApiGateway.Models.Microservices.Settings.ControlSettings;
using static ApiGateway.Models.Microservices.Settings.MonederoSettings;
using static ApiGateway.Models.OnPremise.Settings.OnPremiseSettings;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLA LOS REGISTROS DE INYECCION DE INDEPENDENCIAS 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Extensions
{
    /// <summary>
    /// Extensiones del Servicio.
    /// </summary>
    public static class ServiceExtensions
    {
        #region API GATEWAY
        /// <summary>
        /// Configura los registros de Inyeccion de Dependencias para el ApiGateway.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IHttpStrategy, HttpStrategy>();
            serviceCollection.AddTransient<IKafkaWrapper, KafkaWrapper>();
            serviceCollection.AddTransient<IKafkaService, KafkaService>();
            serviceCollection.AddTransient<IKafkaRepository, KafkaRepository>();
            serviceCollection.AddTransient<IOnPremiseSecurity, OnPremiseSecurity>();
            serviceCollection.AddTransient<IEnvironmentService, EnvironmentService>();
            serviceCollection.AddTransient<IServiceExecutionRepository, ServiceExecutionRepository>();
        }

        /// <summary>
        /// Obtiene configuración desde el appSettings para el ApiGateway.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureApiGatewaySettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var apiGatewaySettings = new ApiGatewaySettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.ApiGatewayConfiguration.SectionKey)
                .Bind(apiGatewaySettings);
            serviceCollection.AddSingleton(Options.Create(apiGatewaySettings));
        }

        /// <summary>
        /// Configures the writable.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="section">The section.</param>
        /// <param name="file">The file.</param>
        public static void ConfigureWritable<T>(this IServiceCollection services, IConfigurationSection section, string file) where T : class, new()
        {
            services.Configure<T>(section);
            services.AddTransient<IWritableOptions<T>>(provider =>
            {
                var configuration = (IConfigurationRoot)provider.GetService<IConfiguration>();
                var environment = provider.GetService<IWebHostEnvironment>();
                var options = provider.GetService<IOptionsMonitor<T>>();
                return new WritableOptions<T>(file, section.Key, options, environment, configuration);
            });
        }

        /// <summary>
        /// Registra los clientes HTTP con Inyección de Dependencias
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void AddHttpClients(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddHttpClient<IHttpOperation, DeleteOperation>();
            serviceCollection.AddHttpClient<IHttpOperation, GetOperation>();
            serviceCollection.AddHttpClient<IHttpOperation, PatchOperation>();
            serviceCollection.AddHttpClient<IHttpOperation, PostOperation>();
            serviceCollection.AddHttpClient<IHttpOperation, PutOperation>();
        }
        #endregion

        #region SWAGGER
        /// <summary>
        /// Estable configuración de Swagger a nivel local.
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc(ApiGatewayConstants.Swagger.GroupName, new OpenApiInfo
                {
                    Title = $"ApiGateway {ApiGatewayConstants.Swagger.GroupName}",
                    Version = ApiGatewayConstants.Swagger.GroupName,
                    Description = "Orquestador SuperCore",
                });
            });
        }
        #endregion

        #region AWS
        #region KAFKA
        /// <summary>
        /// Obtiene configuración de Kafka desde el appSettings.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureKafkaSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var kafkaSettings = new KafkaSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.Aws.Kafka.SectionKey)
                .Bind(kafkaSettings);
            serviceCollection.AddSingleton(Options.Create(kafkaSettings));
        }
        #endregion
        #endregion

        #region ON PREMISE
        /// <summary>
        /// Obtiene configuración del appSettings para OnPremeise.
        /// </summary>
        /// <param name="serviceCollection">The service collection.</param>
        public static void ConfigureOnPremSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var authOnPremise = new AuthOnPremise();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.OnPremise.Autentication.OnPremAutenticationSectionKey)
                .Bind(authOnPremise);
            serviceCollection.AddSingleton(Options.Create(authOnPremise));
        }

        /// <summary>
        /// Configura los registros de Inyeccion de Dependencias para OnPremise.
        /// </summary>
        /// <param name="serviceCollection">The service collection.</param>
        public static void ConfigureOnPremServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IOnPremiseServices, OnPremiseServices>();
        }

        #region APERTURA
        /// <summary>
        /// Obtiene configuración del appSettings para el API de Apertura de OnPremise.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureOnPremApertuaMonederoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var onPremAperturaSettings = new OnPremAperturaSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.OnPremise.Apis.AperturaSectionKey)
                .Bind(onPremAperturaSettings);
            serviceCollection.AddSingleton(Options.Create(onPremAperturaSettings));
        }
        #endregion

        #region CONSULTA MOVIMIENTOS
        /// <summary>
        /// Obtiene configuración del appSettings para el API de Consulta Movimientos de OnPremise.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureOnPremConsultaMovimientosSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var consultamovimientosSettings = new OnPremConsultaMovimientosSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.OnPremise.Apis.ConsultaMovimientosSectionKey)
                .Bind(consultamovimientosSettings);
            serviceCollection.AddSingleton(Options.Create(consultamovimientosSettings));
        }

        #region CONSALUTA SALDO
        /// <summary>
        /// Obtiene configuración del appSettings para el API de Consulta Saldo de OnPremise.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureOnPremConsultaSaldoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var consultaSaldoSettings = new OnPremConsultaSaldoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.OnPremise.Apis.ConsultaSaldoSectionKey)
                .Bind(consultaSaldoSettings);
            serviceCollection.AddSingleton(Options.Create(consultaSaldoSettings));
        }

        /// <summary>
        /// Obtiene configuración del appSettings para el API de Traspaso entre Cuentas de OnPremise.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureOnPremTraspasoCuentasSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var consultaSaldoSettings = new OnPremTraspasoCuentasSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.OnPremise.Apis.TraspasoCuentasSectionKey)
                .Bind(consultaSaldoSettings);
            serviceCollection.AddSingleton(Options.Create(consultaSaldoSettings));
        }
        #endregion
        #endregion
        #endregion

        #region MICROSERVICES
        #region MODULO DE MONEDERO
        /// <summary>
        /// Configura los servicios del Módulo de Monedero con Inyeccion de Dependencias.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureMonederoServices(this IServiceCollection serviceCollection)
        {
            //Modulo de monedero
            serviceCollection.AddTransient<IMonederoMicroservices, MonederoMicroservices>();

            //Servicios de Monedero
            serviceCollection.AddTransient<IAbonoService, AbonoService>();
            serviceCollection.AddTransient<IAperturaService, AperturaService>();
            serviceCollection.AddTransient<IBloqueoService, BloqueoService>();
            serviceCollection.AddTransient<ICancelacionService, CancelacionService>();
            serviceCollection.AddTransient<ICancelaRetencionService, CancelaRetencionService>();
            serviceCollection.AddTransient<ICargoService, CargoService>();
            serviceCollection.AddTransient<IConsultaBloqueosService, ConsultaBloqueosService>();
            serviceCollection.AddTransient<IConsultaMonederoService, ConsultaMonederoService>();
            serviceCollection.AddTransient<IConsultaMovimientosService, ConsultaMovimientosService>();
            serviceCollection.AddTransient<IConsultaSaldoService, ConsultaSaldoService>();
            serviceCollection.AddTransient<IConsultaSaldoSicuService, ConsultaSaldoSicuService>();
            serviceCollection.AddTransient<IConsultaTelefonoService, ConsultaTelefonoService>();
            serviceCollection.AddTransient<IDesbloqueoService, DesbloqueoService>();
            serviceCollection.AddTransient<ILiberaRetencionService, LiberaRetencionService>();
            serviceCollection.AddTransient<IModificaTelefonoService, ModificaTelefonoService>();
            serviceCollection.AddTransient<IRetencionService, RetencionService>();
            serviceCollection.AddTransient<IReversaAbonoService, ReversaAbonoService>();
            serviceCollection.AddTransient<IReversaCargoService, ReversaCargoService>();
            serviceCollection.AddTransient<IValidaAcumuladosService, ValidaAcumuladosService>();
            serviceCollection.AddTransient<ITraspasoTelefonosService, TraspasosTelefonosService>();
            serviceCollection.AddTransient<IInversionesService, InversionesService>();
            serviceCollection.AddTransient<ICompraGiftcardsService, CompraGiftcardsService>();
            serviceCollection.AddTransient<ITraspasosBotonPagoService, TraspasosBotonPagoService>();
            serviceCollection.AddTransient<IRecargaTiempoAireService, RecargaTiempoAireService>();
            serviceCollection.AddTransient<ITarjetasService, TarjetasService>();
            serviceCollection.AddTransient<ITraspasoCuentasService, TraspasosCuentasService>();
            serviceCollection.AddTransient<IPagosEstatusBusquedasService, PagosEstatusBusquedasService>();
            serviceCollection.AddTransient<IValiacionReferenciaService, ValidacionReferenciaService>();
            serviceCollection.AddTransient<IPagoServiciosReferenciadosService, PagoServiciosReferenciadosService>();
            serviceCollection.AddTransient<IConsultaDetalleCuentaService, ConsultaDetalleCuentaService>();
            serviceCollection.AddTransient<ICargoAbonoService, CargoAbonoService>();
            serviceCollection.AddTransient<ITransferenciaService, TransferenciaService>();
            serviceCollection.AddTransient<ITarjetaBusquedaService, TarjetaBusquedaService>();
            serviceCollection.AddTransient<IPagosSociosComercialesService, PagosSociosComercialesService>();
            serviceCollection.AddTransient<IPagosCanalesTercerosService, PagosCanalesTercerosService>();
            serviceCollection.AddTransient<IReversosService, ReversosService>();
            serviceCollection.AddTransient<IHealthChecksService, HealthChecksService>();
        }

        #region ABONO
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Abonos.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureAbonoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var abonoSettings = new AbonoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.AbonoSectionKey)
                .Bind(abonoSettings);
            serviceCollection.AddSingleton(Options.Create(abonoSettings));
        }
        #endregion

        #region APERTURA
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Apertura.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureApertuaMonederoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var aperturaSettings = new AperturaSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.AperturaSectionKey)
                .Bind(aperturaSettings);
            serviceCollection.AddSingleton(Options.Create(aperturaSettings));
        }
        #endregion

        #region BLOQUEO
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Bloqueo.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureBloqueoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var bloqueoSettings = new BloqueoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.BloqueoSectionKey)
                .Bind(bloqueoSettings);
            serviceCollection.AddSingleton(Options.Create(bloqueoSettings));
        }
        #endregion

        #region CANCELACION
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Cancelación.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureCancelacionSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var cancelacionSettings = new CancelacionSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.CancelacionSectionKey)
                .Bind(cancelacionSettings);
            serviceCollection.AddSingleton(Options.Create(cancelacionSettings));
        }
        #endregion

        #region CANCELARETENCION
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Cancela Retención.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureCancelaRetencionSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var cancelaretencionSettings = new CancelaRetencionSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.CancelaRetencionSectionKey)
                .Bind(cancelaretencionSettings);
            serviceCollection.AddSingleton(Options.Create(cancelaretencionSettings));
        }
        #endregion

        #region CARGO
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Cargo.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureCargoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var cargoSettings = new CargoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.CargoSectionKey)
                .Bind(cargoSettings);
            serviceCollection.AddSingleton(Options.Create(cargoSettings));
        }
        #endregion

        #region CONSULTABLOQUEOS
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Consulta Bloqueos.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureConsultaBloqueosSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var consultabloqueosSettings = new ConsultaBloqueosSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.ConsultaBloqueosSectionKey)
                .Bind(consultabloqueosSettings);
            serviceCollection.AddSingleton(Options.Create(consultabloqueosSettings));
        }
        #endregion

        #region CONSULTAMONEDERO
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Consulta Monedero.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureConsultaMonederoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var consultamonederoSettings = new ConsultaMonederoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.ConsultaMonederoSectionKey)
                .Bind(consultamonederoSettings);
            serviceCollection.AddSingleton(Options.Create(consultamonederoSettings));
        }
        #endregion

        #region CONSULTA MONEDERO POR TELEFONO
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Consulta Monedero por Teléfono.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureConsultaMonederoTelefonoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var consultamonederoSettings = new ConsultaMonederoTelefonoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.ConsultaMonederoTelefonoSectionKey)
                .Bind(consultamonederoSettings);
            serviceCollection.AddSingleton(Options.Create(consultamonederoSettings));
        }
        #endregion

        #region CONSULTAMOVIMIENTOS
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Consulta Movimientos.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureConsultaMovimientosSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var consultamovimientosSettings = new ConsultaMovimientosSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.ConsultaMovimientosSectionKey)
                .Bind(consultamovimientosSettings);
            serviceCollection.AddSingleton(Options.Create(consultamovimientosSettings));
        }
        #endregion

        #region CONSALUTASALDO
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Consulta Saldo.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureConsultaSaldoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var consultaSaldoSettings = new ConsultaSaldoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.ConsultaSaldoSectionKey)
                .Bind(consultaSaldoSettings);
            serviceCollection.AddSingleton(Options.Create(consultaSaldoSettings));
        }
        #endregion

        #region CONSALUTA SALDO SICU
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Consulta Saldo por SICU.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureConsultaSaldoSicuSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var consultaSaldoSicuSettings = new ConsultaSaldoSicuSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.ConsultaSaldoSicuSectionKey)
                .Bind(consultaSaldoSicuSettings);
            serviceCollection.AddSingleton(Options.Create(consultaSaldoSicuSettings));
        }
        #endregion

        #region CONSULTATELEFONO
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Consulta Teléfono.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureConsultaTelefonoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var consultaTelefonoSettings = new ConsultaTelefonoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.ConsultaTelefonoSectionKey)
                .Bind(consultaTelefonoSettings);
            serviceCollection.AddSingleton(Options.Create(consultaTelefonoSettings));
        }
        #endregion

        #region DESBLOQUEO
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Desbloqueo.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureDesbloqueoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var desbloqueoSettings = new DesbloqueoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.DesbloqueoSectionKey)
                .Bind(desbloqueoSettings);
            serviceCollection.AddSingleton(Options.Create(desbloqueoSettings));
        }
        #endregion

        #region LIBERARETENCION
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Libera Retención".
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureLiberaRetencionSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var liberaRetencionSettings = new LiberaRetencionSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.LiberaRetencionSectionKey)
                .Bind(liberaRetencionSettings);
            serviceCollection.AddSingleton(Options.Create(liberaRetencionSettings));
        }
        #endregion

        #region MODIFICATELEFONO
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Modifica Teléfono.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureModificaTelefonoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var modificaTelefonoSettings = new ModificaTelefonoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.ModificaTelefonoSectionKey)
                .Bind(modificaTelefonoSettings);
            serviceCollection.AddSingleton(Options.Create(modificaTelefonoSettings));
        }
        #endregion

        #region RETENCION
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Retencion.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureRetencionSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var retencionSettings = new RetencionSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.RetencionSectionKey)
                .Bind(retencionSettings);
            serviceCollection.AddSingleton(Options.Create(retencionSettings));
        }
        #endregion

        #region REVERSAABONO
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Reversa Abono.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureReversaAbonoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var reversaAbonoSettings = new ReversaAbonoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.ReversaAbonoSectionKey)
                .Bind(reversaAbonoSettings);
            serviceCollection.AddSingleton(Options.Create(reversaAbonoSettings));
        }
        #endregion

        #region REVERSACARGO
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Reversa Cargo.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureReversaCargoSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var reversaCargoSettings = new ReversaCargoSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.ReversaCargoSectionKey)
                .Bind(reversaCargoSettings);
            serviceCollection.AddSingleton(Options.Create(reversaCargoSettings));
        }
        #endregion

        #region VALIDAACUMULADOS
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de Valida Acumulados.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureValidaAcumuladosSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var validaAcumuladosSettings = new ValidaAcumuladosSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Monedero.ValidaAcumuladosSectionKey)
                .Bind(validaAcumuladosSettings);
            serviceCollection.AddSingleton(Options.Create(validaAcumuladosSettings));
        }
        #endregion

        #endregion

        #region MODULO DE CONTROL
        /// <summary>
        /// Configura los registros de Inyeccion de Dependencias para los Microservicios de Control.
        /// </summary>
        /// <param name="serviceCollection">The service collection.</param>
        public static void ConfigureControlServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IControlMicroservices, ControlMicroservices>();
        }

        #region CAM
        /// <summary>
        /// Obtiene configuración del appSettings para el Microservicio de CAM.
        /// </summary>
        /// <param name="serviceCollection">Objeto de tipo <see cref="IServiceCollection"/></param>
        public static void ConfigureCamSettings(this IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationService(new EnvironmentService());
            var camSettings = new CamSettings();
            configuration.GetConfiguration().GetSection(ApiGatewayConstants.SuperCore.Microservicios.Control.CamSectionKey)
                .Bind(camSettings);
            serviceCollection.AddSingleton(Options.Create(camSettings));
        }
        #endregion
        #endregion
        #endregion
    }
}
