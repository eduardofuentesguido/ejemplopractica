﻿using System;

namespace ApiGateway.Attributes
{
    /// <summary>
    /// Atributo de encriptado.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
    sealed class EncryptAttribute : Attribute
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="EncryptAttribute"/>.
        /// </summary>
        public EncryptAttribute() { }
    }
}
