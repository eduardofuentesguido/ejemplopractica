﻿using System;

namespace ApiGateway.Attributes
{
    /// <summary>
    /// Atributo de descifrado.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
    sealed class DecryptAttribute : Attribute
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="DecryptAttribute"/>.
        /// </summary>
        public DecryptAttribute() { }
    }
}
