using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CLASE INICIAL DE LA APLICACION.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   23 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    23-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace CoreApiGateway
{
    public static class Program
    {
        /// <summary>
        /// Método principal para cargar toda la configuración del sistema.
        /// </summary>
        /// <param name="args"></param>
        public static void Main() => CreateHostBuilder().Build().Run();

        /// <summary>
        /// Método para generar un Host.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder() =>
            Host.CreateDefaultBuilder()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
