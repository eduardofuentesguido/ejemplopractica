using ApiGateway.Extensions;
using ApiGateway.Middleware;
using ApiGateway.Models.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CARGA TODA LA CONFIGURACION DEL API.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   23 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    23-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace CoreApiGateway
{
    public class Startup
    {
        /// <summary>
        /// Obtienen configuración.
        /// </summary>
        public IConfiguration Configuration { get; }
        /// <summary>
        /// Obtiene el entorno de alojamiento web.
        /// </summary>
        public IWebHostEnvironment WebHostEnvironment { get; }

        /// <summary>
        /// Carga archivo appSettings.{webHostEnvironment}.json para la configuración inicial.
        /// </summary>
        /// <param name="webHostEnvironment">Objeto de la inyección de IWebHostEnvironment.</param>
        public Startup(IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;

            var builder = new ConfigurationBuilder()
                .SetBasePath(webHostEnvironment.ContentRootPath)
                .AddJsonFile($"appSettings.{webHostEnvironment.EnvironmentName}.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        /// <summary>
        /// Este método se usa para definir y registrar todos los servicios que se utilizaran.
        /// </summary>
        /// <param name="services">Coleccion de servicios.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            #region Configuration
            services.AddHsts(options =>
            {
                options.Preload = true;
                options.IncludeSubDomains = true;
                options.MaxAge = TimeSpan.FromDays(30);
            });

            services.Configure<CookiePolicyOptions>(opts =>
            {
                opts.CheckConsentNeeded = _ => true;
                opts.HttpOnly = HttpOnlyPolicy.Always;
                opts.Secure = CookieSecurePolicy.Always;
                opts.MinimumSameSitePolicy = SameSiteMode.Strict;
            });
            #endregion

            //Run Dependency Injection
            #region API GATEWAY
            services.AddControllers();
            services.ConfigureServices();
            services.ConfigureApiGatewaySettings();
            services.AddHttpClients();
            services.ConfigureWritable<ServiceSettings>(
                Configuration.GetSection("ServicesConfiguration"), $"appSettings.{WebHostEnvironment.EnvironmentName}.json");
            #endregion

            #region SWAGGER
            services.ConfigureSwagger();
            #endregion

            #region AWS
            services.ConfigureKafkaSettings();
            #endregion

            #region ON PREMISE
            services.ConfigureOnPremSettings();
            services.ConfigureOnPremServices();
            services.ConfigureOnPremApertuaMonederoSettings();
            services.ConfigureOnPremConsultaMovimientosSettings();
            services.ConfigureOnPremConsultaSaldoSettings();
            services.ConfigureOnPremTraspasoCuentasSettings();
            #endregion

            #region MICROSERVICES
            #region MONEDERO
            services.ConfigureMonederoServices();
            services.ConfigureAbonoSettings();
            services.ConfigureApertuaMonederoSettings();
            services.ConfigureBloqueoSettings();
            services.ConfigureCancelacionSettings();
            services.ConfigureCancelaRetencionSettings();
            services.ConfigureCargoSettings();
            services.ConfigureConsultaBloqueosSettings();
            services.ConfigureConsultaMonederoSettings();
            services.ConfigureConsultaMonederoTelefonoSettings();
            services.ConfigureConsultaMovimientosSettings();
            services.ConfigureConsultaSaldoSettings();
            services.ConfigureConsultaSaldoSicuSettings();
            services.ConfigureConsultaTelefonoSettings();
            services.ConfigureDesbloqueoSettings();
            services.ConfigureLiberaRetencionSettings();
            services.ConfigureModificaTelefonoSettings();
            services.ConfigureRetencionSettings();
            services.ConfigureReversaAbonoSettings();
            services.ConfigureReversaCargoSettings();
            services.ConfigureValidaAcumuladosSettings();
            #endregion

            #region CONTROL
            services.ConfigureControlServices();
            services.ConfigureCamSettings();
            #endregion
            #endregion
        }

        /// <summary>
        /// Este método es usado para registrar y definir la canalización de solicitudes HTTP y configurar servicios externos.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
            ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api Gateway - SuperCore v1");
                });
            }
            else
            {
                app.UseHsts();                
            }

            //Middlewares
            app.UseMiddleware<ResponseTimeMiddleware>();

            //Logging
            loggerFactory.AddAWSProvider(
                Configuration.GetAWSLoggingConfigSection());

            app.UseHsts();
            app.UseRouting();
            app.UseCookiePolicy();
            app.UseHttpsRedirection();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
