﻿using ApiGateway.Common.Aws.Utils.Models.Interfaces.Wrappers;
using ApiGateway.Common.Aws.Utils.Models.Kafka;
using ApiGateway.Models.RepositoryInterfaces.Kafka;
using ApiGateway.Models.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: DEFINE LA CONFIGURACION BÁSICA PARA LLAMAR EL SERVICIO QUE PUBLICA EN KAFKA.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Repositories.Kafka
{
    /// <inheritdoc cref="IKafkaRepository"/>
    public class KafkaRepository : IKafkaRepository
    {
        private readonly IKafkaWrapper _kafkaWrapper;
        private readonly KafkaSettings _kafkaSettings;

        /// <summary>
        /// Constructor de la clase KafkaRepository.
        /// </summary>
        /// <param name="kafkaWrapper">Objecto Wrapper de Kafka.</param>
        /// <param name="kafkaSettings">Objeto Settings de Kafka.</param>
        public KafkaRepository(IKafkaWrapper kafkaWrapper, IOptions<KafkaSettings> kafkaSettings)
        {
            _kafkaWrapper = kafkaWrapper;
            _kafkaSettings = kafkaSettings.Value;
        }

        /// <inheritdoc cref="IKafkaRepository.SendMessageAsync(string)"/>        
        public async Task<Exception> SendMessageAsync(string message)
        {
            return await _kafkaWrapper.SendMessageAsync(message,
                new KafkaConfiguration
                {
                    Topic = _kafkaSettings.Topic,
                    FlushTime = _kafkaSettings.FlushTime,
                    BootstrapServers = _kafkaSettings.BootstrapServers
                });
        }
    }
}
