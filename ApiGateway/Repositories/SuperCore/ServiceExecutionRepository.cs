﻿using ApiGateway.Common.HttpExtensions.Models;
using ApiGateway.Common.HttpExtensions.Models.HttpInterfaces;
using ApiGateway.Common.OnPremise.Utils.Models;
using ApiGateway.Common.OnPremise.Utils.Models.Interfaces;
using ApiGateway.Models.OnPremise.Settings;
using ApiGateway.Models.RepositoryInterfaces.SuperCore;
using ApiGateway.Models.Settings;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using static ApiGateway.Common.HttpExtensions.Models.Enums.HttpEnum;
using static ApiGateway.Models.Enums.ApiGatewayEnums;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: DEFINE LA CONFIGURACION BÁSICA PARA REALIZAR UNA SOLICITUD HTTP.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Repositories.SuperCore
{
    /// <inheritdoc cref="IServiceExecutionRepository"/>
    public class ServiceExecutionRepository : IServiceExecutionRepository
    {
        private readonly IHttpStrategy _httpStrategy;
        private readonly AuthOnPremise _authOnPremise;
        private readonly IOnPremiseSecurity _onPremiseSecurity;

        /// <summary>
        /// Constructor de la clase.
        /// </summary>
        /// <param name="httpStrategy">Objeto <paramref name="httpStrategy"/></param>
        public ServiceExecutionRepository(
            IHttpStrategy httpStrategy,
            IOptions<AuthOnPremise> authOnPremise,
            IOnPremiseSecurity onPremiseSecurity)
        {
            _httpStrategy = httpStrategy;
            _authOnPremise = authOnPremise.Value;
            _onPremiseSecurity = onPremiseSecurity;
        }

        /// <inheritdoc cref="IServiceExecutionRepository.ExecuteDeleteAsync{TSettings}(TSettings, object, Dictionary{string, string}, EnterpriseSystem)"/>
        public async Task<HttpResponseMessage> ExecuteDeleteAsync<TSettings>(TSettings settings, object input, Dictionary<string, string> headerDictionary, EnterpriseSystem enterpriseSystem) where TSettings : MicroserviceBaseSettings
        {
            var content = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, settings.ContentTypeHeaderValue);
            var modelContent = await MapModelDataAsync(settings, content, enterpriseSystem);

            return await _httpStrategy.CallServiceAsync(OperationType.Delete, modelContent, headerDictionary);
        }

        /// <inheritdoc cref="IServiceExecutionRepository.ExecuteGetAsync{TSettings}(TSettings, object, Dictionary{string, string}, EnterpriseSystem)"/>
        public async Task<HttpResponseMessage> ExecuteGetAsync<TSettings>(TSettings settings, object input, Dictionary<string, string> headerDictionary, EnterpriseSystem enterpriseSystem) where TSettings : MicroserviceBaseSettings
        {
            var content = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, settings.ContentTypeHeaderValue);
            var modelContent = await MapModelDataAsync(settings, content, enterpriseSystem);

            return await _httpStrategy.CallServiceAsync(OperationType.Get, modelContent, headerDictionary);
        }

        /// <inheritdoc cref="IServiceExecutionRepository.ExecutePatchAsync{TSettings}(TSettings, object, Dictionary{string, string}, EnterpriseSystem)"/>
        public async Task<HttpResponseMessage> ExecutePatchAsync<TSettings>(TSettings settings, object input, Dictionary<string, string> headerDictionary, EnterpriseSystem enterpriseSystem) where TSettings : MicroserviceBaseSettings
        {
            var content = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, settings.ContentTypeHeaderValue);
            var modelContent = await MapModelDataAsync(settings, content, enterpriseSystem);

            return await _httpStrategy.CallServiceAsync(OperationType.Patch, modelContent, headerDictionary);
        }

        /// <inheritdoc cref="IServiceExecutionRepository.ExecutePostAsync{TSettings}(TSettings, object, Dictionary{string, string}, EnterpriseSystem)"/>
        public async Task<HttpResponseMessage> ExecutePostAsync<TSettings>(TSettings settings, object input, Dictionary<string, string> headerDictionary, EnterpriseSystem enterpriseSystem) where TSettings : MicroserviceBaseSettings
        {
            var content = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, settings.ContentTypeHeaderValue);
            var modelContent = await MapModelDataAsync(settings, content, enterpriseSystem);

            return await _httpStrategy.CallServiceAsync(OperationType.Post, modelContent, headerDictionary);
        }

        /// <inheritdoc cref="IServiceExecutionRepository.ExecutePutAsync{TSettings}(TSettings, object, Dictionary{string, string}, EnterpriseSystem)"/>
        public async Task<HttpResponseMessage> ExecutePutAsync<TSettings>(TSettings settings, object input, Dictionary<string, string> headerDictionary, EnterpriseSystem enterpriseSystem) where TSettings : MicroserviceBaseSettings
        {
            var content = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, settings.ContentTypeHeaderValue);
            var modelContent = await MapModelDataAsync(settings, content, enterpriseSystem);

            return await _httpStrategy.CallServiceAsync(OperationType.Put, modelContent, headerDictionary);
        }

        /// <summary>
        /// Método para mapear las configuraciones de <typeparamref name="TSettings"/> al objeto <see cref="HttpModelContent"/>
        /// </summary>
        /// <typeparam name="TSettings">Objeto de confiraciones basadas en: <see cref="MicroserviceBaseSettings"/></typeparam>
        /// <param name="settings">Objeto de confiraciones basadas en: <see cref="MicroserviceBaseSettings"/></param>        
        /// <param name="enterpriseSystem">Objeto para validar el tipo de sistema a ejecutar: <see cref="EnterpriseSystem"/></param>        
        /// <returns>El método devuelve un objeto <see cref="HttpModelContent"/></returns>        
        private async Task<HttpModelContent> MapModelDataAsync<TSettings>(
            TSettings settings, StringContent httpModelContent, EnterpriseSystem enterpriseSystem) where TSettings : MicroserviceBaseSettings
        {
            string bearerToken;
            if (enterpriseSystem.Equals(EnterpriseSystem.OnPremise))
            {
                var onPremConfiguration = JsonSerializer.Deserialize<OnPremiseConfiguration>(JsonSerializer.Serialize(_authOnPremise));
                bearerToken = await _onPremiseSecurity.GetAuthenticationTokenAsync(onPremConfiguration);
            }
            else
            {
                bearerToken = string.Empty;
            }

            return new HttpModelContent
            {
                Query = settings.Query,
                BaseUri = settings.BaseUri,
                BearerToken = bearerToken,
                Content = httpModelContent,
                RequestUri = settings.RequestUri,
                AcceptHeaderValue = settings.AcceptHeaderValue,
                AuthenticationSchema = settings.AuthenticationSchema,
            };
        }
    }
}
