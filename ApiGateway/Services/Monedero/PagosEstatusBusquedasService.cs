﻿using ApiGateway.Helpers;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Models.ServiceInterfaces.Monedero
{
    public class PagosEstatusBusquedasService : IPagosEstatusBusquedasService
    {
        private readonly ILogger<PagosEstatusBusquedasService> _logger;
        private readonly IControlMicroservices _controlMicroservices;

        public PagosEstatusBusquedasService(
            ILogger<PagosEstatusBusquedasService> logger,
            IControlMicroservices controlMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
        }

        /// <inheritdoc cref="IPagosEstatusBusquedasService.ExecuteServiceAsync(PagoServiciosReferenciadosDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<PagosEstatusBusquedasDataOutput>> ExecuteServiceAsync(
            PagoServiciosReferenciadosDataEntry pagoServiciosReferenciadosDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<PagosEstatusBusquedasDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Mapea datos de entrada para el Microservicio                
                var datosSupercore = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = "RET005",
                        Referencias = new List<string>
                        {
                            { pagoServiciosReferenciadosDataEntry.Transaccion.Transaccion.Referencia }
                        },
                        Montos = new List<decimal>
                        {
                            { Convert.ToDecimal(pagoServiciosReferenciadosDataEntry.Transaccion.Transaccion.Monto) }
                        },
                        Monederos = new List<int>
                        {
                            { Convert.ToInt32(pagoServiciosReferenciadosDataEntry.Transaccion.Transaccion.IdEmisor) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = headerDictionary["x-latitud"],
                        Longitud = headerDictionary["x-longitud"],
                        TokenSrvCom = "",
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion                

                #region Detiene Stopwatch
                timer.Stop();
                decimal timeElapsed = timer.ElapsedMilliseconds;
                datosSupercore.DatosExtendidos.TiempoOrquestador = timeElapsed;
                #endregion

                #region Ejecuta Microservicio de Apertura
                var httpResponseMessage = await _controlMicroservices.ExecuteCamAsync(datosSupercore, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Mapea Datos de respuesta de CAM
                var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                    await httpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta                
                var camDatosSalida = camDataOutput.DatosSalida.Movimientos.FirstOrDefault();
                var notification = notifications.Notificaciones.FirstOrDefault();
                datosSalida.DataOutput = new PagosEstatusBusquedasDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new PagosEstatusBusquedasDataOutputResultado
                    {
                        Operacion = new PagosEstatusBusquedasDataOutputOperacion
                        {
                            IdMovimiento = camDatosSalida.NumeroMovimiento,
                            IdUnico = ""
                        }
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}