﻿using ApiGateway.Common.HttpExtensions.Models;
using ApiGateway.Common.HttpExtensions.Models.HttpInterfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using static ApiGateway.Common.HttpExtensions.Models.Enums.HttpEnum;
using static ApiGateway.Models.Microservices.Settings.MonederoSettings;

namespace ApiGateway.Services.Monedero
{
    public class LiberaRetencionService : ILiberaRetencionService
    {
        private readonly IHttpStrategy _httpStrategy;
        private readonly LiberaRetencionSettings _liberaRetencionSettings;

        public LiberaRetencionService(IHttpStrategy httpStrategy,
            IOptions<LiberaRetencionSettings> liberaRetencionSettings)
        {
            _httpStrategy = httpStrategy;
            _liberaRetencionSettings = liberaRetencionSettings.Value;
        }

        public async Task<object> ExecuteServiceAsync(object input)
        {
            var content = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, _liberaRetencionSettings.ContentTypeHeaderValue);

            var response = await _httpStrategy.CallServiceAsync(OperationType.Post, new HttpModelContent
            {
                AcceptHeaderValue = _liberaRetencionSettings.AcceptHeaderValue,
                AuthenticationSchema = _liberaRetencionSettings.AuthenticationSchema,
                BaseUri = _liberaRetencionSettings.BaseUri,
                BearerToken = string.Empty,
                RequestUri = _liberaRetencionSettings.RequestUri,
                Content = content
            }, null);

            var responseContent = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<dynamic>(responseContent);
        }
    }
}
