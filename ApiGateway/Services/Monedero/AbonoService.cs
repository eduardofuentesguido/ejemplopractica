﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class AbonoService : IAbonoService
    {
        private readonly ILogger<AbonoService> _logger;
        private readonly IControlMicroservices _controlMicroservices;

        public AbonoService(ILogger<AbonoService> logger,
            IControlMicroservices controlMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
        }

        /// <inheritdoc cref="IAbonoService.ExecuteServiceAsync(AbonoDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<AbonoDataOutput>> ExecuteServiceAsync(
            AbonoDataEntry abonoDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<AbonoDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Mapea datos de entrada para el Microservicio                
                var camDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = abonoDataEntry.Transaccion.Abono.Codigo,
                        Referencias = new List<string>
                        {
                            { abonoDataEntry.Transaccion.Abono.Referencia }
                        },
                        Montos = new List<decimal>
                        {
                            { Convert.ToDecimal(abonoDataEntry.Transaccion.Abono.Importe) }
                        },
                        Monederos = new List<int>
                        {
                            { Convert.ToInt32(abonoDataEntry.Transaccion.NumeroCuenta) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = abonoDataEntry.Geolocalizacion.Latitud,
                        Longitud = abonoDataEntry.Geolocalizacion.Longitud,
                        TokenSrvCom = abonoDataEntry.SegundoTokenVerificacion,
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                camDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de CAM
                var httpResponseMessage = await _controlMicroservices.ExecuteCamAsync(camDataEntry, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Mapea Datos de respuesta de CAM
                var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                    await httpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta 
                var camDatosSalida = camDataOutput.DatosSalida.Movimientos.FirstOrDefault();
                var notification = notifications.Notificaciones.FirstOrDefault();
                datosSalida.DataOutput = new AbonoDataOutput
                {
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Codigo = notification?.Notificacion,
                    Resultado = new AbonoDataOutputResultado
                    {
                        Cliente = new AbonoDataOutputCliente
                        {

                        },
                        Operacion = new AbonoDataOutputOperacion
                        {
                            NumeroMovimiento = camDatosSalida.NumeroMovimiento
                        },
                        Sucursal = new AbonoDataOutputSucursal
                        {

                        }
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}