﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class CargoAbonoService : ICargoAbonoService
    {
        /// <summary>
        /// _Ilogger
        /// </summary>
        private readonly ILogger<CargoAbonoService> _logger;
        private readonly IControlMicroservices _controlMicroservices;

        public CargoAbonoService(
            ILogger<CargoAbonoService> logger,
            IControlMicroservices controlMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
        }

        /// <inheritdoc cref="ICargoAbonoService.ExecuteServiceAsync(CargoAbonoDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<CargoAbonoDataOutput>> ExecuteServiceAsync(
            CargoAbonoDataEntry cargoAbonoDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<CargoAbonoDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Abono
                #region Mapea datos de entrada para el Microservicio                    
                var camAbonoDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = "DEP005",
                        Referencias = new List<string>
                        {
                            { cargoAbonoDataEntry.Transaccion.Abono.CodigoAbono }
                        },
                        Montos = new List<decimal>
                        {
                            { Convert.ToDecimal(cargoAbonoDataEntry.Transaccion.Abono.Importe)}
                        },
                        Monederos = new List<int>
                        {
                             { Convert.ToInt32(cargoAbonoDataEntry.Transaccion.NumeroCuenta) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = cargoAbonoDataEntry.Geolocalizacion.Latitud,
                        Longitud = cargoAbonoDataEntry.Geolocalizacion.Longitud,
                        TokenSrvCom = cargoAbonoDataEntry.SegundoTokenVerificacion,
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion                

                #region Detiene Stopwatch
                timer.Stop();
                camAbonoDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de la CAM
                var abonoHttpResponseMessage = await _controlMicroservices.ExecuteCamAsync(camAbonoDataEntry, null);
                JObject abonoResponseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(abonoHttpResponseMessage, _logger);
                #endregion
                #endregion

                #region Validacion de datos
                NotificationsDataValidator.ValidateNotifications(abonoResponseObjectData);
                #endregion

                #region Cargo
                #region Mapea datos de entrada para el Microservicio      
                timer.Restart();
                var camCargoDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = "RET005",
                        Referencias = new List<string>
                        {
                            { cargoAbonoDataEntry.Transaccion.Cargo.CodigoCargo }
                        },
                        Montos = new List<decimal>
                        {
                            { Convert.ToDecimal(cargoAbonoDataEntry.Transaccion.Cargo.Importe)}
                        },
                        Monederos = new List<int>
                        {
                             { Convert.ToInt32(cargoAbonoDataEntry.Transaccion.NumeroCuenta) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = cargoAbonoDataEntry.Geolocalizacion.Latitud,
                        Longitud = cargoAbonoDataEntry.Geolocalizacion.Longitud,
                        TokenSrvCom = cargoAbonoDataEntry.SegundoTokenVerificacion,
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion                

                #region Detiene Stopwatch
                timer.Stop();
                camCargoDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de la CAM
                var cargoHttpResponseMessage = await _controlMicroservices.ExecuteCamAsync(camCargoDataEntry, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(cargoHttpResponseMessage, _logger);
                #endregion
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Mapea Datos de respuesta de CAM
                var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                    await cargoHttpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta                
                var camDatosSalida = camDataOutput.DatosSalida.Movimientos.FirstOrDefault();
                var notification = notifications.Notificaciones.FirstOrDefault();
                datosSalida.DataOutput = new CargoAbonoDataOutput
                {
                    Mensaje = notification?.Mensaje,
                    Folio = "",
                    Codigo = notification?.Notificacion,
                    Resultado = new CargoAbonoDataOutputResultado
                    {
                        Nombre = "",
                        Importe = 0,
                        NumeroMovimiento = camDatosSalida.NumeroMovimiento,
                        FechaOperacion = "",
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
