﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class ConsultaBloqueosService : IConsultaBloqueosService
    {
        private readonly ILogger<ConsultaBloqueosService> _logger;
        private readonly IMonederoMicroservices _monederoMicroservices;

        public ConsultaBloqueosService(
            ILogger<ConsultaBloqueosService> logger,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="IConsultaBloqueosService.ExecuteServiceAsync(ConsultaBloqueosDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<ConsultaBloqueosDataOutput>> ExecuteServiceAsync(
            ConsultaBloqueosDataEntry consultaBloqueosDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<ConsultaBloqueosDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Mapea datos de entrada para el Microservicio                
                var datosSupercore = new DatosSuperCore<MSConsultaBloqueoDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CONSULTABLOQ",
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                       Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSConsultaBloqueoDataEntry
                    {
                        Empresa = headerDictionary["x-codigo-empresa"],
                        IdMonedero = Convert.ToInt32(consultaBloqueosDataEntry.NumeroMonedero)
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = "",
                        Longitud = "",
                        TokenSrvCom = "",
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                decimal timeElapsed = timer.ElapsedMilliseconds;
                datosSupercore.DatosExtendidos.TiempoOrquestador = timeElapsed;
                #endregion

                #region Ejecuta Microservicio de Consulta Bloqueos
                var httpResponseMessage = await _monederoMicroservices.ConsultaBloqueoAsync(datosSupercore, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Mapea Datos de respuesta del Microservicio
                var consultaBloqueoDataOutput = JsonSerializer.Deserialize<MSConsultaBloqueoDataOutput>(
                    await httpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta                     
                var notification = notifications.Notificaciones.FirstOrDefault();
                var bloqueo = consultaBloqueoDataOutput.DatosSalida.Bloqueos?.FirstOrDefault();
                datosSalida.DataOutput = new ConsultaBloqueosDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new ConsultaBloqueoDataOutputResultado
                    {
                        ConceptoBloqueo = bloqueo?.ConceptoBloqueo,
                        DescripcionTipoOperacion = bloqueo?.DesTipoOperacion,
                        TipoOperacion = bloqueo?.TipoOperacion
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
