﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
namespace ApiGateway.Services.Monedero
{
    public class CargoService : ICargoService
    {
        private readonly ILogger<CargoService> _logger;
        private readonly IControlMicroservices _controlMicroservices;

        public CargoService(
            ILogger<CargoService> logger,
            IControlMicroservices controlMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
        }

        /// <inheritdoc cref="ICargoService.ExecuteServiceAsync(CargoDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<PagoTarjetasDataOutput>> ExecuteServiceAsync(
            CargoDataEntry cargoDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<PagoTarjetasDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Mapea datos de entrada para el Microservicio                
                var datosSupercore = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = cargoDataEntry.Transaccion.Cargo.Codigo,
                        Referencias = new List<string>
                        {
                            { cargoDataEntry.Transaccion.Cargo.Referencia }
                        },
                        Montos = new List<decimal>
                        {
                            { Convert.ToDecimal(cargoDataEntry.Transaccion.Cargo.Importe) }
                        },
                        Monederos = new List<int>
                        {
                            { Convert.ToInt32(cargoDataEntry.Transaccion.NumeroCuenta) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = cargoDataEntry.Geolocalizacion.Latitud,
                        Longitud = cargoDataEntry.Geolocalizacion.Longitud,
                        TokenSrvCom = cargoDataEntry.SegundoTokenVerificacion,
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion                

                #region Detiene Stopwatch
                timer.Stop();
                decimal timeElapsed = timer.ElapsedMilliseconds;
                datosSupercore.DatosExtendidos.TiempoOrquestador = timeElapsed;
                #endregion

                #region Ejecuta Microservicio de Apertura
                var httpResponseMessage = await _controlMicroservices.ExecuteCamAsync(datosSupercore, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Mapea Datos de respuesta de CAM
                var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                    await httpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta                
                var notification = notifications.Notificaciones.FirstOrDefault();
                var camDatosSalida = camDataOutput.DatosSalida.Movimientos.FirstOrDefault();
                datosSalida.DataOutput = new PagoTarjetasDataOutput
                {
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Codigo = notification?.Notificacion,
                    Resultado = new PagoTarjetasDataOutputResultado
                    {
                        Cliente = new PagoTarjetasDataOutputCliente
                        {
                            Nombre = "",
                            Representante = "",
                            Direccion = "",
                            Ciudad = "",
                        },
                        Operacion = new PagoTarjetasDataOutputOperacion
                        {
                            FechaOperacion = cargoDataEntry.Transaccion.FechaOperacion,
                            NumeroMovimiento = camDatosSalida.NumeroMovimiento,
                            NumeroCuenta = "",
                            Importe = cargoDataEntry.Transaccion.Cargo.Importe,
                            CodigoDivisa = cargoDataEntry.Transaccion.CodigoDivisa,
                            Divisa = "",
                            Descripcion = "",
                        },
                        Sucursal = new PagoTarjetasDataOutputSucursal
                        {
                            Nombre = "",
                            Descripcion = "",
                            Direccion = "",
                        }
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}