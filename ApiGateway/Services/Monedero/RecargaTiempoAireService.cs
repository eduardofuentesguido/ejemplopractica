﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class RecargaTiempoAireService : IRecargaTiempoAireService
    {
        private readonly IControlMicroservices _controlMicroservices;
        private readonly ILogger<RecargaTiempoAireService> _logger;

        public RecargaTiempoAireService(
            ILogger<RecargaTiempoAireService> logger,
            IControlMicroservices controlMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
        }

        /// <inheritdoc cref="IRecargaTiempoAireService.ExecuteServiceAsync(RecargaTiempoAireDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<RecargaTiempoAireDataOutput>> ExecuteServiceAsync(
            RecargaTiempoAireDataEntry recargaTiempoAireDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<RecargaTiempoAireDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Mapea datos de entrada para el Microservicio
                var camDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = "RET005",
                        Referencias = new List<string>
                        {
                            { recargaTiempoAireDataEntry.Transaccion.IdOperador}
                        },
                        Montos = new List<decimal>
                        {
                            { Convert.ToInt32(recargaTiempoAireDataEntry.Transaccion.MontoRecarga) }
                        },
                        Monederos = new List<int>
                        {
                            { Convert.ToInt32(recargaTiempoAireDataEntry.Transaccion.IdCuenta) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = headerDictionary["x-latitud"],
                        Longitud = headerDictionary["x-longitud"],
                        TokenSrvCom = "3",
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                camDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de CAM
                var httpResponseMessage = await _controlMicroservices.ExecuteCamAsync(camDataEntry, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Devuelve respuesta    
                var notification = notifications.Notificaciones.FirstOrDefault();
                datosSalida.DataOutput = new RecargaTiempoAireDataOutput
                {
                    Mensaje = notification?.Mensaje,
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Resultado = new RecargaTiempoAireDataOutputResultado
                    {
                        NumeroAutorizacion = "",
                        Celular = recargaTiempoAireDataEntry.Transaccion.Celular,
                        MontoRecarga = recargaTiempoAireDataEntry.Transaccion.MontoRecarga,
                        NumeroAutorizacionPago = "",
                        FechaHoraTransaccion = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"),
                        IdFolioTransaccion = "",
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}