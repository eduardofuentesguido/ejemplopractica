﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class CancelacionService : ICancelacionService
    {
        private readonly ILogger<CancelacionService> _logger;
        private readonly IMonederoMicroservices _monederoMicroservices;

        public CancelacionService(
            ILogger<CancelacionService> logger,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="ICancelacionService.ExecuteServiceAsync(CancelacionMonederoDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<CancelacionMonederoDataOutput>> ExecuteServiceAsync(
            CancelacionMonederoDataEntry cancelacionMonederoDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<CancelacionMonederoDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Mapea datos de entrada para el Microservicio                
                var datosSupercore = new DatosSuperCore<MSCancelacionDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CANCELACION",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCancelacionDataEntry
                    {
                        Centro = cancelacionMonederoDataEntry.Centro,
                        IdMonedero = Convert.ToInt32(cancelacionMonederoDataEntry.NumeroMonedero)
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = "",
                        Longitud = "",
                        TokenSrvCom = "",
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                decimal timeElapsed = timer.ElapsedMilliseconds;
                datosSupercore.DatosExtendidos.TiempoOrquestador = timeElapsed;
                #endregion

                #region Ejecuta Microservicio de Cancelacion
                var httpResponseMessage = await _monederoMicroservices.CancelacionAsync(datosSupercore, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Devuelve respuesta          
                var notification = notifications.Notificaciones.FirstOrDefault();
                datosSalida.DataOutput = new CancelacionMonederoDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new CancelacionMonederoDataOutputResultado
                    {
                        Codigo = notification?.Notificacion,
                        Mensaje = notification?.Mensaje
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
