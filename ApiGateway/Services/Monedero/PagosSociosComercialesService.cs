﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class PagosSociosComercialesService : IPagosSociosComercialesService
    {
        private readonly ILogger<PagosSociosComercialesService> _logger;
        private readonly IControlMicroservices _controlMicroservices;

        public PagosSociosComercialesService(
            ILogger<PagosSociosComercialesService> logger,
            IControlMicroservices controlMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
        }

        /// <inheritdoc cref="IPagosSociosComercialesService.ExecuteServiceAsync(PagosSociosComercialesDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<PagosSociosComercialesDataOutput>> ExecuteServiceAsync(
            PagosSociosComercialesDataEntry pagosSociosComercialesDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<PagosSociosComercialesDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Mapea datos de entrada para el Microservicio
                var camDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = "RET005",
                        Referencias = new List<string> { },
                        Montos = new List<decimal>
                        {
                            { Convert.ToDecimal(pagosSociosComercialesDataEntry.Monto)}
                        },
                        Monederos = new List<int>
                        {
                            //{ Convert.ToInt32(pagosSociosComercialesDataEntry.Transaccion.) }
                            { 5 }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = "876543767",
                        Longitud = "31234567",
                        TokenSrvCom = "3",
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                camDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de CAM
                var httpResponseMessage = await _controlMicroservices.ExecuteCamAsync(camDataEntry, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Ejecuta servicio OnPremise                
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Mapea Datos de respuesta de CAM
                var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                    await httpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta
                var notification = notifications.Notificaciones.FirstOrDefault();
                var camDatosSalida = camDataOutput.DatosSalida.Movimientos.FirstOrDefault();
                datosSalida.DataOutput = new PagosSociosComercialesDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new PagosSociosComercialesDataOutputResultado
                    {
                        Cliente = new PagosSociosComercialesDataOutputCliente
                        {
                            Nombre = "",
                            ApellidoMaterno = "",
                            ApellidoPaterno = "",
                            NumeroCuenta = ""
                        },
                        FechaRegistro = DateTime.Now.ToString("yyyy-MM-dd"),
                        HoraRegistro = DateTime.Now.ToString("hh:mm:ss"),
                        NumeroMovimiento = camDatosSalida?.NumeroMovimiento
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
