﻿using ApiGateway.Common.HttpExtensions.Models;
using ApiGateway.Common.HttpExtensions.Models.HttpInterfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using static ApiGateway.Common.HttpExtensions.Models.Enums.HttpEnum;
using static ApiGateway.Models.Microservices.Settings.MonederoSettings;

namespace ApiGateway.Services.Monedero
{
    public class RetencionService : IRetencionService
    {
        private readonly IHttpStrategy _httpStrategy;
        private readonly RetencionSettings _retencionSettings;

        public RetencionService(IHttpStrategy httpStrategy,
            IOptions<RetencionSettings> retencionSettings)
        {
            _httpStrategy = httpStrategy;
            _retencionSettings = retencionSettings.Value;
        }

        public async Task<object> ExecuteServiceAsync(object input)
        {
            var content = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, _retencionSettings.ContentTypeHeaderValue);

            var response = await _httpStrategy.CallServiceAsync(OperationType.Post, new HttpModelContent
            {
                AcceptHeaderValue = _retencionSettings.AcceptHeaderValue,
                AuthenticationSchema = _retencionSettings.AuthenticationSchema,
                BaseUri = _retencionSettings.BaseUri,
                BearerToken = string.Empty,
                RequestUri = _retencionSettings.RequestUri,
                Content = content
            }, null);

            var responseContent = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<dynamic>(responseContent);
        }
    }
}