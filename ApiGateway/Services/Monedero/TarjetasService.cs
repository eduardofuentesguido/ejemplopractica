﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class TarjetasService : ITarjetasService
    {
        private readonly ILogger<ITarjetasService> _logger;
        private readonly IControlMicroservices _controlMicroservices;
        private readonly IMonederoMicroservices _monederoMicroservices;

        public TarjetasService(
            ILogger<TarjetasService> logger,
            IControlMicroservices controlMicroservices,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="ITarjetasService.ExecuteServiceAsync(TarjetaDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<TarjetaDataOutput>> ExecuteServiceAsync(
            TarjetaDataEntry tarjetaDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<TarjetaDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Ejecuta servicio OnPremise
                timer.Stop();
                #endregion

                #region Devuelve respuesta

                await Task.Run(() =>
                    datosSalida.DataOutput = new TarjetaDataOutput
                    {
                        Mensaje = "Operación ",
                        Folio = "0987654321",
                        Resultado = new TarjetaDataOutputResultado
                        {
                            Cvv = "123",
                            NumeroCuenta = "1234567890",
                            FechaExpiracion = "20220101",
                            NumeroTarjeta = "1234567890",
                        }
                    }
                );
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
