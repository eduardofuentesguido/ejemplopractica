﻿using ApiGateway.Common.Extensions;
using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.OnPremise.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using static ApiGateway.Models.Enums.ApiGatewayEnums;

namespace ApiGateway.Services.Monedero.Cuentas
{
    /// <inheritdoc cref="IAperturaService"/>
    public class AperturaService : IAperturaService
    {
        private readonly ServiceSettings _serviceSettings;
        private readonly ILogger<AperturaService> _logger;
        private readonly IOnPremiseServices _onPremiseServices;
        private readonly ApiGatewaySettings _apiGatewaySettings;
        private readonly IMonederoMicroservices _monederoMicroservices;

        /// <summary>
        /// Constructor del Servicio de Apertura de Monedero.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="onPremiseServices"></param>
        /// <param name="serviceSettings"></param>
        /// <param name="monederoMicroservices"></param>
        public AperturaService(
            ILogger<AperturaService> logger,
            IOnPremiseServices onPremiseServices,
            IOptions<ServiceSettings> serviceSettings,
            IOptions<ApiGatewaySettings> apiGatewaySettings,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _onPremiseServices = onPremiseServices;
            _serviceSettings = serviceSettings.Value;
            _apiGatewaySettings = apiGatewaySettings.Value;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="IAperturaService.ExecuteServiceAsync(AperturaDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<AperturaDataOutput>> ExecuteServiceAsync(
            AperturaDataEntry aperturaDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<AperturaDataOutput>();
            try
            {
                #region Valida Headers
                var headers = new Dictionary<string, string>
                {
                    { ApiGatewayConstants.Headers.IdInteraccion, headerDictionary[ApiGatewayConstants.Headers.IdInteraccion] }
                };
                #endregion

                //var decryptedClass = await DecryptAttributeHelper.GetDecryptedObjectAsync(aperturaDataEntry);
                //var encryptedObject = await EncryptAttributeHelper.GetEncryptedObjectAsync(aperturaDataEntry);
                //var jsonString = JsonSerializer.Serialize(decryptedClass);

                HttpResponseMessage onPremResponseMessage = null;
                HttpResponseMessage superCoreResponseMessage = null;
                JObject _onPremResponseObjectData = null;
                JObject _superCoreResponseObjectData = null;

                switch (_serviceSettings.AperturaMonedero)
                {
                    case SwitchExecution.SoloAlnova:
                        #region Ejecuta servicio de OnPrem
                        onPremResponseMessage = await _onPremiseServices.AperturaAsync(aperturaDataEntry, headers);
                        _onPremResponseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(onPremResponseMessage, _logger);
                        _logger.LogInformation(_onPremResponseObjectData.ToString());
                        #endregion

                        #region Desencripta información de OnPrem
                        #endregion

                        #region Devuelve respuesta
                        datosSalida.DataOutput = _onPremResponseObjectData.ToObject<AperturaDataOutput>();
                        #endregion
                        break;
                    case SwitchExecution.AlnovaSuperCoreSync:
                        #region Ejecuta servicio de OnPrem
                        onPremResponseMessage = await _onPremiseServices.AperturaAsync(aperturaDataEntry, headers);
                        _onPremResponseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(onPremResponseMessage, _logger);
                        _logger.LogInformation(_onPremResponseObjectData.ToString());
                        #endregion

                        #region Desencripta información de OnPrem
                        #endregion

                        #region Ejecuta Microservicio de Apertura
                        superCoreResponseMessage = await SuperCoreAperturaMonederoAsync(aperturaDataEntry, headerDictionary, "", "");
                        _superCoreResponseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(superCoreResponseMessage, _logger);
                        #endregion

                        #region Validacion de datos del Microservicio
                        NotificationsDataValidator.ValidateNotifications(_superCoreResponseObjectData);
                        #endregion

                        #region Devuelve respuesta
                        datosSalida.DataOutput = _onPremResponseObjectData.ToObject<AperturaDataOutput>();
                        #endregion
                        break;
                    case SwitchExecution.SuperCoreAlnova:
                    case SwitchExecution.AlnovaSuperCoreAsync:
                        datosSalida.Exception = new OperationException(
                            string.Format(_apiGatewaySettings.CodigosRespuesta.Errores.ECTL001,
                            nameof(_serviceSettings.AperturaMonedero).SplitCamelCase()), nameof(_apiGatewaySettings.CodigosRespuesta.Errores.ECTL001));
                        break;
                    default:
                        datosSalida.Exception = new OperationException(
                            string.Format(_apiGatewaySettings.CodigosRespuesta.Errores.ECTL001,
                            nameof(_serviceSettings.AperturaMonedero).SplitCamelCase()), nameof(_apiGatewaySettings.CodigosRespuesta.Errores.ECTL001));
                        break;
                }
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }

        private async Task<HttpResponseMessage> SuperCoreAperturaMonederoAsync(
            AperturaDataEntry aperturaDataEntry, IHeaderDictionary headerDictionary, string cuentaClabe, string cuentaInstrumental)
        {
            #region Inicializa StopWatch
            Stopwatch timer = new Stopwatch();
            timer.Start();
            #endregion

            #region Mapea datos de entrada para el Microservicio de Apertura
            var datosSupercore = new DatosSuperCore<MSAperturaDataEntry>
            {
                DatosControl = new DatosControl
                {
                    Id = "APERTURA",
                    Canal = aperturaDataEntry.Cliente.IdOrigen,
                    Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                    Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                    Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario,
                },
                DatosEntrada = new MSAperturaDataEntry
                {
                    Nombre = aperturaDataEntry.Cliente.Nombre,
                    ApePaterno = aperturaDataEntry.Cliente.ApellidoPaterno,
                    ApeMaterno = aperturaDataEntry.Cliente.ApellidoMaterno,
                    Celular = aperturaDataEntry.Cliente.Contacto.NumeroCelular,
                    Email = aperturaDataEntry.Cliente.Contacto.CorreoElectronico,
                    CuentaClabe = cuentaClabe,
                    CuentaInstrumental = cuentaInstrumental,
                    FechaNacimiento = aperturaDataEntry.Cliente.FechaNacimiento,
                    IdGenero = aperturaDataEntry.Cliente.IdGenero,
                    IdSICU = aperturaDataEntry.Sicu,
                    IdTipoMonedero = Convert.ToInt32(aperturaDataEntry.IdProducto),
                    IdSubTipoMonedero = Convert.ToInt32(aperturaDataEntry.IdSubProducto)
                },
                DatosExtendidos = new DatosExtendidos
                {
                    Latitud = aperturaDataEntry.Geolocalizacion.Latitud,
                    Longitud = aperturaDataEntry.Geolocalizacion.Longitud,
                    TokenSrvCom = "",
                    UUID = headerDictionary[ApiGatewayConstants.Headers.IdInteraccion]
                }
            };
            #endregion                

            #region Detiene Stopwatch
            timer.Stop();
            datosSupercore.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
            #endregion

            #region Ejecuta Microservicio
            return await _monederoMicroservices.AperturaAsync(datosSupercore, null);
            #endregion
        }
    }
}