﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero.Cuentas
{
    public class ConsultaDetalleCuentaService : IConsultaDetalleCuentaService
    {
        private readonly ILogger<ConsultaDetalleCuentaService> _logger;
        private readonly IMonederoMicroservices _monederoMicroservices;

        public ConsultaDetalleCuentaService(
            ILogger<ConsultaDetalleCuentaService> logger,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="IConsultaDetalleCuentaService.ExecuteServiceAsync(ConsultaDetalleCuentaDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<ConsultaDetalleCuentaDataOutput>> ExecuteServiceAsync(
            ConsultaDetalleCuentaDataEntry consultaDetalleCuentaDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<ConsultaDetalleCuentaDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Mapea datos de entrada para el Microservicio                
                var datosSupercore = new DatosSuperCore<MSConsultaDetalleCuentaDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "ConsulMonSicu",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSConsultaDetalleCuentaDataEntry
                    {
                        IdSicu = consultaDetalleCuentaDataEntry.Sicu
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = "",
                        Longitud = "",
                        TokenSrvCom = "",
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                decimal timeElapsed = timer.ElapsedMilliseconds;
                datosSupercore.DatosExtendidos.TiempoOrquestador = timeElapsed;
                #endregion

                #region Ejecuta Microservicio de ConsultaMonederoSicu
                var httpResponseMessage = await _monederoMicroservices.ConsultaMonederoSicuAsync(datosSupercore, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Devuelve respuesta          
                var notification = notifications.Notificaciones.FirstOrDefault();
                datosSalida.DataOutput = new ConsultaDetalleCuentaDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new ConsultaDetalleCuentaDataOutputResultado
                    {
                        Cuenta = new Cuenta
                        {
                            Numero = responseObjectData.SelectToken("datosSalida.idMonedero").ToString(),
                            Nivel = "N4",
                            MontoDeposito = 9999999
                        }
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}