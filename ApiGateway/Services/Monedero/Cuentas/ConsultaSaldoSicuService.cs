﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.OnPremise.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;
using static ApiGateway.Models.Enums.ApiGatewayEnums;

namespace ApiGateway.Services.Monedero.Cuentas
{
    public class ConsultaSaldoSicuService : IConsultaSaldoSicuService
    {
        private readonly ServiceSettings _serviceSettings;
        private readonly IOnPremiseServices _onPremiseServices;
        private readonly ILogger<ConsultaSaldoSicuService> _logger;
        private readonly IMonederoMicroservices _monederoMicroservices;

        public ConsultaSaldoSicuService(
            IOnPremiseServices onPremiseServices,
            ILogger<ConsultaSaldoSicuService> logger,
            IOptions<ServiceSettings> serviceSettings,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _onPremiseServices = onPremiseServices;
            _serviceSettings = serviceSettings.Value;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="IConsultaMovimientosService.ExecuteServiceAsync(ConsultaMovimientosDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<ConsultaSaldoSicuDataOutput>> ExecuteServiceAsync(
            ConsultaSaldoSicuDataEntry consultaSaldoSicuDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<ConsultaSaldoSicuDataOutput>();
            try
            {
                switch (_serviceSettings.ConsultaMovimientos)
                {
                    case SwitchExecution.SoloAlnova:
                        var onPremResponse = await _onPremiseServices.ConsultaSaldoAsync(consultaSaldoSicuDataEntry, null);
                        JObject onPremResponseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(onPremResponse, _logger);
                        _logger.LogInformation(onPremResponseObjectData.ToString());
                        datosSalida.DataOutput = onPremResponseObjectData.ToObject<ConsultaSaldoSicuDataOutput>();
                        break;
                    case SwitchExecution.AlnovaSuperCoreAsync:
                        break;
                    case SwitchExecution.AlnovaSuperCoreSync:
                        break;
                    case SwitchExecution.SuperCoreAlnova:
                        break;
                    default:
                        datosSalida.Exception = new ArgumentException("La configuración del servicio de Traspaso entre Cuentas no es valida:");
                        break;
                }

                //#region Inicializa StopWatch
                //Stopwatch timer = new Stopwatch();
                //timer.Start();
                //#endregion                

                //#region Mapea datos de entrada para el Microservicio                
                //var datosSupercore = new DatosSuperCore<MSConsultaSaldoSicuDataEntry>
                //{
                //    DatosControl = new DatosControl
                //    {
                //        Id = "CONSULSALSICU",
                //        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                //       Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                //        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                //        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                //    },
                //    DatosEntrada = new MSConsultaSaldoSicuDataEntry
                //    {
                //        Sicu = consultaSaldoSicuDataEntry.Folio
                //    },
                //    DatosExtendidos = new DatosExtendidos
                //    {
                //        Latitud = "Robert",
                //        Longitud = "Downey Jr.",
                //        TokenSrvCom = "3",
                //        UUID = headerDictionary["x-id-interaccion"]
                //    }
                //};
                //#endregion

                //#region Detiene Stopwatch
                //timer.Stop();
                //decimal timeElapsed = timer.ElapsedMilliseconds;
                //datosSupercore.DatosExtendidos.TiempoOrquestador = timeElapsed;
                //#endregion

                //#region Ejecuta Microservicio de Consulta Saldo Sicu
                //var httpResponseMessage = await _monederoMicroservices.ConsultaSaldoSicuAsync(datosSupercore);
                //JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                //#endregion

                //#region Validacion de datos
                //var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                //#endregion

                //#region Mapear respuesta del Microservicio
                //var client = JsonSerializer.Deserialize<Client>(
                //    responseObjectData.SelectToken("datosSalida").ToString(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                //#endregion

                //#region Devuelve respuesta
                //var notification = notifications.Notificaciones.FirstOrDefault();
                //datosSalida.DataOutput = new ConsultaSaldoSicuDataOutput
                //{
                //    Codigo = notification?.Notificacion,
                //    Folio = "",
                //    Mensaje = notification?.Mensaje,
                //    Resultado = new Resul
                //    {
                //        Cliente = client
                //    }
                //};
                //#endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
