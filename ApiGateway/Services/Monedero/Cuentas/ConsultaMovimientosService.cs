﻿using ApiGateway.Common.OnPremise.Utils.Models;
using ApiGateway.Common.OnPremise.Utils.Models.Interfaces;
using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.OnPremise.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using static ApiGateway.Models.Enums.ApiGatewayEnums;

namespace ApiGateway.Services.Monedero
{
    public class ConsultaMovimientosService : IConsultaMovimientosService
    {
        private readonly ServiceSettings _serviceSettings;
        private readonly IOnPremiseServices _onPremiseServices;
        private readonly ILogger<ConsultaMovimientosService> _logger;
        private readonly IMonederoMicroservices _monederoMicroservices;
        private readonly IOnPremiseSecurity _onPremiseSecurity;

        /// <summary>
        /// Constructor del la clase ConsultaMovimientosService.
        /// </summary>
        /// <param name="monederoMicroservices"></param>
        public ConsultaMovimientosService(
            IOnPremiseServices onPremiseServices,
            IOptions<ServiceSettings> serviceSettings,
            IOnPremiseSecurity onPremiseSecurity,
            ILogger<ConsultaMovimientosService> logger,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _onPremiseServices = onPremiseServices;
            _onPremiseSecurity = onPremiseSecurity;
            _serviceSettings = serviceSettings.Value;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="IConsultaMovimientosService.ExecuteServiceAsync(ConsultaMovimientosDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<ConsultaMovimientosDataOutput>> ExecuteServiceAsync(
            ConsultaMovimientosDataEntry consultaMovimientosDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<ConsultaMovimientosDataOutput>();
            try
            {
                //#region Inicializa StopWatch
                //Stopwatch timer = new Stopwatch();
                //timer.Start();
                //#endregion

                #region Valida Headers
                var headers = new Dictionary<string, string>
                {
                    { ApiGatewayConstants.Headers.IdInteraccion, headerDictionary[ApiGatewayConstants.Headers.IdInteraccion] }
                };
                #endregion

                //switch (_serviceSettings.ConsultaMovimientos)
                //{
                //    case SwitchExecution.SoloAlnova:
                //        var onPremResponse = await _onPremiseServices.ConsultaMovimientosAsync(consultaMovimientosDataEntry, headers);
                //        JObject onPremResponseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(onPremResponse, _logger);
                //        _logger.LogInformation(onPremResponseObjectData.ToString());
                //        datosSalida.DataOutput = onPremResponseObjectData.ToObject<ConsultaMovimientosDataOutput>();
                //        break;
                //    case SwitchExecution.AlnovaSuperCoreAsync:
                //        break;
                //    case SwitchExecution.AlnovaSuperCoreSync:
                //        break;
                //    case SwitchExecution.SuperCoreAlnova:
                //        break;
                //    default:
                //        datosSalida.Exception = new ArgumentException("La configuración del servicio de Consulta Movimientos no es valida:");
                //        break;
                //}

                var onPremConfiguration = new OnPremiseConfiguration
                {
                    BaseUri = "https://dev-api.bancoazteca.com.mx",
                    ConsumerKey = "4DrqGGm9bMGW9kis6VlHyGVptlDPMvDX",
                    ConsumerSecret = "A9moYTRk6GPmlbrQ",
                    ContentType = "application/x-www-form-urlencoded",
                    GrantType = "client_credentials",
                    RequestUri = "/oauth2/v1/token",
                };
                var bearerToken = await _onPremiseSecurity.GetAuthenticationTokenAsync(onPremConfiguration);

                datosSalida.DataOutput = new ConsultaMovimientosDataOutput
                {
                    Codigo = "",
                    Folio = "",
                    Mensaje = $"Token: {bearerToken}",
                    Resultado = new ConsultaMovimientosDataOutputResultado
                    {
                        
                    }
                };

                //#region Mapea datos de entrada para el Microservicio                
                //var datosSupercore = new DatosSuperCore<MSConsultaMovimientosDataEntry>
                //{
                //    DatosControl = new DatosControl
                //    {
                //        Id = "CONSULMOV",
                //       Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                //        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                //        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                //        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                //    },
                //    DatosEntrada = new MSConsultaMovimientosDataEntry
                //    {
                //        Monedero = consultaMovimientosDataEntry.NumeroCuenta,
                //        FechaInicio = consultaMovimientosDataEntry.FechaInicial,
                //        FechaFin = consultaMovimientosDataEntry.FechaFinal
                //    },
                //    DatosExtendidos = new DatosExtendidos
                //    {
                //        Latitud = "Robert",
                //        Longitud = "Downey Jr.",
                //        TokenSrvCom = "3",
                //        UUID = headerDictionary["x-id-interaccion"]
                //    }
                //};
                //#endregion                

                //#region Detiene Stopwatch
                //timer.Stop();                
                //datosSupercore.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                //#endregion

                //#region Ejecuta Microservicio de Consulta de Movimientos
                //var httpResponseMessage = await _monederoMicroservices.ConsultaMovimientosAsync(datosSupercore);
                //JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                //#endregion

                //#region Validacion de datos
                //var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                //#endregion

                //var movimientos = JsonSerializer.Deserialize<MSConsultaMovimientosDataOutput>(
                //    await httpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

                //#region Devuelve respuesta      
                //var notification = notifications.Notificaciones.FirstOrDefault();
                //datosSalida.DataOutput = new ConsultaMovimientosDataOutput
                //{
                //    Codigo = notification?.Notificacion,
                //    Folio = "",
                //    Mensaje = notification?.Mensaje,
                //    Resultado = new ConsultaMovimientosDataOutputResultado
                //    {
                //        Producto = movimientos.DatosSalida?.Producto,
                //        NumeroCuenta = movimientos.DatosSalida?.Monedero.ToString(),
                //        TitularCuenta = movimientos.DatosSalida?.TitularMonedero,
                //        FechaConsulta = movimientos.DatosSalida?.FechaConsulta,
                //        HoraConsulta = movimientos.DatosSalida?.HoraConsulta,
                //        Movimientos = movimientos.DatosSalida?.Movimientos.Select(x => new Movimiento
                //        {
                //            CodigoDivisa = x.CodigoDivisa,
                //            Descripcion = x.ConceptoMovimiento,
                //            FechaOperacion = x.FechaRegistro,
                //            Importe = x.MontoOperacion,
                //            NumeroMovimiento = x.NumeroMovimiento,
                //            Saldo = x.SaldoAnterior
                //        }).ToList()
                //    }
                //};
                //#endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
