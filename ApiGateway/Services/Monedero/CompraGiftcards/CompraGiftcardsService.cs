﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero.CompraGiftcards
{
    public class CompraGiftcardsService : ICompraGiftcardsService
    {
        private readonly ILogger<CompraGiftcardsService> _logger;
        private readonly IControlMicroservices _controlMicroservices;

        /// <summary>
        /// Constructor del la clase ConsultaMovimientosService.
        /// </summary>
        /// <param name="monederoMicroservices"></param>
        /// <param name="controlMicroservices"></param>
        public CompraGiftcardsService(
            ILogger<CompraGiftcardsService> logger,
            IControlMicroservices controlMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
        }


        /// <inheritdoc cref="ICompraGiftcardsService.ExecuteServiceAsync(string, CompraGiftcardsDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<CompraGiftcardsDataOutput>> ExecuteServiceAsync(
            string idOperador, CompraGiftcardsDataEntry compraGiftcardsDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<CompraGiftcardsDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Mapea datos de entrada para el Microservicio
                var camDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = "RET005",
                        Referencias = new List<string>
                        {
                            { "Compra de tarjeta de regalo" }
                        },
                        Montos = new List<decimal>
                        {
                            { Convert.ToDecimal(compraGiftcardsDataEntry.Transaccion.Monto) }
                        },
                        Monederos = new List<int>
                        {
                            { Convert.ToInt32(compraGiftcardsDataEntry.Transaccion.Cuenta) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = compraGiftcardsDataEntry.Geolocalizacion.Latitud,
                        Longitud = compraGiftcardsDataEntry.Geolocalizacion.Longitud,
                        TokenSrvCom = "3",
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                camDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de CAM
                var httpResponseMessage = await _controlMicroservices.ExecuteCamAsync(camDataEntry, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Devuelve respuesta                                
                var notification = notifications.Notificaciones.FirstOrDefault();
                datosSalida.DataOutput = new CompraGiftcardsDataOutput
                {
                    Mensaje = notification?.Mensaje,
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Resultado = new CompraGiftcardsDataOutputResultado
                    {
                        FechaHoraOperacion = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
