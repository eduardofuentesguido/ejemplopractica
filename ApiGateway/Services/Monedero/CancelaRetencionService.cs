﻿using ApiGateway.Common.HttpExtensions.Models;
using ApiGateway.Common.HttpExtensions.Models.HttpInterfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using static ApiGateway.Common.HttpExtensions.Models.Enums.HttpEnum;
using static ApiGateway.Models.Microservices.Settings.MonederoSettings;

namespace ApiGateway.Services.Monedero
{
    public class CancelaRetencionService : ICancelaRetencionService
    {
        private readonly IHttpStrategy _httpStrategy;
        private readonly CancelaRetencionSettings _cancelaretencionSettings;

        public CancelaRetencionService(IHttpStrategy httpStrategy,
            IOptions<CancelaRetencionSettings> cancelaretencionSettings)
        {
            _httpStrategy = httpStrategy;
            _cancelaretencionSettings = cancelaretencionSettings.Value;
        }

        public async Task<object> ExecuteServiceAsync(object input)
        {
            var content = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, _cancelaretencionSettings.ContentTypeHeaderValue);

            var response = await _httpStrategy.CallServiceAsync(OperationType.Post, new HttpModelContent
            {
                AcceptHeaderValue = _cancelaretencionSettings.AcceptHeaderValue,
                AuthenticationSchema = _cancelaretencionSettings.AuthenticationSchema,
                BaseUri = _cancelaretencionSettings.BaseUri,
                BearerToken = string.Empty,
                RequestUri = _cancelaretencionSettings.RequestUri,
                Content = content
            }, null);

            var responseContent = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<dynamic>(responseContent);
        }
    }
}