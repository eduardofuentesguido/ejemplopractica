﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class ReversosService : IReversosService
    {
        private readonly ILogger<ReversosService> _logger;
        private readonly IControlMicroservices _controlMicroservices;

        public ReversosService(ILogger<ReversosService> logger,
            IControlMicroservices controlMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
        }

        /// <inheritdoc cref="IReversosService.ExecuteServiceAsync(ReversoDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<ReversoDataOutput>> ExecuteServiceAsync(
            ReversoDataEntry reversoDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<ReversoDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Mapea datos de entrada para el Microservicio                
                var camDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        UUIDR = reversoDataEntry.UUIDR,
                        TipoOperacion = reversoDataEntry.TipoOperacion,
                        Referencias = new List<string> { },
                        Montos = new List<decimal> { },
                        Monederos = new List<int> { }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = reversoDataEntry.Geolocalizacion.Latitud,
                        Longitud = reversoDataEntry.Geolocalizacion.Longitud,
                        TokenSrvCom = "",
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                camDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de CAM
                var httpResponseMessage = await _controlMicroservices.ExecuteCamAsync(camDataEntry, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Mapea Datos de respuesta de CAM
                var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                    await httpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta 
                var camDatosSalida = camDataOutput.DatosSalida.Movimientos.FirstOrDefault();
                var notification = notifications.Notificaciones.FirstOrDefault();
                datosSalida.DataOutput = new ReversoDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new ReversoDataOutputResultado
                    {
                        ConceptoMovimiento = camDatosSalida.ConceptoMovimiento,
                        CuentaRelacionada = camDatosSalida.CuentaRelacionada,
                        NumeroMovimiento = camDatosSalida.NumeroMovimiento,
                        Operacion = camDatosSalida.Operacion,
                        SaldoActual = camDatosSalida.SaldoActual
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
