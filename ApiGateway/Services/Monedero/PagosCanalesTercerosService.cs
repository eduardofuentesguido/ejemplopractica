﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class PagosCanalesTercerosService : IPagosCanalesTercerosService
    {
        private readonly ILogger<PagosSociosComercialesService> _logger;
        private readonly IControlMicroservices _controlMicroservices;

        public PagosCanalesTercerosService(
            ILogger<PagosSociosComercialesService> logger,
            IControlMicroservices controlMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
        }

        /// <inheritdoc cref="IPagosCanalesTercerosService.ExecuteServiceAsync(PagosCanalesTercerosDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<PagosCanalesTercerosDataOutput>> ExecuteServiceAsync(
            PagosCanalesTercerosDataEntry pagosCanalesTercerosDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<PagosCanalesTercerosDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Mapea datos de entrada para el Microservicio
                var camDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = "RET005",
                        Referencias = new List<string> { },
                        Montos = new List<decimal>
                        {
                            { Convert.ToDecimal(pagosCanalesTercerosDataEntry.Monto)}
                        },
                        Monederos = new List<int>
                        {
                            { Convert.ToInt32(pagosCanalesTercerosDataEntry.CuentaCargo) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = "876543767",
                        Longitud = "31234567",
                        TokenSrvCom = "3",
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                camDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de CAM
                var httpResponseMessage = await _controlMicroservices.ExecuteCamAsync(camDataEntry, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Ejecuta servicio OnPremise                
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Mapea Datos de respuesta de CAM
                var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                    await httpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta
                var notification = notifications.Notificaciones.FirstOrDefault();
                var camDatosSalida = camDataOutput.DatosSalida.Movimientos.FirstOrDefault();
                datosSalida.DataOutput = new PagosCanalesTercerosDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new PagosCanalesTercerosDataOutputResultado
                    {
                        Cliente = new PagosCanalesTercerosDataOutputCliente
                        {
                            Nombre = "",
                            ApellidoPaterno = "",
                            ApellidoMaterno = ""
                        },
                        FechaHora = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"),
                        NumeroCuenta = "",
                        NumeroMovimiento = camDatosSalida?.NumeroMovimiento,
                        IdProgramaSocial = ""
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
