﻿using ApiGateway.Common.HttpExtensions.Models;
using ApiGateway.Common.HttpExtensions.Models.HttpInterfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using static ApiGateway.Common.HttpExtensions.Models.Enums.HttpEnum;
using static ApiGateway.Models.Microservices.Settings.MonederoSettings;

namespace ApiGateway.Services.Monedero
{
    public class ReversaCargoService : IReversaCargoService
    {
        private readonly IHttpStrategy _httpStrategy;
        private readonly ReversaCargoSettings _reversaCargoSettings;

        public ReversaCargoService(IHttpStrategy httpStrategy,
            IOptions<ReversaCargoSettings> reversaCargoSettings)
        {
            _httpStrategy = httpStrategy;
            _reversaCargoSettings = reversaCargoSettings.Value;
        }

        public async Task<object> ExecuteServiceAsync(object input)
        {
            var content = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, _reversaCargoSettings.ContentTypeHeaderValue);

            var response = await _httpStrategy.CallServiceAsync(OperationType.Post, new HttpModelContent
            {
                AcceptHeaderValue = _reversaCargoSettings.AcceptHeaderValue,
                AuthenticationSchema = _reversaCargoSettings.AuthenticationSchema,
                BaseUri = _reversaCargoSettings.BaseUri,
                BearerToken = string.Empty,
                RequestUri = _reversaCargoSettings.RequestUri,
                Content = content
            }, null);

            var responseContent = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<dynamic>(responseContent);
        }
    }
}