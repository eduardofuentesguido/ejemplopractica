﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class ConsultaSaldoService : IConsultaSaldoService
    {
        private readonly ILogger<ConsultaSaldoService> _logger;
        private readonly IMonederoMicroservices _monederoMicroservices;

        public ConsultaSaldoService(
            ILogger<ConsultaSaldoService> logger,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="IConsultaSaldoService.ExecuteServiceAsync(ConsultaSaldoMonederoDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<ConsultaSaldoMonederoDataOutput>> ExecuteServiceAsync(
            ConsultaSaldoMonederoDataEntry consultaSaldoMonederoDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<ConsultaSaldoMonederoDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Mapea datos de entrada para el Microservicio                
                var datosSupercore = new DatosSuperCore<MSConsultaSaldoMonederoDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CONSULSALDO",
                        Canal = ApiGatewayConstants.Avisos.CodigosDeAviso[""],
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSConsultaSaldoMonederoDataEntry
                    {
                        Empresa = 5499,
                        IdMonedero = Convert.ToInt32(consultaSaldoMonederoDataEntry.NumeroCuenta)
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = "Robert",
                        Longitud = "Downey Jr.",
                        TokenSrvCom = "3",
                        UUID = ""
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                datosSupercore.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de Consulta Bloqueos
                var httpResponseMessage = await _monederoMicroservices.ConsultaSaldoAsync(datosSupercore, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Mapea Datos de respuesta del Microservicio
                var mSConsultaSaldoMonedero = JsonSerializer.Deserialize<MSConsultaSaldoMonederoDataOutput>(
                    responseObjectData.SelectToken("datosSalida").ToString(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta
                var notification = notifications.Notificaciones.FirstOrDefault();
                datosSalida.DataOutput = new ConsultaSaldoMonederoDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new ConsultaSaldoMonederoDataOutputResultado
                    {
                        Divisa = mSConsultaSaldoMonedero?.DesDivisa,
                        NumeroCuenta = mSConsultaSaldoMonedero?.IdMonedero.ToString(),
                        TitularCuenta = mSConsultaSaldoMonedero?.TitularMonedero,
                        NumeroOperacion = mSConsultaSaldoMonedero?.NumeroOperacion.ToString(),
                        SaldoDisponible = mSConsultaSaldoMonedero?.SaldoDisponible.ToString(),
                        DescripcionCuenta = mSConsultaSaldoMonedero?.DesTipoMonedero,
                        FechaHoraOperacion = mSConsultaSaldoMonedero?.FechahoraOperacion
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}

