﻿using ApiGateway.Common.HttpExtensions.Models;
using ApiGateway.Common.HttpExtensions.Models.HttpInterfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using static ApiGateway.Common.HttpExtensions.Models.Enums.HttpEnum;
using static ApiGateway.Models.Microservices.Settings.MonederoSettings;

namespace ApiGateway.Services.Monedero
{
    public class ValidaAcumuladosService : IValidaAcumuladosService
    {
        private readonly IHttpStrategy _httpStrategy;
        private readonly ValidaAcumuladosSettings _validaAcumuladosSettings;

        public ValidaAcumuladosService(IHttpStrategy httpStrategy,
            IOptions<ValidaAcumuladosSettings> validaAcumuladosSettings)
        {
            _httpStrategy = httpStrategy;
            _validaAcumuladosSettings = validaAcumuladosSettings.Value;
        }

        public async Task<object> ExecuteServiceAsync(object input)
        {
            var content = new StringContent(JsonSerializer.Serialize(input), Encoding.UTF8, _validaAcumuladosSettings.ContentTypeHeaderValue);

            var response = await _httpStrategy.CallServiceAsync(OperationType.Post, new HttpModelContent
            {
                AcceptHeaderValue = _validaAcumuladosSettings.AcceptHeaderValue,
                AuthenticationSchema = _validaAcumuladosSettings.AuthenticationSchema,
                BaseUri = _validaAcumuladosSettings.BaseUri,
                BearerToken = string.Empty,
                RequestUri = _validaAcumuladosSettings.RequestUri,
                Content = content
            }, null);

            var responseContent = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<dynamic>(responseContent);
        }
    }
}
