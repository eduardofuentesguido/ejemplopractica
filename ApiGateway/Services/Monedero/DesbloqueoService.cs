﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class DesbloqueoService : IDesbloqueoService
    {
        private readonly ILogger<DesbloqueoService> _logger;
        private readonly IMonederoMicroservices _monederoMicroservices;

        public DesbloqueoService(
            ILogger<DesbloqueoService> logger,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="IDesbloqueoService.ExecuteServiceAsync(DesbloqueoMonederoDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<DesbloqueoMonederoDataOutput>> ExecuteServiceAsync(
            DesbloqueoMonederoDataEntry desbloqueoMonederoDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<DesbloqueoMonederoDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Mapea datos de entrada para el Microservicio                
                var datosSupercore = new DatosSuperCore<MSDesbloqueDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "DESBLOQUEO",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSDesbloqueDataEntry
                    {
                        TipoOperacion = desbloqueoMonederoDataEntry.TipoOperacion,
                        IdMonedero = Convert.ToInt32(desbloqueoMonederoDataEntry.NumeroMonedero)
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = "",
                        Longitud = "",
                        TokenSrvCom = "",
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                decimal timeElapsed = timer.ElapsedMilliseconds;
                datosSupercore.DatosExtendidos.TiempoOrquestador = timeElapsed;
                #endregion

                #region Ejecuta Microservicio de Desbloqueo de Monedero
                var httpResponseMessage = await _monederoMicroservices.DesbloqueoAsync(datosSupercore, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Devuelve respuesta          
                var notification = notifications.Notificaciones.FirstOrDefault();
                datosSalida.DataOutput = new DesbloqueoMonederoDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new DesbloqueoMonederoDataOutputResultado
                    {
                        NumeroOperacion = responseObjectData.SelectToken("datosSalida.numeroOperacion").Value<string>(),
                        SaldoImpactado = responseObjectData.SelectToken("datosSalida.saldoImpactado").Value<string>()
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
