﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero.Traspasos
{
    public class TransferenciaService : ITransferenciaService
    {
        private readonly ILogger<TransferenciaService> _logger;
        private readonly IControlMicroservices _controlMicroservices;

        public TransferenciaService(
            ILogger<TransferenciaService> logger,
            IControlMicroservices controlMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
        }

        /// <inheritdoc cref="ITransferenciaService.ExecuteServiceAsync(TransferenciaDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<TransferenciaDataOutput>> ExecuteServiceAsync(
            TransferenciaDataEntry transferenciaDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<TransferenciaDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Mapea datos de entrada para el Microservicio
                var camDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = "RET005",
                        Referencias = new List<string>
                        {
                            { transferenciaDataEntry.Transferencia.Concepto }
                        },
                        Montos = new List<decimal>
                        {
                            { Convert.ToDecimal(transferenciaDataEntry.Transferencia.Monto)}
                        },
                        Monederos = new List<int>
                        {
                            { Convert.ToInt32(transferenciaDataEntry.CuentaOrigen) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = transferenciaDataEntry.Geolocalizacion.Latitud,
                        Longitud = transferenciaDataEntry.Geolocalizacion.Longitud,
                        TokenSrvCom = transferenciaDataEntry.SegundoTokenVerificacion,
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Ejecuta Microservicio de CAM
                var httpResponseMessage = await _controlMicroservices.ExecuteCamAsync(camDataEntry, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Ejecuta servicio OnPremise                            
                #endregion               

                #region Mapea Datos de respuesta de CAM
                var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                    await httpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta
                var notification = notifications.Notificaciones.FirstOrDefault();
                var camDatosSalida = camDataOutput.DatosSalida.Movimientos.FirstOrDefault();
                datosSalida.DataOutput = new TransferenciaDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new TransferenciaDataOutputResultado
                    {
                        Cargo = new TransferenciaDataOutputCargo
                        {
                            ClaveRastreo = "",
                            CodigoDivisa = "",
                            Concepto = camDatosSalida.ConceptoMovimiento,
                            ConceptoOperacion = camDatosSalida.Operacion,
                            FechaRegistro = DateTime.Now.ToString("yyyy-MM.dd"),
                            HoraRegistro = DateTime.Now.ToString("hh:mm:ss"),
                            Monto = transferenciaDataEntry.Transferencia.Monto,
                        },
                        FechaOperacion = DateTime.Now.ToString("yyyy-MM.dd"),
                        HoraOperacion = DateTime.Now.ToString("hh:mm:ss"),
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
