﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.OnPremise.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static ApiGateway.Models.Enums.ApiGatewayEnums;

namespace ApiGateway.Services.Monedero.Traspasos
{
    public class TraspasosCuentasService : ITraspasoCuentasService
    {
        private readonly ServiceSettings _serviceSettings;
        private readonly IOnPremiseServices _onPremiseServices;
        private readonly ILogger<ITraspasoCuentasService> _logger;
        private readonly IControlMicroservices _controlMicroservices;
        private readonly IMonederoMicroservices _monederoMicroservices;

        public TraspasosCuentasService(
            IOnPremiseServices onPremiseServices,
            ILogger<TraspasosCuentasService> logger,
            IOptions<ServiceSettings> serviceSettings,
            IControlMicroservices controlMicroservices,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _onPremiseServices = onPremiseServices;
            _serviceSettings = serviceSettings.Value;
            _controlMicroservices = controlMicroservices;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="ITraspasoCuentasService.ExecuteServiceAsync(TraspasoCuentasDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<TraspasoCuentasDataOutput>> ExecuteServiceAsync(
            TraspasoCuentasDataEntry traspasoCuentasDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<TraspasoCuentasDataOutput>();
            try
            {
                //#region Inicializa StopWatch
                //Stopwatch timer = new Stopwatch();
                //timer.Start();
                //#endregion

                #region Valida Headers
                var headers = new Dictionary<string, string>
                {
                    { ApiGatewayConstants.Headers.IdInteraccion, headerDictionary[ApiGatewayConstants.Headers.IdInteraccion] },
                    { ApiGatewayConstants.Headers.IdClaveRastreo, headerDictionary[ApiGatewayConstants.Headers.IdClaveRastreo] },
                    { ApiGatewayConstants.Headers.IdOperacionConciliacion, headerDictionary[ApiGatewayConstants.Headers.IdOperacionConciliacion] }
                };
                #endregion

                switch (_serviceSettings.ConsultaMovimientos)
                {
                    case SwitchExecution.SoloAlnova:
                        var onPremResponse = await _onPremiseServices.TraspasoCuentasAsync(traspasoCuentasDataEntry, headers);
                        JObject onPremResponseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(onPremResponse, _logger);
                        _logger.LogInformation(onPremResponseObjectData.ToString());
                        datosSalida.DataOutput = onPremResponseObjectData.ToObject<TraspasoCuentasDataOutput>();
                        break;
                    case SwitchExecution.AlnovaSuperCoreAsync:
                        break;
                    case SwitchExecution.AlnovaSuperCoreSync:
                        break;
                    case SwitchExecution.SuperCoreAlnova:
                        break;
                    default:
                        datosSalida.Exception = new ArgumentException("La configuración del servicio de Traspaso entre Cuentas no es valida:");
                        break;
                }

                //#region Microservicio de CAM                
                //timer.Start();
                //#region Mapea datos de entrada para el Microservicio
                //var camDataEntry = new DatosSuperCore<MSCamDataEntry>
                //{
                //    DatosControl = new DatosControl
                //    {
                //        Id = "CAM",
                //        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                //       Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                //        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                //        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                //    },
                //    DatosEntrada = new MSCamDataEntry
                //    {
                //        TipoOperacion = "TRA001",
                //        Referencias = new List<string>
                //        {
                //            { traspasoCuentasDataEntry.Traspaso.Concepto }
                //        },
                //        Montos = new List<decimal>
                //        {
                //            { traspasoCuentasDataEntry.Traspaso.Monto },
                //            { traspasoCuentasDataEntry.Traspaso.Monto }
                //        },
                //        Monederos = new List<int>
                //        {
                //            { Convert.ToInt32(traspasoCuentasDataEntry.Traspaso.CuentaOrigen) },
                //            { Convert.ToInt32(traspasoCuentasDataEntry.Beneficiario.IdCliente) }
                //        }
                //    },
                //    DatosExtendidos = new DatosExtendidos
                //    {
                //        Latitud = traspasoCuentasDataEntry.Geolocalizacion.Latitud,
                //        Longitud = traspasoCuentasDataEntry.Geolocalizacion.Longitud,
                //        TokenSrvCom = traspasoCuentasDataEntry.SegundoTokenVerificacion,
                //        UUID = headerDictionary["x-id-interaccion"]
                //    }
                //};
                //#endregion

                //#region Detiene Stopwatch
                //timer.Stop();
                //camDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                //#endregion

                //#region Ejecuta Microservicio de CAM

                //var httpResponseMessage = await _controlMicroservices.ExecuteCamAsync(camDataEntry);
                //JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                //#endregion
                //#endregion

                //#region Validacion de datos
                //var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                //#endregion

                //#region Mapea Datos de respuesta de CAM
                //var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                //    await httpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                //#endregion

                //#region Devuelve respuesta
                //var camDatosSalida = camDataOutput.DatosSalida.Movimientos.FirstOrDefault();
                //var notification = notifications.Notificaciones.FirstOrDefault();
                //datosSalida.DataOutput = new TraspasoCuentasDataOutput
                //{
                //    Mensaje = notification?.Mensaje,
                //    Codigo = notification?.Notificacion,
                //    Folio = "",
                //    Resultado = new TraspasoCuentasDataOutputResultado
                //    {
                //        FechaOperacion = DateTime.Now.ToString("yyyy-MM-dd"),
                //        HoraOperacion = DateTime.Now.ToString("mm:ss"),
                //        NumeroMovimiento = camDatosSalida.NumeroMovimiento,
                //    }
                //};
                //#endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
