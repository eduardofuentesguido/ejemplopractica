﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero.Traspasos
{
    public class TraspasosTelefonosService : ITraspasoTelefonosService
    {
        private readonly ILogger<TraspasosTelefonosService> _logger;
        private readonly IControlMicroservices _controlMicroservices;
        private readonly IMonederoMicroservices _monederoMicroservices;

        public TraspasosTelefonosService(
            ILogger<TraspasosTelefonosService> logger,
            IControlMicroservices controlMicroservices,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="ITraspasoTelefonosService.ExecuteServiceAsync(TraspasoTelefonoDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<TraspasoTelefonoDataOutput>> ExecuteServiceAsync(
            TraspasoTelefonoDataEntry traspasoTelefonoDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<TraspasoTelefonoDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Microservicio de Consulta Monedero
                #region Mapea datos de entrada para el Microservicio                
                var datosSupercore = new DatosSuperCore<MSConsultaMonederoTelefonoDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CONSULMON",
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = "B204632",
                       Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                    },
                    DatosEntrada = new MSConsultaMonederoTelefonoDataEntry
                    {
                        NumeroTelefono = traspasoTelefonoDataEntry.Traspaso.NumeroTelefonoOrigen
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = traspasoTelefonoDataEntry.Geolocalizacion.Latitud,
                        Longitud = traspasoTelefonoDataEntry.Geolocalizacion.Longitud,
                        TokenSrvCom = traspasoTelefonoDataEntry.SegundoTokenVerificacion,
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion                

                #region Detiene Stopwatch
                timer.Stop();
                datosSupercore.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de Consulta de Monedero
                var httpResponseMessage = await _monederoMicroservices.ConsultaMonederoAsync(datosSupercore, null);
                #endregion
                #endregion

                #region Validacion de datos
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Microservicio de CAM
                timer.Reset();
                timer.Start();
                #region Mapea datos de entrada para el Microservicio
                var camDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                       Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = "TRA001",
                        Referencias = new List<string>
                        {
                            { traspasoTelefonoDataEntry.Traspaso.Concepto }
                        },
                        Montos = new List<decimal>
                        {
                            { Convert.ToDecimal(traspasoTelefonoDataEntry.Traspaso.Monto) },
                            { Convert.ToDecimal(traspasoTelefonoDataEntry.Traspaso.Monto) }
                        },
                        Monederos = new List<int>
                        {
                            { responseObjectData.SelectToken("datosSalida.monedero").Value<int>() },
                            { Convert.ToInt32(traspasoTelefonoDataEntry.Beneficiario.IdCliente) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = traspasoTelefonoDataEntry.Geolocalizacion.Latitud,
                        Longitud = traspasoTelefonoDataEntry.Geolocalizacion.Longitud,
                        TokenSrvCom = traspasoTelefonoDataEntry.SegundoTokenVerificacion,
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                camDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de CAM
                var httpResponseMessageCam = await _controlMicroservices.ExecuteCamAsync(camDataEntry, null);
                #endregion
                #endregion

                #region Validacion de datos
                JObject responseCamObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                var notifications = NotificationsDataValidator.ValidateNotifications(responseCamObjectData);
                #endregion

                #region Mapea Datos de respuesta de CAM
                var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                    await httpResponseMessageCam.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta
                var notification = notifications?.Notificaciones.FirstOrDefault();
                var camDatosSalida = camDataOutput?.DatosSalida.Movimientos.FirstOrDefault();
                datosSalida.DataOutput = new TraspasoTelefonoDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new TraspasoTelefonoDataOutputResultado
                    {
                        FechaOperacion = DateTime.Now.ToString("yyyy-MM-dd"),
                        HoraOperacion = DateTime.Now.ToString("hh:mm"),
                        NumeroMovimiento = camDatosSalida?.NumeroMovimiento
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
