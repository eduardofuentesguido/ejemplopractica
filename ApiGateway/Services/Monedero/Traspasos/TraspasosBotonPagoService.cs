﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero.Traspasos
{
    public class TraspasosBotonPagoService : ITraspasosBotonPagoService
    {
        private readonly IControlMicroservices _controlMicroservices;
        private readonly ILogger<TraspasosBotonPagoService> _logger;

        public TraspasosBotonPagoService(
            IControlMicroservices controlMicroservices,
            ILogger<TraspasosBotonPagoService> logger)
        {
            _logger = logger;
            _controlMicroservices = controlMicroservices;
        }

        /// <inheritdoc cref="ITraspasosBotonPagoService.ExecuteServiceAsync(TraspasoBotonPagoDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<TraspasoBotonPagoDataOutput>> ExecuteServiceAsync(
            TraspasoBotonPagoDataEntry traspasoBotonPagoDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<TraspasoBotonPagoDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Microservicio de CAM
                #region Mapea datos de entrada para el Microservicio
                var camDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = "TRA001",
                        Referencias = new List<string>
                        {
                            { traspasoBotonPagoDataEntry.Traspaso.Concepto }
                        },
                        Montos = new List<decimal>
                        {
                            { traspasoBotonPagoDataEntry.Traspaso.Monto },
                            { traspasoBotonPagoDataEntry.Traspaso.Monto }
                        },
                        Monederos = new List<int>
                        {
                            { traspasoBotonPagoDataEntry.Traspaso.CuentaOrigen },
                            { Convert.ToInt32(traspasoBotonPagoDataEntry.Beneficiario.IdCliente) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = traspasoBotonPagoDataEntry.Geolocalizacion.Latitud,
                        Longitud = traspasoBotonPagoDataEntry.Geolocalizacion.Longitud,
                        TokenSrvCom = traspasoBotonPagoDataEntry.SegundoTokenVerificacion,
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                camDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de CAM
                var httpResponseMessage = await _controlMicroservices.ExecuteCamAsync(camDataEntry, null);
                #endregion

                #region Valida Respuesta de CAM
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Mapea Datos de respuesta de CAM
                var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                    await httpResponseMessage.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta
                var notification = notifications.Notificaciones.FirstOrDefault();
                var camDatosSalida = camDataOutput.DatosSalida.Movimientos.FirstOrDefault();
                datosSalida.DataOutput = new TraspasoBotonPagoDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new TraspasoBotonPagoDataOutputResultado
                    {
                        DescripcionDestino = "",
                        DescripcionOrigen = "",
                        NumeroMovimiento = camDatosSalida.NumeroMovimiento,
                        FechaOperacion = DateTime.Now.ToString("yyyy-MM-dd"),
                        HoraOperacion = DateTime.Now.ToString("mm:ss")
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
