﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero.Traspasos
{
    public class TarjetaBusquedaService : ITarjetaBusquedaService
    {
        private readonly ILogger<TarjetaBusquedaService> _logger;
        private readonly IMonederoMicroservices _monederoMicroservices;

        public TarjetaBusquedaService(
            ILogger<TarjetaBusquedaService> logger,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="ITarjetaBusquedaService.ExecuteServiceAsync(TarjetaBusquedaDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<TarjetaBusquedaDataOutput>> ExecuteServiceAsync(
            TarjetaBusquedaDataEntry tarjetaBusquedaDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<TarjetaBusquedaDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Ejecuta servicio OnPremise
                #endregion

                #region Mapea datos de entrada para el Microservicio                
                var datosSupercore = new DatosSuperCore<MSConsultaSaldoSicuDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CONSULSALSICU",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSConsultaSaldoSicuDataEntry
                    {
                        Sicu = tarjetaBusquedaDataEntry.Sicu
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = tarjetaBusquedaDataEntry.Latitud,
                        Longitud = tarjetaBusquedaDataEntry.Longitud,
                        UUID = headerDictionary["x-id-interaccion"],
                        TokenSrvCom = "",
                    }
                };
                #endregion                

                #region Detiene Stopwatch
                timer.Stop();
                decimal timeElapsed = timer.ElapsedMilliseconds;
                datosSupercore.DatosExtendidos.TiempoOrquestador = timeElapsed;
                #endregion

                #region Ejecuta microservicio de consulta de monedero
                var httpResponseMessage = await _monederoMicroservices.ConsultaSaldoSicuAsync(datosSupercore, null);
                JObject responseObjectData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessage, _logger);
                #endregion

                #region Validacion de datos
                NotificationsDataValidator.ValidateNotifications(responseObjectData);
                #endregion

                #region Devuelve respuesta                                
                datosSalida.DataOutput = new TarjetaBusquedaDataOutput
                {
                    Resultadotd = new ResultadoTD
                    {
                        NumeroContrato = "",
                        CodigoMoneda = "",
                        CodigoComisionPlan = "",
                        DescripcionComision = "",
                        IdentificadorClave = "",
                        ValorEstatus = "",
                        ValorDisposicion = "",
                        Participante = new Participante
                        {
                            Nombre = "",
                            Clave = "",
                            Secuencia = "",
                        },
                        Tarjeta = new Tarjeta
                        {
                            Numero = "",
                            Tipo = "",
                            ValorTipo = "",
                            Estatus = "",
                            FechaExpiracion = "",
                        },
                        Monto = new Monto
                        {
                            Credito = "",
                            LimiteCredito = "",
                            AutorizadoCredito = "",
                            Disponible = "",
                            Autorizado = "",
                            Total = "",
                        },
                        Limite = new Limite
                        {
                            CajeroDia = "",
                            ZonaDia = "",
                            Credito = "",
                            DispositivoDia = "",
                            CajeroMes = "",
                        },
                    },
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
