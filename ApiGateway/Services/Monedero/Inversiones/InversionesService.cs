﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero.Inversiones
{
    public class InversionesService : IInversionesService
    {
        private readonly ILogger<InversionesService> _logger;
        private readonly IMonederoMicroservices _monederoMicroservices;

        /// <summary>
        /// Constructor del la clase ConsultaMovimientosService.
        /// </summary>
        /// <param name="monederoMicroservices"></param>
        public InversionesService(
            ILogger<InversionesService> logger,
            IMonederoMicroservices monederoMicroservices)
        {
            _logger = logger;
            _monederoMicroservices = monederoMicroservices;
        }

        /// <inheritdoc cref="IConsultaMovimientosService.ExecuteServiceAsync(ConsultaMovimientosDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<InversionesDataOutput>> ExecuteServiceAsync(
            InversionesDataEntry inversionesDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<InversionesDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Mapea datos de entrada para el Microservicio                
                #endregion                

                #region Detiene Stopwatch
                timer.Stop();
                #endregion

                await Task.Run(() =>
                    datosSalida.DataOutput = new InversionesDataOutput
                    {
                        Mensaje = "Operación exitosa.",
                        Folio = "864232520200521141446",
                        Codigo = "",
                        Resultado = new InversionesDataOutputResultado
                        {
                            MontoUdi = "13456",
                            FechaTransaccion = "2019-10-02",
                            TipoDivisa = "MXN"
                        }
                    }
                );
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
