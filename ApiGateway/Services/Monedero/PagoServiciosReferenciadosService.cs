﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiGateway.Services.Monedero
{
    public class PagoServiciosReferenciadosService : IPagoServiciosReferenciadosService
    {
        private readonly IControlMicroservices _controlMicroservices;
        private readonly ILogger<IPagoServiciosReferenciadosService> _logger;
        private readonly IMonederoMicroservices _monederoMicroservices;

        public PagoServiciosReferenciadosService(
            IControlMicroservices controlMicroservices,
            IMonederoMicroservices monederoMicroservices,
            ILogger<IPagoServiciosReferenciadosService> logger)
        {
            _controlMicroservices = controlMicroservices;
            _monederoMicroservices = monederoMicroservices;
            _logger = logger;
        }

        /// <inheritdoc cref="IPagoServiciosReferenciadosService.ExecuteServiceAsync(PagoServiciosReferenciadosDataEntry, IHeaderDictionary)"/>
        public async Task<DatosSalida<PagoServiciosReferenciadosPagoDataOutput>> ExecuteServiceAsync(
            PagoServiciosReferenciadosDataEntry pagoServiciosReferenciadosDataEntry, IHeaderDictionary headerDictionary)
        {
            var datosSalida = new DatosSalida<PagoServiciosReferenciadosPagoDataOutput>();
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                #region Microservicio de Consulta Monedero
                #region Mapea datos de entrada para el Microservicio de MSConsultaDetalleCuentaDataEntry
                #endregion

                #region Ejecuta Microservicio de Consulta de Monedero
                #endregion
                #endregion

                #region Validacion de datos                
                #endregion

                #region Microservicio de CAM                

                #region Mapea datos de entrada para el Microservicio
                var camDataEntry = new DatosSuperCore<MSCamDataEntry>
                {
                    DatosControl = new DatosControl
                    {
                        Id = "CAM",
                        Canal = ApiGatewayConstants.SuperCore.DatosDeControl.Canal,
                        Centro = ApiGatewayConstants.SuperCore.DatosDeControl.Centro,
                        Empresa = ApiGatewayConstants.SuperCore.DatosDeControl.Empresa,
                        Usuario = ApiGatewayConstants.SuperCore.DatosDeControl.Usuario
                    },
                    DatosEntrada = new MSCamDataEntry
                    {
                        TipoOperacion = "RET005",
                        Referencias = new List<string>
                        {
                            { pagoServiciosReferenciadosDataEntry.Transaccion.Transaccion.Referencia}
                        },
                        Montos = new List<decimal>
                        {
                            { Convert.ToDecimal(pagoServiciosReferenciadosDataEntry.Transaccion.Transaccion.Monto) }
                        },
                        Monederos = new List<int>
                        {
                            { Convert.ToInt32(pagoServiciosReferenciadosDataEntry.Transaccion.Transaccion.IdEmisor) }
                        }
                    },
                    DatosExtendidos = new DatosExtendidos
                    {
                        Latitud = headerDictionary["x-latitud"],
                        Longitud = headerDictionary["x-longitud"],
                        TokenSrvCom = "3",
                        UUID = headerDictionary["x-id-interaccion"]
                    }
                };
                #endregion

                #region Detiene Stopwatch
                timer.Stop();
                camDataEntry.DatosExtendidos.TiempoOrquestador = timer.ElapsedMilliseconds;
                #endregion

                #region Ejecuta Microservicio de CAM
                var httpResponseMessageCam = await _controlMicroservices.ExecuteCamAsync(camDataEntry, null);
                JObject responseObjectCamData = await HttpResponseDataValidator.GetResponseMessageAsync(httpResponseMessageCam, _logger);
                #endregion
                #endregion

                #region Validacion de datos
                var notifications = NotificationsDataValidator.ValidateNotifications(responseObjectCamData);
                #endregion

                #region Mapea Datos de respuesta de CAM
                var camDataOutput = JsonSerializer.Deserialize<MSCamDataOutput>(
                    await httpResponseMessageCam.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                #endregion

                #region Devuelve respuesta
                var notification = notifications.Notificaciones.FirstOrDefault();
                var camDatosSalida = camDataOutput.DatosSalida.Movimientos.FirstOrDefault();
                datosSalida.DataOutput = new PagoServiciosReferenciadosPagoDataOutput
                {
                    Codigo = notification?.Notificacion,
                    Folio = "",
                    Mensaje = notification?.Mensaje,
                    Resultado = new PagoServiciosReferenciadosPagoDataOutputResultado
                    {
                        Operacion = new PagoServiciosReferenciadosPagoDataOutputOperacion
                        {
                            IdMovimiento = camDatosSalida.NumeroMovimiento,
                            IdUnico = "",
                            Atributos = new Models.ControllerDataOutput.Atributos
                            {
                                Parametros = new List<Models.ControllerDataOutput.Parametros>
                                {
                                    {
                                        new Models.ControllerDataOutput.Parametros
                                        {
                                            Nombre = "",
                                            IdTipo = "",
                                            Valor = ""
                                        }
                                    }
                                },
                                Adicionales = new List<Models.ControllerDataOutput.Adicionales>
                                {
                                    {
                                        new Models.ControllerDataOutput.Adicionales
                                        {
                                            Nombre = "",
                                            IdTipo = "",
                                            Valor = ""
                                        }
                                    }
                                }
                            }
                        }
                    }
                };
                #endregion
            }
            catch (Exception ex) when (ex != null)
            {
                datosSalida.Exception = ex;
            }
            return datosSalida;
        }
    }
}
