﻿using ApiGateway.Common.Extensions;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ApiGateway.Services.Crypto
{
    /// <summary>
    /// The crypto service.
    /// </summary>
    public static class CryptoService
    {
        private static readonly string aes_key = "S4U3R0C2A3R0L1N4O3P4Z3P0KRPLWQFT";
        private static readonly string aes_iv = "02A221F1199B8240E22261C165885D35";

        /// <summary>
        /// Desencripta un texto o cadena de carácteres.
        /// </summary>
        /// <param name="encryptedText">Cadena de carácteres o texto a desencriptar.</param>
        /// <returns>Devuelve un texto desencriptado.</returns>
        public static async Task<string> DecryptAsync(string encryptedText)
        {
            byte[] cipherBytes = Convert.FromBase64String(encryptedText.UrlDecode());

            using AesCryptoServiceProvider cryptoServiceProvider = new AesCryptoServiceProvider();
            cryptoServiceProvider.KeySize = 256;
            cryptoServiceProvider.BlockSize = 128;            
            cryptoServiceProvider.Mode = CipherMode.CBC;
            cryptoServiceProvider.Padding = PaddingMode.PKCS7;
            cryptoServiceProvider.Key = Encoding.UTF8.GetBytes(aes_key);
            cryptoServiceProvider.IV = StringExtensions.StringToByteArray(aes_iv);

            using MemoryStream memoryStream = new MemoryStream();
            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoServiceProvider.CreateDecryptor(), CryptoStreamMode.Write))
            {
                await cryptoStream.WriteAsync(cipherBytes, 0, cipherBytes.Length).ConfigureAwait(false);
                cryptoStream.Close();
            }
            return Encoding.UTF8.GetString(memoryStream.ToArray());
        }

        /// <summary>
        /// Encripta un texto plano.
        /// </summary>
        /// <param name="plainText">Cadena de carácteres o texto a encriptar.</param>
        /// <returns>Devuelve un texto encriptado.</returns>
        public static async Task<string> EncryptAsync(string plainText)
        {
            byte[] clearInputBytes = Encoding.UTF8.GetBytes(plainText);

            using AesCryptoServiceProvider cryptoServiceProvider = new AesCryptoServiceProvider();
            cryptoServiceProvider.KeySize = 256;
            cryptoServiceProvider.BlockSize = 128;
            cryptoServiceProvider.Mode = CipherMode.CBC;
            cryptoServiceProvider.Padding = PaddingMode.PKCS7;
            cryptoServiceProvider.Key = Encoding.UTF8.GetBytes(aes_key);
            cryptoServiceProvider.IV = StringExtensions.StringToByteArray(aes_iv);

            using MemoryStream memoryStream = new MemoryStream();
            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoServiceProvider.CreateEncryptor(), CryptoStreamMode.Write))
            {
                await cryptoStream.WriteAsync(clearInputBytes, 0, clearInputBytes.Length).ConfigureAwait(false);
                cryptoStream.Close();
            }
            return Convert.ToBase64String(memoryStream.ToArray()).UrlEncode();
        }
    }
}
