﻿using ApiGateway.Helpers;
using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.Microservices.DataEntry;
using ApiGateway.Models.Microservices.DataOutput;
using ApiGateway.Models.Microservices.Interfaces;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using ApiGateway.Models.ServiceInterfaces;

namespace ApiGateway.Services
{
    public class HealthChecksService:IHealthChecksService
    {


        /// <inheritdoc cref="IHealthChecksService.ExecuteServiceAsync()"/>
        public async Task<String> ExecuteServiceAsync()
        {
            String body = new String("{\"status\": \"UP\"}");
            return body;
        }
    }
}
