﻿using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.OnPremise.Interfaces;
using ApiGateway.Models.RepositoryInterfaces.SuperCore;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using static ApiGateway.Models.Enums.ApiGatewayEnums;
using static ApiGateway.Models.OnPremise.Settings.OnPremiseSettings;

namespace ApiGateway.Services.OnPremise
{
    public class OnPremiseServices : IOnPremiseServices
    {
        private readonly OnPremAperturaSettings _onPremAperturaSettings;
        private readonly OnPremConsultaSaldoSettings _onPremConsultaSaldoSettings;
        private readonly OnPremConsultaMovimientosSettings _onPremConsultaMovimientosSettings;
        private readonly OnPremTraspasoCuentasSettings _onPremTraspasoCuentasSettings;
        private readonly IServiceExecutionRepository _serviceExecutionRepository;

        public OnPremiseServices(
            IOptions<OnPremAperturaSettings> onPremAperturaSettings,
            IOptions<OnPremConsultaSaldoSettings> onPremConsultaSaldoSettings,
            IOptions<OnPremConsultaMovimientosSettings> onPremConsultaMovimientosSettings,
            IOptions<OnPremTraspasoCuentasSettings> onPremTraspasoCuentasSettings,
            IServiceExecutionRepository serviceExecutionRepository)
        {
            _onPremAperturaSettings = onPremAperturaSettings.Value;
            _onPremConsultaSaldoSettings = onPremConsultaSaldoSettings.Value;
            _onPremConsultaMovimientosSettings = onPremConsultaMovimientosSettings.Value;
            _onPremTraspasoCuentasSettings = onPremTraspasoCuentasSettings.Value;
            _serviceExecutionRepository = serviceExecutionRepository;
        }

        /// <inheritdoc cref="IOnPremiseServices.AperturaAsync(AperturaDataEntry, Dictionary{string, string})"/>
        public async Task<HttpResponseMessage> AperturaAsync(AperturaDataEntry aperturaDataEntry, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_onPremAperturaSettings, aperturaDataEntry, headerDictionary, EnterpriseSystem.OnPremise);
        }

        /// <inheritdoc cref="IOnPremiseServices.ConsultaMovimientosAsync(ConsultaMovimientosDataEntry, Dictionary{string, string})"/>
        public async Task<HttpResponseMessage> ConsultaMovimientosAsync(ConsultaMovimientosDataEntry consultaMovimientosDataEntry, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_onPremConsultaMovimientosSettings, consultaMovimientosDataEntry, headerDictionary, EnterpriseSystem.OnPremise);
        }

        /// <inheritdoc cref="IOnPremiseServices.ConsultaSaldoAsync(ConsultaSaldoSicuDataEntry, Dictionary{string, string})"/>
        public async Task<HttpResponseMessage> ConsultaSaldoAsync(ConsultaSaldoSicuDataEntry consultaSaldoSicuDataEntry, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_onPremConsultaSaldoSettings, consultaSaldoSicuDataEntry, headerDictionary, EnterpriseSystem.OnPremise);
        }

        /// <inheritdoc cref="IOnPremiseServices.TraspasoCuentasAsync(TraspasoCuentasDataEntry, Dictionary{string, string})"/>
        public async Task<HttpResponseMessage> TraspasoCuentasAsync(TraspasoCuentasDataEntry traspasoCuentasDataEntry, Dictionary<string, string> headerDictionary)
        {
            return await _serviceExecutionRepository.ExecutePostAsync(_onPremTraspasoCuentasSettings, traspasoCuentasDataEntry, headerDictionary, EnterpriseSystem.OnPremise);
        }
    }
}
