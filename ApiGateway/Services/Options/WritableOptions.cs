﻿using ApiGateway.Models.ServiceInterfaces.Options;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace ApiGateway.Services.Options
{
    /// <inheritdoc cref="IWritableOptions{T}"/>
    public class WritableOptions<T> : IWritableOptions<T> where T : class, new()
    {
        private readonly string _file;
        private readonly string _section;
        private readonly IOptionsMonitor<T> _options;
        private readonly IWebHostEnvironment _environment;
        private readonly IConfigurationRoot _configuration;

        /// <summary>
        /// Gets the value.
        /// </summary>
        public T Value => _options.CurrentValue;

        /// <summary>
        /// Gets the.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>A T.</returns>
        public T Get(string name) => _options.Get(name);

        /// <summary>
        /// Initializes a new instance of the <see cref="WritableOptions"/> class.
        /// </summary>
        /// <param name="file">The file name.</param>
        /// <param name="section">The section name.</param>
        /// <param name="options">The options.</param>
        /// <param name="environment">The environment.</param>
        /// <param name="configuration">The configuration.</param>
        public WritableOptions(
            string file,
            string section,
            IOptionsMonitor<T> options,
            IWebHostEnvironment environment,
            IConfigurationRoot configuration)
        {
            _file = file;
            _section = section;
            _options = options;
            _environment = environment;
            _configuration = configuration;
        }

        /// <inheritdoc cref="IWritableOptions.Update(Action{T})"/>
        public void Update(Action<T> applyChanges)
        {
            var fileProvider = _environment.ContentRootFileProvider;
            var fileInfo = fileProvider.GetFileInfo(_file);
            var physicalPath = fileInfo.PhysicalPath;

            var jObject = JsonConvert.DeserializeObject<JObject>(File.ReadAllText(physicalPath));
            var sectionObject = jObject.TryGetValue(_section, out JToken section) ?
                JsonConvert.DeserializeObject<T>(section.ToString()) : (Value ?? new T());

            applyChanges(sectionObject);

            jObject[_section] = JObject.Parse(JsonConvert.SerializeObject(sectionObject));
            File.WriteAllText(physicalPath, JsonConvert.SerializeObject(jObject, Formatting.Indented));
            _configuration.Reload();
        }
    }
}
