﻿using ApiGateway.Models.RepositoryInterfaces.Kafka;
using ApiGateway.Models.ServiceInterfaces.Kafka;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: LLAMA AL SERVICIO QUE SE ENCQARGQA DE PUBLICAR EN KAFKAQQQQQ.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Services.Kafka
{
    public class KafkaService : IKafkaService
    {
        private readonly IKafkaRepository _kafkaRepository;

        /// <summary>
        /// Constructor de la clase KafkaRepository
        /// </summary>
        /// <param name="kafkaRepository">Inyección de <see cref="IKafkaRepository"/></param>
        public KafkaService(IKafkaRepository kafkaRepository)
        {
            _kafkaRepository = kafkaRepository;
        }

        /// <inheritdoc cref="IKafkaService.SendMessageAsync(string)"/>
        public async Task SendMessageAsync(string message)
        {
            var result = await _kafkaRepository.SendMessageAsync(message);

            if (result != null)
            {
                throw result;
            }
        }
    }
}
