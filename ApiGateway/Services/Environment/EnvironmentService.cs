﻿using ApiGateway.Models;
using ApiGateway.Models.ServiceInterfaces.Environment;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: DEFINE LA CONFIGURACION PARA LEER LA VARIABLE DE ENTORNO DEL AMBIENTE DONDE SE ESTÁ EJECUTANDO.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Services.Environment
{
    /// <inheritdoc cref="IEnvironmentService"/>
    public class EnvironmentService : IEnvironmentService
    {
        public string EnvironmentName { get; }

        /// <summary>
        /// Constructor de la clase EnvironmentService
        /// </summary>
        public EnvironmentService()
        {
            EnvironmentName =
                System.Environment.GetEnvironmentVariable(ApiGatewayConstants.EnvironmentVariables.Environment)
                ?? ApiGatewayConstants.Environments.Development;
        }
    }
}
