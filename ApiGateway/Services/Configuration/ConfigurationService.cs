﻿using ApiGateway.Models;
using ApiGateway.Models.ServiceInterfaces.Configuration;
using ApiGateway.Models.ServiceInterfaces.Environment;
using Microsoft.Extensions.Configuration;
using System.IO;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: DEFINE LA CONFIGURACION PARA LA LECTURA DE LOS ARCHIVOS JSON.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Services.Configuration
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly IEnvironmentService _environmentServiceService;

        /// <summary>
        /// Constructor de la clase ConfigurationService
        /// </summary>
        /// <param name="environmentService"></param>
        public ConfigurationService(IEnvironmentService environmentService)
        {
            _environmentServiceService = environmentService;
        }

        /// <inheritdoc cref="IConfigurationService.GetConfiguration"/>
        public IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(
                    $"{ApiGatewayConstants.AppSettingsSource.FileName}.{_environmentServiceService.EnvironmentName}.{ApiGatewayConstants.AppSettingsSource.FileExtension}",
                    false, true)
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
