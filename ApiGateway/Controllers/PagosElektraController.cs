﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Text.Json;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLADOR DE ORQUESTADOR 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CONTROLADOR ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Controllers
{
    [Route("supercore/pagos/elektra")]
    [ApiController]
    public class PagosElektraController : ControllerBase
    {
        private readonly int statusCode = 500;
        private readonly ILogger<PagosElektraController> _logger;
        private readonly IPagosEstatusBusquedasService _pagosEstatusBusquedasService;
        private readonly IValiacionReferenciaService _valiacionReferenciaService;
        private readonly IRecargaTiempoAireService _RecargaTiempoAireService;
        private readonly IPagoServiciosReferenciadosService _pagoServiciosReferenciadosService;
        private readonly IHealthChecksService _healthChecksService;

        public PagosElektraController(
            ILogger<PagosElektraController> logger,
            IRecargaTiempoAireService RecargaTiempoAireService,
            IPagosEstatusBusquedasService pagosEstatusBusquedasService,
            IValiacionReferenciaService valiacionReferenciaService,
            IPagoServiciosReferenciadosService pagoServiciosReferenciadosService,
            IHealthChecksService healthChecksService)
        {
            _logger = logger;
            _RecargaTiempoAireService = RecargaTiempoAireService;
            _valiacionReferenciaService = valiacionReferenciaService;
            _pagosEstatusBusquedasService = pagosEstatusBusquedasService;
            _pagoServiciosReferenciadosService = pagoServiciosReferenciadosService;
            _healthChecksService = healthChecksService;
        }

        [HttpGet("v1/actuator/health/readinessState")]
        public async Task<IActionResult> ReadinessState()
        {
            var responseService = await _healthChecksService.ExecuteServiceAsync();
            return Ok(responseService);

        }

        /// <summary>
        /// Realiza una recarga de tiempo aire.
        /// </summary>
        /// <param name="RecargaTiempoAireDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("conectividad/tiempo-aire/v1/recargas")]
        public async Task<IActionResult> Recargas(RecargaTiempoAireDataEntry RecargaTiempoAireDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de RecargaTiempoAire.");
                var responseService = await _RecargaTiempoAireService.ExecuteServiceAsync(RecargaTiempoAireDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución del servicio de RecargaTiempoAire.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Consulta Saldo Sicu.");
            }
        }

        /// <summary>
        /// Valida las referencias de un pago.
        /// </summary>
        /// <param name="validacionReferenciasDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("switch-comercial/pagos-referenciados/v1/pagos/validacion-referencias")]
        public async Task<IActionResult> ValidacionReferencias(PagoServiciosReferenciadosDataEntry validacionReferenciasDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de pagos de servicio referenciados.");
                var responseService = await _valiacionReferenciaService.ExecuteServiceAsync(validacionReferenciasDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución del servicio de pagos de servicio referenciados.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Consulta Saldo Sicu.");
            }
        }

        /// <summary>
        /// Consulta el estatus de un pago.
        /// </summary>
        /// <param name="pagoServiciosReferenciadosDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("switch-comercial/pagos-referenciados/v1/pagos/estatus/busquedas")]
        public async Task<IActionResult> EstatusPagos(PagoServiciosReferenciadosDataEntry pagoServiciosReferenciadosDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de pagos de servicio referenciados.");
                var responseService = await _pagosEstatusBusquedasService.ExecuteServiceAsync(pagoServiciosReferenciadosDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución del servicio de pagos de servicio referenciados.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Consulta Saldo Sicu.");
            }
        }

        /// <summary>
        /// Realiza el pago de un servicio referenciado.
        /// </summary>
        /// <param name="pagoServiciosReferenciadosDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("switch-comercial/pagos-referenciados/v1/pagos")]
        public async Task<IActionResult> Pagos(PagoServiciosReferenciadosDataEntry pagoServiciosReferenciadosDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de RecargaTiempoAire.");
                var responseService = await _pagoServiciosReferenciadosService.ExecuteServiceAsync(pagoServiciosReferenciadosDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución del servicio de RecargaTiempoAire.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Pagos de Servicios Referenciados.");
            }
        }
    }
}
