﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Text.Json;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLADOR DE ORQUESTADOR 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   15 / 07 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    13-07-2021   CONTROLADOR ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Controllers
{
    [Route("supercore/gestion-clientes")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly int statusCode = 500;
        private readonly ILogger<ClientesController> _logger;
        private readonly IConsultaTelefonoService _consultaTelefonoService;
        private readonly IModificaTelefonoService _modificaTelefonoService;
        private readonly IHealthChecksService _healthChecksService;

        public ClientesController(
            ILogger<ClientesController> logger,
            IConsultaTelefonoService consultaTelefonoService,
            IModificaTelefonoService modificaTelefonoService,
            IHealthChecksService healthChecksService)
        {
            _logger = logger;
            _consultaTelefonoService = consultaTelefonoService;
            _modificaTelefonoService = modificaTelefonoService;
            _healthChecksService = healthChecksService;
        }

        [HttpGet("v1/actuator/health/readinessState")]
        public async Task<IActionResult> ReadinessState()
        {
            var responseService = await _healthChecksService.ExecuteServiceAsync();
            return Ok(responseService);

        }

        /// <summary>
        /// Realiza la busqueda de un teléfono.
        /// </summary>
        /// <returns></returns>
        [HttpGet("v1/clientes/telefonos/busquedas")]
        public async Task<IActionResult> ConsultaTelfonoSicu()
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Consulta de Telfono por SICU.");
                var responseService = await _consultaTelefonoService.ExecuteServiceAsync(headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de Consulta de Telfono por SICU.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Consulta de Telefono por SICU.");
            }
        }

        /// <summary>
        /// Modifica el teléfono de un monedero.
        /// </summary>
        /// <param name="modificaTelfonoDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPut("v1/clientes/telefonos")]
        public async Task<IActionResult> ModificaTelfono(ModificaTelfonoDataEntry modificaTelfonoDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Modifica Telfono.");
                var responseService = await _modificaTelefonoService.ExecuteServiceAsync(modificaTelfonoDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de Modifica Telefono.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Modifica Telefono.");
            }
        }
    }
}
