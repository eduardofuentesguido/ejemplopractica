﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Text.Json;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLADOR DE ORQUESTADOR 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CONTROLADOR ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Controllers
{
    [Route("supercore/captacion/traspasos")]
    [ApiController]
    public class TranspasosController : ControllerBase
    {
        private readonly int statusCode = 500;
        private readonly ILogger<TranspasosController> _logger;
        private readonly ITraspasoCuentasService _traspasoCuentasService;
        private readonly ITraspasoTelefonosService _traspasoTelefonosService;
        private readonly ITraspasosBotonPagoService _traspasoBotonPagoService;
        private readonly IHealthChecksService _healthChecksService;

        public TranspasosController(
            ILogger<TranspasosController> logger,
            ITraspasoCuentasService traspasoCuentasService,
            ITraspasoTelefonosService traspasoTelefonosService,
            ITraspasosBotonPagoService traspasoBotonPagoService,
            IHealthChecksService healthChecksService)
        {
            _logger = logger;
            _traspasoCuentasService = traspasoCuentasService;
            _traspasoTelefonosService = traspasoTelefonosService;
            _traspasoBotonPagoService = traspasoBotonPagoService;
            _healthChecksService = healthChecksService;
        }
        [HttpGet("v1/actuator/health/readinessState")]
        public async Task<IActionResult> ReadinessState()
        {
            var responseService = await _healthChecksService.ExecuteServiceAsync();
            return Ok(responseService);

        }

        /// <summary>
        /// Traspasos entre cuentas de mexico por numero de telefono.
        /// </summary>
        /// <param name="traspasoTelefonoDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/telefonos")]
        public async Task<IActionResult> Telefono(TraspasoTelefonoDataEntry traspasoTelefonoDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Traspaso entre Telefonos.");
                var responseService = await _traspasoTelefonosService.ExecuteServiceAsync(traspasoTelefonoDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución del servicio de traspaso entre telefonos.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de traspaso entre teléfonos.");
            }
        }

        /// <summary>
        /// Traspasos entre cuentas por numero de cuenta.
        /// </summary>
        /// <param name="traspasoBotonPagoDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/boton-pago")]
        public async Task<IActionResult> BotonPago(TraspasoBotonPagoDataEntry traspasoBotonPagoDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headersReq = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Boton Pago.");
                var responseService = await _traspasoBotonPagoService.ExecuteServiceAsync(traspasoBotonPagoDataEntry, headersReq);
                _logger.LogInformation("Termina ejecución del servicio de Boton Pago.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Boton Pago.");
            }
        }

        /// <summary>
        /// Traspasos entre cuentas por numero de cuenta.
        /// </summary>
        /// <param name="traspasoCuentasDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/cuentas")]
        public async Task<IActionResult> Cuentas(TraspasoCuentasDataEntry traspasoCuentasDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Apertura.");
                var responseService = await _traspasoCuentasService.ExecuteServiceAsync(traspasoCuentasDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución del servicio de apertura.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de traspaso entre cuentas.");
            }
        }
    }
}
