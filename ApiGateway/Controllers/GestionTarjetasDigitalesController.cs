﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Text.Json;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLADOR DE ORQUESTADOR 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CONTROLADOR ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Controllers
{
    [Route("supercore/captacion/gestion-tarjetas-digitales")]
    [ApiController]
    public class GestionTarjetasDigitalesController : ControllerBase
    {
        private readonly int statusCode = 500;
        private readonly ITarjetasService _tarjetasService;
        private readonly ITarjetaBusquedaService _tarjetaBusquedaService;
        private readonly ILogger<GestionTarjetasDigitalesController> _logger;
        private readonly IHealthChecksService _healthChecksService;

        public GestionTarjetasDigitalesController(
            ITarjetasService tarjetasService,
            ITarjetaBusquedaService tarjetaBusquedaService,
            ILogger<GestionTarjetasDigitalesController> logger,
            IHealthChecksService healthChecksService)
        {
            _tarjetasService = tarjetasService;
            _tarjetaBusquedaService = tarjetaBusquedaService;
            _logger = logger;
            _healthChecksService = healthChecksService;
        }

        [HttpGet("v1/actuator/health/readinessState")]
        public async Task<IActionResult> ReadinessState()
        {
            var responseService = await _healthChecksService.ExecuteServiceAsync();
            return Ok(responseService);

        }

        /// <summary>
        /// Genera Tarjeta digital.
        /// </summary>
        /// <param name="tarjetaDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/tarjetas")]
        public async Task<IActionResult> Tarjetas(TarjetaDataEntry tarjetaDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Tarjeta.");
                var responseService = await _tarjetasService.ExecuteServiceAsync(tarjetaDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución del servicio de Tarjeta.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Tarjetas.");
            }
        }

        /// <summary>
        /// Busqueda de tarjetas digitales.
        /// </summary>
        /// <param name="input">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/tarjetas/busquedas")]
        public async Task<IActionResult> BusquedaTarjetas(TarjetaBusquedaDataEntry tarjetaBusquedaDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Busqueda de Tarjetas.");
                var responseService = await _tarjetaBusquedaService.ExecuteServiceAsync(tarjetaBusquedaDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución del servicio de Busqueda de Tarjetas.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Busqueda de Tarjetas.");
            }
        }
    }
}
