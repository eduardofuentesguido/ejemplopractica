﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Text.Json;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLADOR DE ORQUESTADOR 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CONTROLADOR ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Controllers
{
    [Route("supercore/captacion/cuentas")]
    [ApiController]
    public class CuentasController : ControllerBase
    {
        private readonly int statusCode = 500;
        private readonly ILogger<CuentasController> _logger;
        private readonly IAperturaService _aperturaService;
        private readonly IConsultaSaldoService _consultaSaldoService;
        private readonly IConsultaSaldoSicuService _consultaSaldoSicuService;
        private readonly IConsultaMovimientosService _consultaMovimientosService;
        private readonly IConsultaDetalleCuentaService _consultaDetalleCuentaService;
        private readonly IHealthChecksService _healthChecksService;

        public CuentasController(
            IAperturaService aperturaService,
            ILogger<CuentasController> logger,
            IConsultaSaldoService consultaSaldoService,
            IConsultaMovimientosService consultaMovimientosService,
            IConsultaSaldoSicuService consultaSaldoSicuService,
            IConsultaDetalleCuentaService consultaDetalleCuentaService,
            IHealthChecksService healthChecksService)
        {
            _logger = logger;
            _aperturaService = aperturaService;
            _consultaSaldoService = consultaSaldoService;
            _consultaSaldoSicuService = consultaSaldoSicuService;
            _consultaMovimientosService = consultaMovimientosService;
            _consultaDetalleCuentaService = consultaDetalleCuentaService;
            _healthChecksService = healthChecksService;
        }

        [HttpGet("v1/actuator/health/readinessState")]
        public async Task<IActionResult> ReadinessState()
        {
            var responseService = await _healthChecksService.ExecuteServiceAsync();
            return Ok(responseService);

        }

        /// <summary>
        /// Apertura de Cuenta.
        /// </summary>
        /// <param name="aperturaDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/aperturas")]
        public async Task<IActionResult> Aperturas(AperturaDataEntry aperturaDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Apertura.");
                var responseService = await _aperturaService.ExecuteServiceAsync(aperturaDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de apertura.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Apertura de Cuentas.");
            }
        }

        /// <summary>
        /// Consulta Movimientos
        /// </summary>
        /// <param name="consultaMovimientosDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/movimientos/busquedas")]
        public async Task<IActionResult> ConsultaMovimientos(ConsultaMovimientosDataEntry consultaMovimientosDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Consulta de Movimientos.");
                var responseService = await _consultaMovimientosService.ExecuteServiceAsync(consultaMovimientosDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de Consulta de Movimientos.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Consulta de Movimientos.");
            }
        }

        /// <summary>
        /// Consulta saldo SICU
        /// </summary>
        /// <param name="consultaSaldoSicuDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/busquedas")]
        public async Task<IActionResult> ConsultaSaldoSicu(ConsultaSaldoSicuDataEntry consultaSaldoSicuDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Consulta de Saldo por SICU.");
                var responseService = await _consultaSaldoSicuService.ExecuteServiceAsync(consultaSaldoSicuDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de Consulta de Saldo por SICU.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Consulta Saldo Sicu.");
            }
        }

        /// <summary>
        /// Consulta saldo por Monedero
        /// </summary>
        /// <param name="consultaSaldoMonederoDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/saldos/busquedas")]
        public async Task<IActionResult> ConsultaSaldoMonedero(ConsultaSaldoMonederoDataEntry consultaSaldoMonederoDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Consulta de Saldo por Monedero.");
                var responseService = await _consultaSaldoService.ExecuteServiceAsync(consultaSaldoMonederoDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de Consulta de Saldo por Monedero.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Consulta Saldo Sicu.");
            }
        }

        /// <summary>
        /// Consulta el detalle de la cuenta (Monedero) por SICU.
        /// </summary>
        /// <param name="consultaDetalleCuentaDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/detalles/busquedas")]
        public async Task<IActionResult> ConsultaDetalleCuenta(ConsultaDetalleCuentaDataEntry consultaDetalleCuentaDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Consulta Detalle De Cuenta.");
                var responseService = await _consultaDetalleCuentaService.ExecuteServiceAsync(consultaDetalleCuentaDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de Consulta detalle cuenta.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Consulta detalle cuenta.");
            }
        }
    }
}
