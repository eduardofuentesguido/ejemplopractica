﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Text.Json;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLADOR DE ORQUESTADOR 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   15 / 07 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    15-07-2021   CONTROLADOR ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Controllers
{
    [Route("supercore/captacion/canales-terceros/transacciones")]
    [ApiController]
    public class CanalesTercerosController : ControllerBase
    {
        private readonly int statusCode = 500;
        private readonly ILogger<CanalesTercerosController> _logger;
        private readonly IPagosCanalesTercerosService _pagosCanalesTercerosService;
        private readonly IHealthChecksService _healthChecksService;

        public CanalesTercerosController(
            ILogger<CanalesTercerosController> logger,
            IPagosCanalesTercerosService pagosCanalesTercerosService,
             IHealthChecksService healthChecksService)
        {
            _logger = logger;
            _pagosCanalesTercerosService = pagosCanalesTercerosService;
            _healthChecksService = healthChecksService;
        }


        [HttpGet("v1/actuator/health/readinessState")]
        public async Task<IActionResult> ReadinessState()
        {
            var responseService = await _healthChecksService.ExecuteServiceAsync();
            return Ok(responseService);

        }

        /// <summary>
        /// Realiza el pago de un tercero.
        /// </summary>
        /// <param name="pagosCanalesTercerosDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/pagos")]
        public async Task<IActionResult> Pagos(PagosCanalesTercerosDataEntry pagosCanalesTercerosDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;

                _logger.LogInformation("Ejecuta servicio de Pagos - Canales Terceros.");
                var responseService = await _pagosCanalesTercerosService.ExecuteServiceAsync(pagosCanalesTercerosDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de Pagos - Canales Terceros.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Pagos - Socios Comerciales..");
            }
        }
    }
}
