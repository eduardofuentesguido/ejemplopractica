﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Text.Json;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLADOR DE ORQUESTADOR 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CONTROLADOR ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Controllers
{
    [Route("supercore/captacion/transferencias")]
    [ApiController]
    public class TransferenciasController : ControllerBase
    {
        private readonly int statusCode = 500;
        private readonly ILogger<TransferenciasController> _logger;
        private readonly ITransferenciaService _transferenciaService;
        private readonly IHealthChecksService _healthChecksService;

        public TransferenciasController(ILogger<TransferenciasController> logger,
            ITransferenciaService transferenciaService,
            IHealthChecksService healthChecksService)
        {
            _logger = logger;
            _transferenciaService = transferenciaService;
            _healthChecksService = healthChecksService;
        }

        [HttpGet("v1/actuator/health/readinessState")]
        public async Task<IActionResult> ReadinessState()
        {
            var responseService = await _healthChecksService.ExecuteServiceAsync();
            return Ok(responseService);

        }

        /// <summary>
        /// Realiza una transferencia Spei
        /// </summary>
        /// <param name="transferenciaDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/spei")]
        public async Task<IActionResult> Transferencia(TransferenciaDataEntry transferenciaDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Transferencia de SPEI.");
                var responseService = await _transferenciaService.ExecuteServiceAsync(transferenciaDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución de Transferencia de SPEI.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Transferencia de SPEI.");
            }
        }
    }
}
