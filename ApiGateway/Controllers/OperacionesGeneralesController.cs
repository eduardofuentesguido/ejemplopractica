﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ControllerDataOutput;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces;
using ApiGateway.Models.ServiceInterfaces.Options;
using ApiGateway.Models.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Text.Json;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLADOR DE ORQUESTADOR 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   22 / 07 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    22-07-2021   CONTROLADOR ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Controllers
{
    /// <summary>
    /// Controlador de operaciones generales del Orquestador.
    /// </summary>
    [Route("supercore/captacion/operaciones-generales")]
    [ApiController]
    public class OperacionesGeneralesController : ControllerBase
    {
        private readonly int statusCode = 500;
        private readonly ServiceSettings _serviceSettings;
        private readonly IReversosService _reversosService;
        private readonly ILogger<OperacionesGeneralesController> _logger;
        private readonly IWritableOptions<ServiceSettings> _writableServiceSettings;
        private readonly IHealthChecksService _healthChecksService;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="OperacionesGeneralesController"/>.
        /// </summary>
        /// <param name="reversosService">El servicio de reversos.</param>
        /// <param name="logger">The logger.</param>
        public OperacionesGeneralesController(
            IOptions<ServiceSettings> serviceSettings,
            IReversosService reversosService,
            ILogger<OperacionesGeneralesController> logger,
            IWritableOptions<ServiceSettings> writableServiceSettings,
            IHealthChecksService healthChecksService)
        {
            _serviceSettings = serviceSettings?.Value;
            _logger = logger;
            _reversosService = reversosService;
            _writableServiceSettings = writableServiceSettings;
            _healthChecksService = healthChecksService;
        }

        [HttpGet("v1/actuator/health/readinessState")]
        public async Task<IActionResult> ReadinessState()
        {
            var responseService = await _healthChecksService.ExecuteServiceAsync();
            return Ok(responseService);

        }

        /// <summary>
        /// Ejecuta el reverso de una operación.
        /// </summary>
        /// <param name="reversoDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/reversos")]
        public async Task<IActionResult> EjecutaReversos(ReversoDataEntry reversoDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Reversos.");
                var responseService = await _reversosService.ExecuteServiceAsync(reversoDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de reversos.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Reversos.");
            }
        }

        /// <summary>
        /// Actualiza la configuración del archivo appSettings.
        /// </summary>
        /// <param name="configuracionServiciosDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/configuracion-servicios")]
        public async Task<IActionResult> ActualizaConfiguracion(ConfiguracionServiciosDataEntry configuracionServiciosDataEntry)
        {
            try
            {
                await Task.Run(() =>
                    _writableServiceSettings.Update(applyChanges =>
                    {
                        var propertyInfo = applyChanges.GetType().GetProperty(configuracionServiciosDataEntry.NombreServicio);
                        if (propertyInfo == null)
                        {
                            throw new OperationException($"El nombre del servicio: {configuracionServiciosDataEntry.NombreServicio} no existe en el contexto actual.", "ECTL000");
                        }

                        TypeConverter typeConverter = TypeDescriptor.GetConverter(propertyInfo.PropertyType);
                        var propValue = typeConverter.ConvertFromString(configuracionServiciosDataEntry.ModoEjecucion);
                        applyChanges.GetType().GetProperty(configuracionServiciosDataEntry.NombreServicio).SetValue(applyChanges, propValue);

                        _serviceSettings.GetType().GetProperty(configuracionServiciosDataEntry.NombreServicio).SetValue(_serviceSettings, propValue);
                    })
                );

                var response = new ConfiguracionServiciosDataOutput
                {
                    Codigo = "ACTL000",
                    Mensaje = $"Actualización del servicio {configuracionServiciosDataEntry.NombreServicio} completada correctamente."
                };

                _logger.LogInformation($"Respuesta de ejecución:\n{response}");
                return Ok(response);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Actualización de la Configuración.");
            }
        }
    }
}
