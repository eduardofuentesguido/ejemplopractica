﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLADOR DE ORQUESTADOR 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CONTROLADOR ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Controllers
{
    [Route("supercore/captacion/gestion-cuentas-corporativas")]
    [ApiController]
    public class GestionCuentasCorporativasController : ControllerBase

    {
        private readonly int statusCode = 500;
        private readonly IAbonoService _abonoService;
        private readonly ILogger<GestionCuentasCorporativasController> _logger;
        private readonly ICargoService _cargoService;
        private readonly ICargoAbonoService _cargoAbonoService;
        private readonly IHealthChecksService _healthChecksService;

        public GestionCuentasCorporativasController(
            IAbonoService abonoService,
            ICargoService cargoService,
            ICargoAbonoService cargoAbonoService,
            ILogger<GestionCuentasCorporativasController> logger,
            IHealthChecksService healthChecksService)
        {
            _cargoService = cargoService;
            _abonoService = abonoService;
            _cargoAbonoService = cargoAbonoService;
            _logger = logger;
            _healthChecksService = healthChecksService;
        }

        [HttpGet("v1/actuator/health/readinessState")]
        public async Task<IActionResult> ReadinessState()
        {
            var responseService = await _healthChecksService.ExecuteServiceAsync();
            return Ok(responseService);

        }

        /// <summary>
        /// Cargo y abonos a negocios externos.
        /// </summary>
        /// <param name="cargoAbonoDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/cargo-abono")]
        public async Task<IActionResult> CargoAbono(CargoAbonoDataEntry cargoAbonoDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Cargo-Abono.");
                var responseService = await _cargoAbonoService.ExecuteServiceAsync(cargoAbonoDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución de Cargo-Abono");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Cargo-Abono");
            }
        }

        /// <summary>
        /// Cargo a negocios externos.
        /// </summary>
        /// <param name="cargoDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/cargos")]
        public async Task<IActionResult> Cargos(CargoDataEntry cargoDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de pago de tarjeta.");
                var responseService = await _cargoService.ExecuteServiceAsync(cargoDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución del servicio de pago de tarjeta.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Consulta de Cargos.");
            }
        }

        /// <summary>
        /// Abonos a negocios externos.
        /// </summary>
        /// <param name="abonoDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/abonos")]
        public async Task<IActionResult> Abonos(AbonoDataEntry abonoDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Abono.");
                var responseService = await _abonoService.ExecuteServiceAsync(abonoDataEntry, dictionaryHeader);
                _logger.LogInformation("Termina ejecución del servicio de Abono.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Abonos.");
            }
        }
    }
}
