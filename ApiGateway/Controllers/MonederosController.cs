﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Text.Json;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLADOR DE ORQUESTADOR 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   13 / 07 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    13-07-2021   CONTROLADOR ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Controllers
{
    [Route("supercore/gestion-monedero")]
    [ApiController]
    public class MonederosController : ControllerBase
    {
        private readonly int statusCode = 500;
        private readonly IBloqueoService _bloqueoService;
        private readonly ILogger<MonederosController> _logger;
        private readonly IDesbloqueoService _desbloqueoService;
        private readonly ICancelacionService _cancelacionService;
        private readonly IConsultaBloqueosService _consultaBloqueosService;
        private readonly IHealthChecksService _healthChecksService;

        public MonederosController(
            IBloqueoService bloqueoService,
            ILogger<MonederosController> logger,
            IDesbloqueoService desbloqueoService,
            ICancelacionService cancelacionService,
            IConsultaBloqueosService consultaBloqueosService,
            IHealthChecksService healthChecksService)
        {
            _logger = logger;
            _bloqueoService = bloqueoService;
            _desbloqueoService = desbloqueoService;
            _cancelacionService = cancelacionService;
            _consultaBloqueosService = consultaBloqueosService;
            _healthChecksService = healthChecksService;
        }

        [HttpGet("v1/actuator/health/readinessState")]
        public async Task<IActionResult> ReadinessState()
        {
            var responseService = await _healthChecksService.ExecuteServiceAsync();
            return Ok(responseService);

        }

        /// <summary>
        /// Realiza la cancelación de un Monedero.
        /// </summary>
        /// <param name="cancelacionMonederoDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/monederos/cancelaciones")]
        public async Task<IActionResult> CancelacionMonedero(CancelacionMonederoDataEntry cancelacionMonederoDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Cancelacion de un Monedero.");
                var responseService = await _cancelacionService.ExecuteServiceAsync(cancelacionMonederoDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de Cancelacion de un Monedero.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Cancelacion de un Monedero.");
            }
        }

        /// <summary>
        /// Realiza el bloqueo de un Monedero.
        /// </summary>
        /// <param name="bloqueoMonederoDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/monederos/bloqueos")]
        public async Task<IActionResult> BloqueoMonedero(BloqueoMonederoDataEntry bloqueoMonederoDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Bloqueo de Monedero.");
                var responseService = await _bloqueoService.ExecuteServiceAsync(bloqueoMonederoDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de Bloqueo de Monedero.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Bloqueo de Monedero.");
            }
        }

        /// <summary>
        /// Realiza el desbloqueo de un monedero.
        /// </summary>
        /// <param name="desbloqueoMonederoDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/monederos/desbloqueos")]
        public async Task<IActionResult> DesbloqueoMonedero(DesbloqueoMonederoDataEntry desbloqueoMonederoDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Desbloqueo de Monedero.");
                var responseService = await _desbloqueoService.ExecuteServiceAsync(desbloqueoMonederoDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de Desbloqueo de Monedero.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex) when (ex != null)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Desbloqueo de Monedero.");
            }
        }

        /// <summary>
        /// Realiza la consulta de los bloqueos de un Monedero.
        /// </summary>
        /// <param name="consultaBloqueosDataEntry"></param>
        /// <returns></returns>
        [HttpPost("v1/monederos/bloqueos/busquedas")]
        public async Task<IActionResult> ConsultaBloqueoMonedero(ConsultaBloqueosDataEntry consultaBloqueosDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var headerDictionary = HttpContext.Request.Headers;
                _logger.LogInformation("Ejecuta servicio de Consulta de Bloqueo de un Monedero.");
                var responseService = await _consultaBloqueosService.ExecuteServiceAsync(consultaBloqueosDataEntry, headerDictionary);
                _logger.LogInformation("Termina ejecución del servicio de Consulta de Bloqueo de un Monedero.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de Consulta de Bloqueo de un Monedero.");
            }
        }
    }
}
