﻿using ApiGateway.Models;
using ApiGateway.Models.ControllerDataEntry;
using ApiGateway.Models.ServiceInterfaces.Monedero;
using ApiGateway.Models.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Text.Json;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: CONTROLADOR DE ORQUESTADOR 
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CONTROLADOR ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Controllers
{
    [Route("supercore/corresponsalia-bancaria/operaciones-arcus")]
    [ApiController]
    public class OperadoresController : ControllerBase
    {
        private readonly int statusCode = 500;
        private readonly ILogger<OperadoresController> _logger;
        private readonly ICompraGiftcardsService _compraGiftcardsService;
        private readonly IHealthChecksService _healthChecksService;

        public OperadoresController(
            ILogger<OperadoresController> logger,
            ICompraGiftcardsService compraGiftcardsService,
            IHealthChecksService healthChecksService)
        {
            _logger = logger;
            _compraGiftcardsService = compraGiftcardsService;
            _healthChecksService = healthChecksService;
        }

             [HttpGet("v1/actuator/health/readinessState")]
        public async Task<IActionResult> ReadinessState()
        {
            var responseService = await _healthChecksService.ExecuteServiceAsync();
            return Ok(responseService);

        }
        /// <summary>
        /// Realiza la compra de una tarjeta.
        /// </summary>
        /// <param name="idOperador">Id de Operador.</param>
        /// <param name="compraGiftcardsDataEntry">Datos de entrada.</param>
        /// <returns></returns>
        [HttpPost("v1/operadores/{idOperador}/recargas/tarjetas-regalo")]
        public async Task<IActionResult> TarjetasRegalo(string idOperador, CompraGiftcardsDataEntry compraGiftcardsDataEntry)
        {
            try
            {
                #region Inicializa StopWatch
                Stopwatch timer = new Stopwatch();
                timer.Start();
                #endregion

                var dictionaryHeader = this.HttpContext.Request.Headers;
                _logger.LogInformation("Inicializa servicio de Compra de Gift Cards.");
                var responseService = await _compraGiftcardsService.ExecuteServiceAsync(idOperador, compraGiftcardsDataEntry, dictionaryHeader);
                _logger.LogInformation("Finaliza servicio de Compra de Gift Cards.");

                #region Detiene Stopwatch
                timer.Stop();
                responseService.ElapsedTime = timer.ElapsedMilliseconds;
                #endregion

                if (responseService.DataOutput != null)
                {
                    _logger.LogInformation($"Respuesta de ejecución del servicio: {JsonSerializer.Serialize(responseService)}");
                }

                if (responseService.Exception != null)
                {
                    throw responseService.Exception;
                }

                return Ok(responseService.DataOutput);
            }
            catch (OperationException ex)
            {
                _logger.LogError($"Código: {ex.Code}\nError: {ex.Message}\nStack: {ex.StackTrace}");
                return BadRequest(new { Codigo = ex.Code, Mensaje = ex.Message });
            }
            catch (Exception ex) when (ex != null)
            {
                _logger.LogError($"Error: {ex.Message}\nStack: {ex.StackTrace}");
                return StatusCode(statusCode, new { Mensaje = ex.Message });
            }
            finally
            {
                _logger.LogInformation("Finaliza proceso de compra de Gift Cards.");
            }
        }
    }
}
