﻿using ApiGateway.Common.OnPremise.Utils.Models;
using ApiGateway.Common.OnPremise.Utils.Models.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: OBTIENE EL TOKEN DE ON PREMISE.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.OnPremise.Utils.Services
{
    internal class AuthenticationService : IAuthenticationService
    {
        public async Task<string> GetTokenAsync(OnPremiseConfiguration onPremiseConfiguration)
        {
            var keyValues = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("grant_type", onPremiseConfiguration.GrantType)
            };

            using var httpClient = new HttpClient();
            using var content = new FormUrlEncodedContent(keyValues);
            content.Headers.Clear();
            content.Headers.ContentType = new MediaTypeHeaderValue(onPremiseConfiguration.ContentType);

            var requestUri = $"{onPremiseConfiguration.BaseUri}{onPremiseConfiguration.RequestUri}";
            var byteArray = Encoding.ASCII.GetBytes($"{onPremiseConfiguration.ConsumerKey}:{onPremiseConfiguration.ConsumerSecret}");
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            HttpResponseMessage httpResponseMessage = await httpClient.PostAsync(requestUri, content).ConfigureAwait(false);
            if ((int)httpResponseMessage.StatusCode < 200 || (int)httpResponseMessage.StatusCode > 299)
            {
                throw new HttpRequestException(new JObject
                {
                    new JProperty("reasonPhrase", httpResponseMessage.ReasonPhrase),
                    new JProperty("statusCode", (int) httpResponseMessage.StatusCode),
                    new JProperty("content", new JObject(
                        new JProperty("Request", httpResponseMessage.RequestMessage.RequestUri.ToString()),
                        new JProperty("HTTP Method Request", httpResponseMessage.RequestMessage.Method.ToString()),
                        new JProperty("Content Request", "Contenido privado, no se mostrará en el log."),
                        new JProperty("Response", await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false))).ToString())
                }.ToString());
            }

            var onPremiseAuthResponse = JsonConvert.DeserializeObject<OnPremiseAuthResponse>(
                await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            return onPremiseAuthResponse.AccessToken;
        }
    }
}
