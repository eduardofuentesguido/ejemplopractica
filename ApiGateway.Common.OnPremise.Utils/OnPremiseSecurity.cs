﻿using ApiGateway.Common.OnPremise.Utils.Models;
using ApiGateway.Common.OnPremise.Utils.Models.Interfaces;
using ApiGateway.Common.OnPremise.Utils.Services;
using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: DEVUELVE EL TOKEN DE ON PREMISE.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.OnPremise.Utils
{
    public class OnPremiseSecurity : IOnPremiseSecurity
    {
        public async Task<string> GetAuthenticationTokenAsync(OnPremiseConfiguration onPremiseConfiguration)
            => await new AuthenticationService().GetTokenAsync(onPremiseConfiguration).ConfigureAwait(false);
    }
}
