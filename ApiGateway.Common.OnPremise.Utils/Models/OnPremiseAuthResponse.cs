﻿using Newtonsoft.Json;
using System.Collections.Generic;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: MODELO DE DATOS PARA MAPEAR RESPUESTA DEL TOKEN DE ON PREMISE.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   26 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    26-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.OnPremise.Utils.Models
{
    internal class OnPremiseAuthResponse
    {
        [JsonProperty(PropertyName = "refresh_token_expires_in")]
        public string RefreshTokenExpiresIn { get; set; }

        [JsonProperty(PropertyName = "api_product_list")]
        public string ApiProductList { get; set; }

        [JsonProperty(PropertyName = "api_product_list_json")]
        public List<string> ApiProductListJson { get; set; }

        [JsonProperty(PropertyName = "organization_name")]
        public string OrganizationName { get; set; }

        [JsonProperty(PropertyName = "developer.email")]
        public string DeveloperEmail { get; set; }

        [JsonProperty(PropertyName = "token_type")]
        public string TokenType { get; set; }

        [JsonProperty(PropertyName = "issued_at")]
        public string IssuedAt { get; set; }

        [JsonProperty(PropertyName = "client_id")]
        public string ClientId { get; set; }

        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "application_name")]
        public string ApplicationName { get; set; }

        [JsonProperty(PropertyName = "scope")]
        public string Scope { get; set; }

        [JsonProperty(PropertyName = "expires_in")]
        public string ExpiresIn { get; set; }

        [JsonProperty(PropertyName = "refresh_count")]
        public string RefreshCount { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
    }
}
