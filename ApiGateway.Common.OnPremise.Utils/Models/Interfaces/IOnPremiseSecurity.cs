﻿using System.Threading.Tasks;

//--------------------------------------------------------------------------------
//-- DESCRIPCION: INTERFAZ PARA ESTABLECER MÉTODOS DE RESPUESTA DEL SERVICIO ON PREMISE.
//-- CREADOR: EDGAR GONZALO MARTINEZ ANGELES
//-- FECHA CREACION:   28 / 06 / 2021
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--   CODE      AUTOR     FECHA         DESCRIPCION
//-- ---------- ------- ------------ ---------------------------------------------
//--  EGMA0001   EGMA    28-06-2021   CLASE ORIGINAL
//--------------------------------------------------------------------------------

namespace ApiGateway.Common.OnPremise.Utils.Models.Interfaces
{
    public interface IOnPremiseSecurity
    {
        Task<string> GetAuthenticationTokenAsync(OnPremiseConfiguration onPremiseConfiguration);
    }
}
