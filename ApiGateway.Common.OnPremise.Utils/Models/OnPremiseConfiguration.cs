﻿namespace ApiGateway.Common.OnPremise.Utils.Models
{
    public class OnPremiseConfiguration
    {
        public string BaseUri { get; set; }
        public string RequestUri { get; set; }
        public string GrantType { get; set; }
        public string ContentType { get; set; }
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
    }
}
